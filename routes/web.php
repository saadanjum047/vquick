<?php

// use ImageOptimizer;

use App\Area;
use App\carts;
use App\Discount;

Route::get('/', function () {
    return view('welcome');
});
Route::get('/contact', function () {
    return view('contact');
})->name('contact') ; 


Route::get('/about', function () {
    return view('about');
})->name('about') ;


  //  language change
  Route::get('change-language/{lang}' , function($lang){
    
    App::setLocale($lang);

    session()->put('locale', $lang);

    if(session('locale') == 'ar'){

        $_SESSION['language'] = 'arabic' ;
    }else{
        $_SESSION['language'] = 'english' ;

    }
    return redirect()->back();

    
} ) ->name ('change-language') ;


Route::get('email', function () {

    $orderstatus = App\orderstatuses::find(38);
    $shipping = App\shippings::first();
    // , , 'shipping' => $shipping
    return view('emails.order_status' , ['msg' => 'Hello World' , 'order' => $orderstatus  ] );
})->name('email') ;


Route::get('/product', 'ProductsController@showProducts' )->name('product') ;

Route::get('/gallery', 'GalleriesController@showGalleryImages' )->name('gallery') ;

// Route::get('/product', function () {
//     return view('product');
// });
Route::get('/userlogin', function () {


    // dd(url()->previous());

    if(isset($_SESSION['logged_in'])){
        return redirect()->back()->with('error' , 'Already Logged in');
    }
    if(strpos( url()->previous() ,  'checkout') ){
        session()->put('checkout' , 'checkout');
    }

    return view('userlogin');

})->name('userlogin') ;
Route::get('/signup', function () {

    if(isset($_SESSION['logged_in'])){
        return redirect()->back()->with('error' , 'Already Logged in');
    }

    if(strpos( url()->previous() ,  'checkout') ){
        session()->put('checkout' , 'checkout');
    }
    $areas = Area::all();
    return view('signup' , compact('areas'));
});


Route::get('reset/password','RegusersController@resetEmailPassword')->name('reset.password');
Route::post('send-reset-password-link','RegusersController@sentPasswordResetLink')->name('send-reset-password-link');

Route::post('save-new-password','RegusersController@sentNewPassword')->name('save-new-password');


Route::get('password/reset/{email}/token/{token}','RegusersController@resetPassword')->name('password.reset.token');


Route::get('edit-profile' , 'RegusersController@editProfile')->name('edit-profile');


if(!isset($_SESSION)) 
{ 
    session_start(); 
}
use App\products;
use App\regusers;
use App\shippings;
use Carbon\Carbon;
use App\contactuses;
use App\orderstatuses;
use App\Jobs\SendDiscountMail;
use App\Jobs\SendCustomerEmail;
use App\Jobs\SendCustomerEmailCheck;
use Illuminate\Support\Facades\Route;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Artisan;
use Spatie\ImageOptimizer\OptimizerChainFactory;

Route::get('/checkout', "CheckoutController@viewCart" )->name('checkout') ;
Auth::routes();

Route::get('/home', 'AbcController@index')->middleware('auth');
Route::get('/header', 'HeaderController@index')->name('header');
Route::get('/aside', 'AsideController@index')->name('aside');
Route::resource('homes','HomeController')->middleware('auth');
Route::resource('categories','CategoriesController')->middleware('auth');
Route::get('/manage-category', 'CategoriesController@approve')->name('category');
Route::resource('products','ProductsController')->middleware('auth');

Route::get('products/exports/{ids}','ProductsController@exportProducts')->name('products.exports');

Route::get('/manage-products', 'ProductsController@approve')->name('products');
Route::resource('aboutuses','AboutusesController')->middleware('auth');
Route::resource('contactuses','ContactusesController')->middleware('auth');
Route::resource('orders','OrdersController')->middleware('auth');
Route::resource('homepages','HomepagesController')->middleware('auth');
Route::resource('galleries','GalleriesController')->middleware('auth');
Route::resource('shippings','ShippingsController')->middleware('auth');
Route::resource('areas','AreaController')->middleware('auth');
Route::get('areas/delete/{id}','AreaController@destroy')->name('areas.delete') ->middleware('auth');

Route::resource('regusers','RegusersController');

Route::post('guest-signup' ,'RegusersController@guestSignup' )->name('guest-signup') ;
Route::get('guest-signup' ,'RegusersController@guestSignup' )->name('guest-signup') ;

Route::get('reguser/{user}/orders','RegusersController@viewOrders')->name('reguser-orders');


Route::get('filter-users' , 'RegusersController@filter' )->name('filter-users') ;
Route::get('/export-customers/{ids}', 'RegusersController@exportCustomers')->name('export-customers');

Route::resource('carts','CartsController');
Route::resource('productsections','ProductsectionsController')->middleware('auth');
Route::get('/user-login', 'RegusersController@login')->name('orders');
Route::get('/user-logout', 'RegusersController@logout')->name('orders');
Route::get('/contact-form', 'ContactFormController@store')->name('orders');

Route::get('/carts', 'CartsController@store')->name('carts');
Route::get('/update-cart-quantity', 'CartsController@updateCartQuantity')->name('update-cart-quantity');

Route::get('/update-cart-quantity-ajax/{cart_id}/{quantity}', 'CartsController@updateCartQuantityAjax')->name('update-cart-quantity-ajax');

Route::post('register-guest', 'RegusersController@guestSignupStore')->name('register-guest') ;

Route::post('/order-cart', 'OrderstatusesController@store')->name('orderstatuses');
Route::get('/order-cart', 'OrderstatusesController@store')->name('orderstatuses');

Route::get('/sadad-payment', 'OrderstatusesController@sadadPayment')->name('sadad-payment');

Route::get('/sadad-payment-success', 'OrderstatusesController@sadadPaymentSuccess')->name('sadad-payment-success');

Route::post('/sadad-payment-success', 'OrderstatusesController@sadadPaymentSuccess')->name('sadad-payment-success');


Route::get('paymentsuccess', 'OrderstatusesController@payment_success');
Route::get('paymenterror', 'OrderstatusesController@payment_error');

Route::get('/delete-cart', 'CartsController@delete')->name('carts');
Route::resource('admins','AdminsController')->middleware('auth');

Route::get('/contact-form','ContactFormController@create');
Route::post('/contact-form','ContactFormController@store');
Route::post('/user-logout','RegusersController@logout');
Route::post('/user-login', 'RegusersController@login');
// Route::get('/product-category', 'ProductsController@show1')->name('products');
Route::get('/product-category', 'ProductsController@showCategoryProducts')->name('products');

Route::get('/gallery-images', 'GalleriesController@showGallery')->name('gallery-images');

Route::get('/order-history', 'OrdersController@showHistory')->name('order-history');
Route::get('/order-history/{order}/details', 'OrdersController@showOrderDetails')->name('order-history.details');

Route::resource('orderstatuses','OrderstatusesController')->middleware('auth');

Route::get('/export-orders/{ids}', 'OrderstatusesController@exportOrders')->name('export-orders');

Route::get('/export-orders-products/{ids}', 'OrderstatusesController@exportOrdersProducts')->name('export-orders-products');

Route::get('/export-order/{id}', 'OrderstatusesController@exportOrder')->name('export-order');
Route::get('/export-invoice/{id}', 'OrderstatusesController@exportInvoice')->name('export-invoice');

Route::get('/order-filter', 'OrderstatusesController@search')->name('orderstatuses');

Route::get('/product-filter', 'OrderstatusesController@search1')->name('orderstatuses');
Route::resource('first','FirstController');

Route::get('manage-discounts' , 'DiscountController@index')->name('manage-discounts');
Route::post('update-discounts' , 'DiscountController@update')->name('update-discounts');

Route::get('gift-discount/{customer}' , 'DiscountController@giftDiscount')->name('gift-discount');
Route::get('remove-discount/{customer}' , 'DiscountController@removeDiscount')->name('remove-discount');

// dropzone images
Route::post('/images-save', 'GalleriesController@saveDropzoneImage')->name('images-save') ;
Route::get('/images-delete/{id}', 'GalleriesController@deleteDropzoneImage');








Route::post('/update-category-order', 'CategoriesController@updateOrder') ;

Route::post('/update-product-order', 'ProductsController@updateOrder') ;

// Route::get('test', function(){
//     // return view('layouts.main');
//     $orderstatuses = orderstatuses::latest()->first();
    
//     $customer = regusers::where('id' ,$orderstatuses->cid )->get();

//     $message = 'Dear '. $orderstatuses->customer->full_name.', we have received order # ' . $orderstatuses->id .' and are working on it now.<br></p><p style="color: #242424;">We will email you an update when we have delivered it.<br>';

//     $subject = 'Your vQuick Order is submitted';

//     dispatch(new SendCustomerEmail($customer , $message , $subject , $orderstatuses ));

// });

Route::get('email-test' , function(){

    $order = orderstatuses::latest()->first();

    $customer = regusers::where('id' ,$order->cid )->get();

    $message = 'Dear '. $order->customer->full_name.', we have received order # ' . $order->id .' and are working on it now.<br></p><p style="color: #242424;">We will email you an update when we have delivered it.<br>';

    $subject = 'Your vQuick Order is submitted';

    dispatch(new SendCustomerEmailCheck($customer , $message , $subject , $order ));
    echo "Email is dispatched  " . Carbon::now();
} );

Route::get('start-queue' , function(){
    Artisan::call("queue:restart");
    Artisan::call("queue:work");    
});


Route::get('invoice/{id}', function($id){

    $order = orderstatuses::find( $id );
    $msg = 'message';
    // return view('pdf.invoice' , compact('order' , 'msg') );
    return view('pdf.pdf_invoice2' , compact('order' , 'msg') );
});

Route::get('payment_success', function(){
    // return view('payment_success');
});
// use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

Route::get('resize' , function(){
    
});
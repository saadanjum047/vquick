<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orders extends Model
{
    protected $fillable = [
        'order_no', 'payment_method', 'quantity','available','status','full_name','address','date','product_name'
    ];
}
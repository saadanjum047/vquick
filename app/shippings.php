<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shippings extends Model
{
    protected $fillable = [
        'fee'
    ];
}

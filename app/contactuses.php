<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contactuses extends Model
{
    protected $fillable = [
        'phone', 'email'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    protected $fillable = [
        'image', 'name','arabic_name', 'description' , 'description_ar','price','stock_available','category','is_approved' 
    ];



    public function getProperNameAttribute($value){

        if(session('locale') == 'ar'){
            return $this->arabic_name;
        }else{
            return $this->name;
        }
    }

    public function getProperDescriptionAttribute($value){

        if(session('locale') == 'ar'){
            return $this->description_ar;
        }else{
            return $this->description;
        }
    }


}

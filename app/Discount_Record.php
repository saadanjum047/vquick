<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount_Record extends Model
{

    protected $table = 'discount_records';
    protected $guarded = [];
}

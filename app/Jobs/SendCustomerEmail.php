<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


class SendCustomerEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    // public $users;
    public $tries = 3;

    public $message;
    public $users;
    public $user;
    public $subject;
    public $order;

    public function __construct($users , $message , $subject , $order  )
    {
        $this->users = $users;
        $this->message = $message;
        $this->subject = $subject;
        $this->order = $order;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      
        $data['msg'] = $this->message ;
        $data['order'] = $this->order;
        $data['from'] = 'VQuick';
        // dd($data['order']);
        foreach($this->users as $user){
            
            if(isset($user) && $user->email){
                
                $send['email'] = $user->email;
                
                if($user->name){
                    $send['name'] = $user->name;
                }
                elseif($user->full_name){
                    $send['name'] = $user->full_name;
                }else{
                    $send['name'] = 'User';
                }
                
                
            try{
                    \Mail::send( 'emails.order_status', $data, function ($message) use ($send) {
                    $message->to( $send['email'] , $send['name']);
                    // $message->from( env('MAIL_FROM_ADDRESS') , 'vQuick');
                    $message->subject($this->subject);
                });
                }catch(Exception $e){
                    
                }
            }
        }
            
        // dd('send mail');
    }
}

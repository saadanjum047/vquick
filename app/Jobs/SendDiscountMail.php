<?php

namespace App\Jobs;

use App\Discount;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendDiscountMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $user;
    public $message;
    public $subject;


    public function __construct($user)
    {
        $this->user = $user;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $discount = Discount::first();
        
        $this->message = 'Congratulations '. $this->user->full_name.', <br>Now you are one of vquick family Enjoy '.$discount->percentage .' % for your next orders <br>Your discount will be expired on '. $this->user->discount_end_date;

    
        $this->subject = 'Congratulations '.$this->user->full_name.', you are one of vquick family ';
  
  
        $data['msg'] = $this->message ;
        $data['from'] = 'VQuick';
        $send['email'] = $this->user->email;
        $send['name'] = $this->user->full_name;
      
        // \Mail::send( 'emails.text_email', $data, function ($message) use ($send) {
        //     $message->to( $send['email'] , $send['name']);
        //     // $message->from( env('MAIL_FROM_ADDRESS') , 'vQuick');
        //     $message->subject($this->subject);
        // });

        \Mail::send( 'emails.order_status', $data, function ($message) use ($send) {
            $message->to( $send['email'] , $send['name']);
            // $message->from( env('MAIL_FROM_ADDRESS') , 'vQuick');
            $message->subject($this->subject);
        });


    }
}

<?php

namespace App\Exports;

use App\Invoice;
use App\orderstatuses;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class OrdersProductsExport implements FromView
{    
    
    private $orders;

    public function __construct($products = [], $headings = []){
        $this->products = $products;
        
    }

    public function view(): View
    {
        return view('exports.orders_products', [
            'products' => $this->products
        ]);
    }

   
}

// namespace App\Exports;

// use App\Invoice;
// use App\orderstatuses;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;

// class OrdersExport implements FromView
// {
//     public function view(): View
//     {
//         return view('exports.orders', [
//             'orders' => orderstatuses::all()
//         ]);
//     }
// }
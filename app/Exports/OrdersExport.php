<?php

namespace App\Exports;

use App\Invoice;
use App\orderstatuses;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class OrdersExport implements FromView
{    
    
    private $orders;

    public function __construct($orders = [], $headings = []){
        $this->orders = $orders;
        
    }

    public function view(): View
    {
        return view('exports.orders', [
            'orders' => $this->orders
        ]);
    }

   
}

// namespace App\Exports;

// use App\Invoice;
// use App\orderstatuses;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;

// class OrdersExport implements FromView
// {
//     public function view(): View
//     {
//         return view('exports.orders', [
//             'orders' => orderstatuses::all()
//         ]);
//     }
// }
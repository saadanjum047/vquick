<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class ProductExport implements FromView
{    
    
    private $products;

    public function __construct($products = [], $headings = []){
        $this->products = $products;
        
    }

    public function view(): View
    {
        return view('exports.products', [
            'products' => $this->products
        ]);
    }

   
}
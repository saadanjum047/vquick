<?php

namespace App\Exports;

use App\Invoice;
use App\orderstatuses;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class CustomerExport implements FromView
{    
    
    private $customers;

    public function __construct($customers = [], $headings = []){
        $this->customers = $customers;
        
    }

    public function view(): View
    {
        return view('exports.customers', [
            'customers' => $this->customers
        ]);
    }

   
}
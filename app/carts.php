<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carts extends Model
{
    protected $fillable = [
        'image', 'name', 'description','price','quantity','available','category','cid','is_approved','order_id'
    ];


    protected static function boot()
    {
        
        static::retrieved(function ($cart) {

            if($cart->product && $cart->is_approved == 0){
                $cart->original_price = $cart->product->price * $cart->quantity;
            }

            if($cart->is_approved == 0 && $cart->customer){
                    
                if(isset($_SESSION["id"]) && $cart->customer->has_discount == 1){
                    
                    // dd('event');
                    $discount = Discount::first();
            
                    $cart->price = (($cart->product->price / 100 ) * ( 100 - $discount->percentage )) * $cart->quantity;


                    $cart->original_price = $cart->product->price * $cart->quantity;
                    
                    
                    // dd($cart);
                    // $cart->price = $cart->price - ($cart->price * $discount->percentage/100);

                    $cart->discount = $discount->percentage;
                    // selling price = actual price - (actual price * (discount / 100))
                    $cart->save(); 
                }else{
                    $cart->price = $cart->product->price * $cart->quantity ;
                    $cart->discount = null ;
                    $cart->save(); 

                }
            }
        });
        parent::boot();
    }


    public function manageDiscount(){
        
                $discount = Discount::first();
                $this->price = (($this->product->price / 100 ) * ( 100 - $discount->percentage )) * $this->quantity;

                $this->discount = $discount->percentage;
                $this->save();
                return $this; 
        }




    public function order_status(){
        return $this->belongsTo(orderstatuses::class , 'order_id');
    }
    

    public function product(){
        return $this->belongsTo(products::class , 'name' , 'name' ) ;
    }
    
    public function customer(){
        return $this->belongsTo(regusers::class , 'cid');
    }


}
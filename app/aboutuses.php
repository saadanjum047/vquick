<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aboutuses extends Model
{
    protected $fillable = [
        'title', 'description' , 'title_ar', 'description_ar', 'image'
    ];
}

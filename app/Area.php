<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $guarded = [];

    protected $casts = [
        'charges' => 'integer',
    ];

    public function getProperAreaAttribute($value){

        if(session('locale') == 'ar'){
            return $this->ar_area;
        }else{
            return $this->area;
        }
    }

}

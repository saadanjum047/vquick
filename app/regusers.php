<?php

namespace App;

use Carbon\Carbon;
use App\Jobs\SendDiscountMail;
use Illuminate\Database\Eloquent\Model;

class regusers extends Model
{
    // protected $fillable = [
    //     'email','password','full_name','phone','address','zone','street','building' , 'is_guest'
    // ];

    protected $guarded = [];


    protected static function boot()
    {
        
        static::retrieved(function ($customer) {

            // dd(Carbon::parse($customer->discount_end_date) , Carbon::now() , Carbon::parse($customer->discount_end_date) <= Carbon::now());
            if($customer->has_discount == 1 && Carbon::parse($customer->discount_end_date) <= Carbon::now() ){
                $customer->has_discount = 0;
                $customer->discount_end_date = null;
                $customer->save();
            }

            if($customer->is_guest == 0){

                $discount = Discount::first();

                if(isset($discount) && $customer->new_orders->count() >= $discount->orders ){
                    
                    // dd('discount given' , $customer->new_orders );
                    // $customer->orders()->delete();

                    $customer->has_discount = 1;
                    $customer->discount_end_date = Carbon::now()->addDays(30)->toDateString();
                    $customer->save();
                    foreach($customer->new_orders as $order){
                        $order->counted_for_discount = 1;
                        $order->save();
                    }

                    dispatch(new SendDiscountMail($customer ));

                }
            }


            // if($customer->is_guest == 1 && $customer->has_discount == 1){
            //     $customer->has_discount = 0;
            //     $customer->discount_end_date = null;
            //     $customer->save();
            // }
            });
        parent::boot();
    }


    public function orders(){
        return $this->hasMany(orderstatuses::class , 'cid');
    }

    public function area(){
        return $this->belongsTo(Area::class);
    }

    public function new_orders(){
        
        // return $this->hasMany(orderstatuses::class , 'cid')->whereRaw('payment_method == sadad' AND 'payment_status == paid')->orWhere('payment_method' , 'cash on delivery')->where('counted_for_discount' , 0);
        return $this->hasMany(orderstatuses::class , 'cid')->where([
                ['payment_method' , 'sadad'] ,
                ['payment_status', 'paid'] 
            ])->orWhere([
                ['payment_method' , 'cash on delivery'] , 
                ['payment_status' , 'paid'] , 
                ['status', 'delivered']
            ])->where('cid' , $this->id)
            ->where('counted_for_discount' , 0);

        
        // return $this->hasMany(orderstatuses::class , 'cid')->where([['payment_method' , 'sadad'] ,['payment_status', 'paid'] ])->orWhere([['payment_method' , 'cash on delivery'] , ['payment_status' , 'paid'] , ['status', 'delivered']] )->where('counted_for_discount' , 0);
       
    }

}

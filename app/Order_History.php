<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_History extends Model
{
    protected $table = 'orders_history';
    protected $guarded = [];

    public function customer(){
        return $this->belongsTo(reguser::class , 'cid');
    }
}

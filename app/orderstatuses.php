<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class orderstatuses extends Model
{
    protected $guarded = [
        // 'total_price','cid','payment_method','status','area_id'
    ];



    public function area(){
        return $this->belongsTo(Area::class);
    }


    public function customer(){
        return $this->belongsTo( regusers::class, 'cid');
    }

    public function carts(){
        return $this->hasMany(carts::class , 'order_id');
    }

    
}

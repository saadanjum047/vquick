<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class QueueWorker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:queue_worker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A Command to run the Queues on the server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // dd('asd');
        Artisan::call('queue:restart');
        Artisan::call('queue:work');
        // Artisan::call('queue:work --daemon');
    }
}

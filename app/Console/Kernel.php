<?php

namespace App\Console;

use App\regusers;
use App\orderstatuses;
use App\Jobs\SendCustomerEmail;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        
        // if (stripos((string) shell_exec('ps xf | grep \'[q]ueue:work\''), 'artisan queue:work') === false) {
        //     $schedule->command('queue:work --daemon --tries=3 --timeout=5');
        // }

        // $schedule->command('command:queue_worker')
        
        $schedule->command('queue:restart');

        $schedule->command('queue:work --tries=3');
        // ->withoutOverlapping();

        // $schedule->command('queue:work --daemon --tries=3 --timeout=5')
        // ->cron('* * * * * *')
        // ->withoutOverlapping();

        // $schedule->command('queue:restart')
        //     ->everyFiveMinutes();

        // $schedule->command('queue:work --daemon')
        //     ->everyMinute()
        //     ->withoutOverlapping();

        // $schedule->command('queue:restart');
        // $schedule->command('queue:work', ['--daemon']);  


        // if (!strstr(shell_exec('ps xf'), 'php artisan queue:work')) {
            
        //     $schedule->command('queue:restart');
        //     $schedule->command('queue:work');
        //             //  ->everyMinute();
        // }

        


        // $schedule->command('queue:work');
        // $schedule->command('queue:work --daemon');
        // nohup php artisan queue:work --daemon &
        // $schedule->call(function(){
        //     Artisan::call("queue:restart");
        //     Artisan::call("queue:work -daemon &");    
        // });

        // $schedule->call(function(){
        //     $order = orderstatuses::latest()->first();

        //     $customer = regusers::where('id' ,$order->cid )->get();
        
        //     $message = 'Dear '. $order->customer->full_name.', we have received order # ' . $order->id .' and are working on it now.<br></p><p style="color: #242424;">We will email you an update when we have delivered it.<br>';
        
        //     $subject = 'Your vQuick Order is submitted';
        
        //     dispatch(new SendCustomerEmail($customer , $message , $subject , $order ));
        //         })->everyMinute();

    }



    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

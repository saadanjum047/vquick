<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\homepages;
class HomepagesController extends Controller
{
    public function index()
    {
        $homepages = homepages::latest()->simplePaginate(5);
        return view('homepages.index',compact('homepages'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('homepages.create');
    }
  
    public function store(Request $request)
    {
        if($request->hasfile('image'))
        {
           foreach($request->file('image') as $file)
           {
               $name=$file->getClientOriginalName();
               $file->move(public_path().'/files/', $name);  
               $data[] = $name;  
           }
        }
        $homepages= new homepages();
        $homepages->image=json_encode($data);
        $homepages->description = request('description');
        $homepages->title = request('title');
        $homepages->save();
        return redirect()->route('homepages.index')
                        ->with('success','Home Page created successfully.');
    }

    public function show(homepages $homepage)
    {
        return view('homepages.show',compact('homepage'));
    }
   
    public function edit(homepages $homepage)
    {
        return view('homepages.edit',compact('homepage'));
    }
    
    public function update(Request $request, homepages $homepage)
    {
  
        $homepage->update($request->all());
  
        return redirect()->route('homepages.index')
                        ->with('success','Home Page updated successfully');
    }

    public function destroy(homepages $homepage)
    {
        $homepage->delete();
  
        return redirect()->route('homepages.index')
                        ->with('success','Home Page deleted successfully');
    }
}

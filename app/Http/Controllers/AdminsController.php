<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
class AdminsController extends Controller
{
    public function index()
    {
        $admins = User::all();
        return view('admins.index',compact('admins'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

   
    public function create()
    {
        return view('admins.create');
    }

    public function store(Request $request)
    {
        $admins= new User();
        $admins->email = request('email');
        $admins->password = Hash::make(request('password'));
        $admins->name = request('name');
        $admins->save();
        return redirect()->route('admins.index')
            ->with('success','Admin created successfully.');
    }

    public function show(User $admin)
    {
        return view('admins.show',compact('admin'));
    }
   
    public function edit(User $admin)
    {
        return view('admins.edit',compact('admin'));
    }
    
    public function update(Request $request, User $admin)
    {
  
        $admin->update($request->all());
  
        return redirect()->route('admins.index')
                        ->with('success','Admin updated successfully');
    }

    public function destroy(User $admin)
    {
        
        $admin->delete();
  
        return redirect()->route('admins.index')
                        ->with('success','User deleted successfully');
    }
}

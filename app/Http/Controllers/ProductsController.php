<?php

namespace App\Http\Controllers;

use App\products;
use App\categories;
use App\productsections;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Exports\ProductExport;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image;

class ProductsController extends Controller
{
    public function index()
    {

        $products = products::orderBy('product_order' , 'asc')->get();
        
        // $products = products::orderBy('product_order' , 'asc')->simplePaginate(15);
        // dd($products->pluck('id'));

        return view('products.index',compact('products'));

        // return view('products.index',compact('products'))
        //     ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('products.create');
    }
  
    public function store(Request $request)
    {


        // dd($request->all());

   
        $request->validate([
            'name' => 'required',
        ]);

        $products= new products();
        $products->description = request('description');
        $products->description_ar = request('description_ar');
        $products->name = request('name');
        $products->arabic_name = request('arabic_name');
        $products->price = request('price');
        $products->stock_available = request('stock_available');
        $products->category = request('category');
        $products->is_approved = request('is_approved');
        $products->product_order = products::all()->count();;

        
        if($request->has('images')){
            $all_images = array();

            foreach($request->images as $image){
                
                $file = Image::make($image);
                // $file->resize( 300, null, function ($constraint) {
                //     $constraint->aspectRatio();
                // });

                $name = Str::random(12) . '.'. $image->getClientOriginalExtension();
                $file->save('files/' . $name , 90);    
                $all_images[] = $name;
            }
            $products->images = isset($all_images) ? json_encode($all_images) : null ;
            $products->image = isset($all_images) ? $all_images[0] : null ;

        }
        
        

        // if($request->has('image')){
        //     $file = $request->file('image');
        //     // $name=$file->getClientOriginalName();
            
        //     $file = Image::make($request->file('image'));
            
        //     $file->resize( 300, null, function ($constraint) {
        //         $constraint->aspectRatio();
        //     });

        //     $name = time() . '_' .$products->id . '.'. $request->image->getClientOriginalExtension();

        //     $file->save('files/' . $name , 90);    
        //     // $file->move(public_path().'/files/', $name);
            
        //     $products->image=$name;

        //     // $img = Image::make(public_path() .'/files/'.$products->image);

        //     // $img->resize(300, null, function ($constraint) {
        //     //     $constraint->aspectRatio();
        //     // });
    
        // }
        
        $products->save();

        return redirect()->route('products.index')
                        ->with('success','Products created successfully.');
    }

    public function show(products $product)
    {
        return view('products.show',compact('product'));
    }


    public function showProducts(Request $request){

        $products = products::where('is_approved', '1')->orderBy('product_order' , 'asc')->take(3)->get();

        // dd($request->all());
        
        if ($request->ajax()) {

            
        $productsection = productsections::first(); 

        if( $productsection && $productsection->content == 'categories'){

            $categories = categories::orderBy('cat_order')->active()->skip($request->skip)->take(4)->get();

            foreach($categories as $category){
                if( session('locale') == 'ar' ){
                    $category->proper_name = $category->arabic_name;
                    $category->proper_description = $category->arabic_name;
                }else{
                    $category->proper_name = $category->name;
                    $category->proper_description = $category->description;
                }
            }
            return [
                'categories' => $categories ,
                // 'products' => view('product')->with(compact('products'))->render(),
                // 'next_page' => $products->nextPageUrl()
            ];

        }else{


            $products = products::skip($request->skip)->orderBy('product_order' , 'asc')->take(4)->get();

            foreach($products as $product){
                // $product->javascript_product = (string)(json_encode($product));
                if( session('locale') == 'ar' ){
                    $product->proper_name = $product->arabic_name;
                    $product->proper_description = $product->arabic_name;
                }else{
                    $product->proper_name = $product->name;
                    $product->proper_description = $product->description;
                }
            }
            return [
                'products' => $products ,
                // 'products' => view('product')->with(compact('products'))->render(),
                // 'next_page' => $products->nextPageUrl()
            ];

        }

    
        }

        $productsection = productsections::first(); 

        if( $productsection && $productsection->content == 'categories'){
            $categories = categories::orderBy('cat_order')->active()->get()->take(3);
            if($request->show_all){
                
                $categories = categories::orderBy('cat_order')->active()->get();
                $categories->show_all = 1;
            }

            $productsection = productsections::first();

            return view('product',compact('categories' , 'productsection'));
            
        }else{

            // $products = products::all()->where('is_approved', '1');
            if($request->show_all){
                $products = products::where('is_approved', '1')->orderBy('product_order' , 'asc')->get();
                $products->show_all = 1;
            }else{
                $products = products::where('is_approved', '1')->orderBy('product_order' , 'asc')->get()->take(3) ;
            }

            $productsection = productsections::first();

            return view('product',compact('products' , 'productsection'));

        }

        
        return redirect()->back()->with('error' , 'Some Problem ');

        // return view('product',compact('products'));
        // $category_name=request('category');
        // $products = products::all()->where('category', $category_name)->where('is_approved', '1');

    }
    
    public function show1(Request $request)
    {
        
        $category_name=request('category');
        $products = products::all()->where('category', $category_name)->where('is_approved', '1');

        return view('product',compact('products'));
    }
   
    public function showCategoryProducts(Request $request)
    {
        
        $category_name=request('category');
        $products = products::where('category', $category_name)->where('is_approved', '1')->get()->sortBy('product_order') ;

        // dd($products->pluck('id'));
        // $products = products::latest()->where('category', $category_name)->where('is_approved', '1')->get() ;
        // $products = products::all()->where('category', $category_name)->where('is_approved', '1');

        // dd($products , $category_name );

        return view('product',compact('products'));
    }
   
    public function edit(products $product)
    {
        return view('products.edit',compact('product'));
    }
    
    public function update(Request $request, products $product)
    {
  
     

        // if($request->has('image')){
        //     $file = Image::make($request->file('image'));
        //     $file->resize( 300, null, function ($constraint) {
        //         $constraint->aspectRatio();
        //     });
        //     $name = time() . '_' .$product->id . '.'. $request->image->getClientOriginalExtension();
        //     $file->save('files/' . $name , 90);    
        //     // $file->move(public_path().'/files/', $name);
        //     $product->image=$name;
        // }


        if($request->deleted_images){
            $current_images = json_decode($product->images);
            foreach($request->deleted_images as $deleted){
                
            $del = public_path().'/files/'.$current_images[$deleted];
                if(file_exists($del)){
                    File::delete($del);
                }
                unset($current_images[$deleted]);
            }
            $current_images = array_values($current_images);
            // dd($current_images);

            $product->images  = json_encode($current_images);
            
        }
    
        // uploading images
        if($request->has('images')){
            $all_images = (isset($product->images) && is_array(json_decode($product->images)) ) ?  json_decode($product->images) : array() ;

            foreach($request->images as $image){
                    
                $file = Image::make($image);
                // $file->resize( 300, null, function ($constraint) {
                //     $constraint->aspectRatio();
                // });

                $name = Str::random(12) . '.'. $image->getClientOriginalExtension();
                $file->save('files/' . $name , 90);    
                $all_images[] = $name;
            }
            $product->images = isset($all_images) ? json_encode($all_images) : null ;
        }


        if(isset($product->images) && count(json_decode($product->images)) > 0 ){
            $product->image = json_decode($product->images)[0];
        }


        $product->description = request('description');
        $product->description_ar = request('description_ar');
        $product->name = request('name');
        $product->arabic_name = request('arabic_name');
        $product->price = request('price');
        $product->stock_available = request('stock_available');
        $product->category = request('category');
        $product->is_approved = request('is_approved');
        $product->save();


        // $product->update($request->all());

  
        
        return redirect()->route('products.index')
                        ->with('success','Products updated successfully');
    }

    public function destroy(products $product)
    {
        $product->delete();
        return redirect()->route('products.index')
                        ->with('success','Products deleted successfully');
    }


    public function exportProducts($ids)
    {
        $ids = json_decode($ids);

        $products = products::whereIn('id' , $ids )->get();

        return Excel::download(new ProductExport($products), 'products.xlsx');
    }


    public function updateOrder(Request $request){
        
        // dd($request->all());
        
        $products_ids = $request->ids;
        foreach($products_ids as $id){
            $product = products::find($id);
            $product->product_order = array_search($id, $products_ids);
            $product->save();
        }

        return response()->json(['success' => true , 'message' => "Product Order updated"]);


    }



}

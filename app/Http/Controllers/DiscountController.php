<?php

namespace App\Http\Controllers;

use App\Discount;
use App\regusers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Jobs\SendDiscountMail;
use App\Jobs\SendCustomerEmail;

class DiscountController extends Controller
{
    public function index(){
        $discount = Discount::first() ?? null;
        
        return view('discount.index' , compact('discount'));
    }


    public function update(Request $request){

        $request->validate([
            'orders' => 'required | numeric | min:1',
            'discount' => 'required | numeric | min:0.1'
        ]);

        $discount = Discount::first() ?? null;

        if($discount){
            $discount->orders = $request->orders;
            $discount->percentage = $request->discount;
            $discount->save();
        }else{
            $discount = Discount::create([
                'orders' => $request->orders,
                'percentage' => $request->discount,
            ]);
        }

        return redirect()->back()->with('success' , 'Discount Updated');
    }




    public function giftDiscount($customer_id){
        
        $customer = regusers::find($customer_id);
        $customer->has_discount = 1;
        $customer->discount_end_date = Carbon::now()->addDays(30)->toDateString();
        $customer->save();

        dispatch(new SendDiscountMail($customer ));


        return redirect()->back()->with('message' , 'Discount Gifted');


    }


    public function removeDiscount($customer_id){
        
        $customer = regusers::find($customer_id);
        foreach($customer->new_orders as $order){
            $order->counted_for_discount = 1;
            $order->save();
        }

        
        $customer->has_discount = 0;
        $customer->discount_end_date = null;
        $customer->save();

        return redirect()->back()->with('message' , 'Discount Removed');


    }




}

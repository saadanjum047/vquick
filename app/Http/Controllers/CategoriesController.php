<?php
namespace App\Http\Controllers;
use App\products;
use App\categories;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class CategoriesController extends Controller
{
    public function index()
    {
        //$makes = makes::latest()->paginate(5);
        $categories = categories::orderBy('cat_order' , 'asc')->get();
        // $categories = categories::latest()->simplePaginate(5);
        return view('categories.index',compact('categories'));

        // return view('categories.index',compact('categories'))
        //     ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('categories.create');
    }
  
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        // if(isset($request->image)){
        //     $file = $request->file('image');
        //     $name=$file->getClientOriginalName();
        //     $file->move(public_path().'/files/', $name);
        // }
        if($request->has('image')){
            $file = $request->file('image');
            $file = Image::make($request->file('image'));
            $file->resize( 300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $name = time() . '_' . '.'. $request->image->getClientOriginalExtension();
            $file->save('files/' . $name , 90);                
        }


        $categories= new categories();
        $categories->image=$name;
        $categories->description = request('description');
        $categories->description_ar = request('description_ar');
        $categories->restriction_level = request('restriction_level');
        $categories->name = request('name');
        $categories->cat_order = categories::all()->count();
        $categories->arabic_name = request('arabic_name');
        $categories->is_approved = request('is_approved');
        $categories->save();
        return redirect()->route('categories.index')
                        ->with('success','Categories created successfully.');
    }

    public function show(categories $category)
    {
        return view('categories.show',compact('category'));
    }
   
    public function edit(categories $category)
    {
        return view('categories.edit',compact('category'));
    }
    public function update(Request $request, categories $category)
    {
        // dd($request->all());

      
      products::where('category' , $category->name )->update(array('category' => $request->name ));

        $category->update($request->all());

        if($request->has('image')){
            $file = $request->file('image');
            $file = Image::make($request->file('image'));
            $file->resize( 300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $name = time() . '_' .$category->id . '.'. $request->image->getClientOriginalExtension();
            $file->save('files/' . $name , 90);                
            $category->image = $name;    
            $category->save();    
        }


        // if(isset($request->image)){
        //     $file = $request->file('image');
        //     $name=$file->getClientOriginalName();
        //     $file->move(public_path().'/files/', $name);
            
        //     $category->image = $name;    
        //     $category->save();    

        // }
        return redirect()->route('categories.index')
                        ->with('success','Categories updated successfully');
    }

    public function destroy(categories $category)
    {
        $category->delete();
  
        return redirect()->route('categories.index')
                        ->with('success','Categories deleted successfully');
    }




    public function updateOrder(Request $request){
        
        // dd($request->all());
        
        $categories_ids = $request->ids;
        foreach($categories_ids as $id){
            $category = categories::find($id);
            $category->cat_order = array_search($id, $categories_ids);
            $category->save();
        }

        return response()->json(['success' => true , 'message' => "Categories Order updated"]);


    }


}

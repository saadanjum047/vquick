<?php

namespace App\Http\Controllers;
use App\contactuses;
use Illuminate\Http\Request;
use App\Mail\ContactFormMail;
use Illuminate\Support\Facades\Mail;

class ContactFormController extends Controller
{
    public function create() {
        return view('contact-form.create');
    }

    public function store() {


        $data =request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);
        // $contact_email= request('contact_email');

        if(contactuses::latest()->first()){

            
        $contact_email = contactuses::latest()->first()->email ;
        
        $name= request('name');
        $email= request('email');
        $message1= request('message');

        $subject = 'You have an email from a visitor';
        
        $data['msg'] = 'Dear Admin <br>A Visitor has sent a message <br>Name: <b>'.$name .'</b> <br> Email: <b>'.$email .'</b> <br> Message: '.$message1 .'<br>vQuick' ;

        $email = Mail::send('emails.text_email', $data, function ($message) use ($contact_email , $subject) {
            // $message->from($contact_email, 'Test');
            // $message->sender($email, $name);
            $message->to($contact_email, "Admin");
            $message->subject($subject);
        });

        if (Mail::failures()) {
            // return response showing failed emails
            return redirect()->back()->with('error' , 'Some Problem Occurred') ;
        }
    }
        
        return redirect()->back()->with('success' , 'Your email has been sent') ;

        
    }
}

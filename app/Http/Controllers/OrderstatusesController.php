<?php 
namespace App\Http\Controllers;

use MPDF;
use App\Area;
use App\User;
use App\carts;
use App\Payment;
use App\Discount;
use App\products;
use App\regusers;
use App\shippings;
use App\contactuses;
use Omnipay\Omnipay;
use App\Order_History;
use App\orderstatuses;

use App\Jobs\SendEmail;
use App\Discount_Record;
use App\Exports\CartsExport;

use Illuminate\Http\Request;
use App\Exports\OrdersExport;
use App\Jobs\SendCustomerEmail;
use Illuminate\Support\Facades\Http;

// use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OrdersProductsExport;
// use PDF;
/** Paypal Details classes **/
// use PayPal\Rest\ApiContext;
// use PayPal\Auth\OAuthTokenCredential;
// use PayPal\Api\Amount;
// use PayPal\Api\Details;
// use PayPal\Api\Item;
// use PayPal\Api\ItemList;
// use PayPal\Api\Payer;
// use PayPal\Api\Payment;
// use PayPal\Api\RedirectUrls;
// use PayPal\Api\PaymentExecution;
// use PayPal\Api\Transaction;
// use PayPal\Exception\PayPalConnectionException;





class OrderstatusesController extends Controller
{
    public $gateway;
 
    public function __construct()
    {
        // $this->gateway = Omnipay::create('PayPal_Rest');
        // $this->gateway->setClientId(env('PAYPAL_CLIENT_ID'));
        // $this->gateway->setSecret(env('PAYPAL_CLIENT_SECRET'));
        // $this->gateway->setTestMode(true); //set it to 'false' when go live

        // $this->gateway = Omnipay::create('PayPal_Rest');
        // $this->gateway->setClientId('AVxP-jU0lumxBdqgWJGwc-GydC3vPpRx2X3SrYY_j_b7gsSRy0DCflw6a7w7YA-7YTgxJMpJqAx68H5R');
        // $this->gateway->setSecret('EP0YEH1RwnV1qIw8Vgn21K9vaSEIk7gh1Ybp54kayQwUygeJQoA4-SqWCGIIm3f4B9mrNHVr0ZH1Q28v');
        // $this->gateway->setTestMode(true); //set it to 'false' when go live

    }



    public function index()
    {
        $orderstatuses = orderstatuses::with('area' , 'customer')-> latest()->simplePaginate(15);
        // dd('sad');
        return view('orderstatuses.index',compact('orderstatuses'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
            
    }

    public function search(Request $request)
    {

        // dd($request->all());
        $order_number = request('search_order_number');
        $payment_status = request('search_payment_status');
        $payment_method = request('payment_method');
        $status = request('status');
        $name = request('name');

        $filtered_orders = orderstatuses::all();
        $filters = $request->all();

          
        if($filters['lower_date'] && !$filters['upper_date'] ){
           
            $filtered_orders = orderstatuses::whereDate('created_at', '>=',  $filters['lower_date'])->get();

        }elseif(!$filters['lower_date'] && $filters['upper_date'] ){
           
            $filtered_orders = orderstatuses::whereDate('created_at', '<=',  $filters['upper_date'])->get();

        }elseif($filters['lower_date'] && $filters['upper_date'] ){
           
            $filtered_orders = orderstatuses::where('created_at', '>=', $filters['lower_date'] )->whereDate('created_at' ,'<=', $filters['upper_date'] )->get();

        }else{
            $filtered_orders = orderstatuses::all();
        }



        if($request->name){
            $all_words = explode(' ' , $request->name );

            if($all_words){
            
                $filtered_regusers = regusers::where(function ($user) use ($all_words) {
                    foreach($all_words as $word){
                        $user->where('full_name','like', '%' . $word . '%');
                        }
                    })->get();
            }   
        }
        
        
        // dd($request->all());
        


        if($name){
            $filtered_orders = $filtered_orders->whereIn('cid', $filtered_regusers->pluck('id')->toArray() );
        }

        if($order_number){
            $filtered_orders = $filtered_orders->where('id', $order_number);
        }
        if($payment_status){
            $filtered_orders = $filtered_orders->where('payment_status', $payment_status);
        }
        if($status){
            $filtered_orders = $filtered_orders->where('status', $status);
        }
        
        if($payment_method){
            $filtered_orders = $filtered_orders->where('payment_method', $payment_method);
        }
        // dd($request->all() , $filtered_orders);
     
        // if($order_number ==null && $status== null ) {
        //     $orderstatuses = orderstatuses::all()->where('payment_method', $payment_method);
        // }
        // else if($payment_method ==null && $status== null) {
        //     $orderstatuses = orderstatuses::all()->where('id', $order_number);
        // }
        // else {
        //     $orderstatuses = orderstatuses::all()->where('status', $status);
        // }
        return view('orderstatuses.index',compact('filtered_orders' , 'filters'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function search1(Request $request)
    {

        // dd($request->all());

        $orderstatus = orderstatuses::findOrFail(request('oid'));


        $product_name = request('product_name');
        $oid = request('oid');
        $carts= new carts();
        $carts = carts::all()->where('is_approved', '1')->where('order_id', $oid)->where('name', $product_name);
        
        return view('orderstatuses.show',compact('orderstatuses','carts' , 'product_name' , 'orderstatus'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function show(orderstatuses $orderstatus)
    {


        $carts= new carts();
        $carts = carts::with('product')->where('is_approved', '1')->where('order_id', $orderstatus->id)->get();

        $products = products::whereIn('name' , $carts->pluck('name')->toArray() )->get() ;
        // dd($carts);

        return view('orderstatuses.show',compact('carts' ,'products' ,'orderstatus'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    
    
    public function sadadPayment(Request $request){
        // dd($request->all() , 'sadad-pay');
        
        // $path = "https://sadadqa.com/webpurchase";
        
        // $data = "merchant_id=7803971&data2=value2";
        
        
        if(!isset($_SESSION['id'] ) && isset($request->guest_purchase) ){
            $carts = carts::where('session', session()->getId() )->where('is_approved', '0')->get();

            foreach($carts as $cart){
                $cart->cid = $request->id;
                $cart->save();
            }

        }elseif(isset($_SESSION['id'] )) {

            $carts = carts::where('cid', $_SESSION['id'])->where('is_approved', '0')->get();
        }else{
            return redirect('userlogin')->with('error' , 'You must login to proceed');
        }


    //     $productdetail = null;
        
    //     foreach($carts as $k => $cart){
    //         $productdetail[$k]['itemname'] = $cart->name;
    //         $productdetail[$k]['quantity'] = $cart->quantity;
    //         $productdetail[$k]['amount'] = $cart->price/$cart->quantity;
    //     }
        
    //     // dd($productdetail);
       
    //   $response = Http::post('https://sadadqa.com/webpurchase', [
    //         'merchant_id' => '7803971',
    //         'secret_key' => 'BCYJENm9z6hxyeaD',
    //         'TXN_AMOUNT' => '1',
    //         'ORDER_ID' => '10001',
    //         'MOBILE_NO' => '03147637613',
    //         'secret_key' => url('sadad-payment-success'),
    //         'productdetail' => $productdetail,
    //     ]);
        
    //     return $response;
        
    //     dd($response);
        
        
        $total_price = $request->total_price;        
        $customer_id = $request->id;
        $order_id = 'order_id13213123';
        
        

        
        foreach($carts as $cart){
            if($cart->product && $cart->product->stock_available < $cart->quantity ){
                return redirect()->back()->with('error' , $cart->name .' stock is not enough');
            }
        }
        
        
        $shipping = shippings::first();

        $merchant_id = '7803971';
        $secret_key = 'BCYJENm9z6hxyeaD';
        
        return view('sadad_payment' , compact('order_id' , 'customer_id' , '$total_price' , 'carts' , 'shipping', 'secret_key' , 'merchant_id'));
    }
    


















    public function store(Request $request)
    {
        
        // dd($request->all());

        if(!isset($_SESSION['id'] ) && !isset($request->guest_purchase) ){
            return redirect('userlogin')->with('error' , 'You must login to proceed');
            // return redirect()->back()->with('error' , 'You must login to proceed');
        }
        
        if(!isset($_SESSION['id'] ) && isset($request->guest_purchase) ){
            $carts = carts::where('session', session()->getId() )->where('is_approved', '0')->get();

            foreach($carts as $cart){
                $cart->cid = $request->id;
                // $cart->manageDiscount();
                $cart->save();
            }
        // dd($carts);
        }elseif(isset($_SESSION['id'] )) {

            $carts = carts::where('cid', $_SESSION['id'])->where('is_approved', '0')->get();
            // $carts = carts::where('cid', $_SESSION['id'])->where('session', session()->getId() )->where('is_approved', '0')->get();
        }else{
            return redirect('userlogin')->with('error' , 'You must login to proceed');
        }

        if($carts->count() == 0){
            return redirect('/')->with('error' , 'No Products in the Cart');
        }

        // dd($carts);
        
        // dd($carts , $request->all() );
        foreach($carts as $cart){
            
            $cart_check = $carts->where('name' , $cart->name );
            if($cart_check->count() > 1){
                // dd($cart_check[$cart_check->count()-1]);
                $cart_check->last()->delete();
                // dd();
            }else{
                // dd('No doubles');
            }

            if($cart->product && $cart->product->stock_available < $cart->quantity ){
                return redirect()->back()->with('error' , $cart->name .' stock is not enough');
            }
        }





        $_SESSION['preffered_payment_method'] = $request->payment_method;

       


        if($request->payment_method == 'PayPal'){



        try {
            $response = $this->gateway->purchase(array(
                'amount' =>  (int)number_format($request->total_price , 2) ,
                'currency' => 'USD' ,
                'returnUrl' => url('paymentsuccess') . '?id='.$request->id ,
                // 'returnUrl' => '/paymentsuccess?user_id=111&order_id=999',
                'cancelUrl' => url('paymenterror'),
            ))->send();
      
            if ($response->isRedirect()) {
                $response->redirect(); // this will automatically forward the customer
            } else {
                // not successful

                return $response->getMessage();
            }
        } catch(Exception $e) {
            
            return $e->getMessage();
        }
    
        }else{


        // dd('asd');
        // $shipping = shippings::first();

        // $total_price = $shipping->fee + request('total_price'); 

        $orderstatuses= new orderstatuses();
        // $orderstatuses->total_price = request('total_price');

        $last_order = orderstatuses::all() -> last();
    

        // $orderstatuses->total_price = $total_price;
        $orderstatuses->area_id = request('shipping_charge');
        $orderstatuses->shipping_charges = Area::find(request('shipping_charge'))->charges;
        $orderstatuses->total_price = request('total_price');
        $orderstatuses->cid = request('id');
        $orderstatuses->status = 'pending';
        $orderstatuses->payment_method = $request->payment_method ;
        $orderstatuses->payment_status = 'pending';
        $orderstatuses->save();
        $last_id=$orderstatuses->id;
        
        
        $carts = carts::where('cid', $orderstatuses->cid)->where('is_approved', '0')->get();
        // foreach($carts as $cart){
        //     if($cart->product){
        //         $cart->product->stock_available = $cart->product->stock_available - $cart->quantity;
        //         $cart->product->save();
        //         $cart->available = $cart->product->stock_available;
        //         $cart->save();
        //     }
        // }

        
        $carts= new carts();
        $carts = carts::all();
        $carts->order_id = $last_id;
        $carts->is_approved = '1';

        
        
        // bug found
        // $carts = carts::where('cid', $orderstatuses->cid)->where('is_approved', '0')->update(array('order_id' => $carts->order_id,'is_approved' => '1'));
        $carts = carts::where('cid', $orderstatuses->cid)->where('session', session()->getId() )->where('is_approved', '0')->update(array('order_id' => $carts->order_id,'is_approved' => '1'));

        // dd($carts);


        $carts = carts::all()->where('is_approved', '0')->where('cid', $orderstatuses->cid);
        $shippings = shippings::all();
        
        
        // @dd($orderstatuses->carts);
        
        if($request->payment_method == 'sadad'){
        
            $customer_id = $request->id;
        
            $merchant_id = '7803971';
            $secret_key = 'BCYJENm9z6hxyeaD';
            $total_price = $request->total_price;
            
            return view('sadad_payment' , compact('orderstatuses' , 'customer_id' , 'total_price' , 'carts' , 'secret_key' , 'merchant_id'
            ));
            
        }
        

    
        // dd($orderstatuses);
        
          foreach($orderstatuses->carts as $cart){
            if($cart->product){
                $cart->product->stock_available = $cart->product->stock_available - $cart->quantity;
                $cart->product->save();
                $cart->available = $cart->product->stock_available;
                $cart->save();
            }
        }
        

        $customer = regusers::find($request->id );
        if(isset($_SESSION["id"]) && $customer->has_discount == 1 && $customer->is_guest == 0){
            $orderstatuses->discount_availed = 1;
            $orderstatuses->save();

            $discount = Discount::first();
            Discount_Record::create([
                'order_id' => $orderstatuses->id,
                'discount' => $discount->percentage,
            ]);
        }
       
        unset($_SESSION['preffered_payment_method']);
        

        // $customer = regusers::findOrFail ($orderstatuses->cid );
        
        $customer = regusers::where('id' ,$orderstatuses->cid )->get();

        $message = 'Dear '. $orderstatuses->customer->full_name.', we have received order # ' . $orderstatuses->id .' and are working on it now.<br></p><p style="color: #242424;">We will email you an update when we have delivered it.<br>';

        $subject = 'Your vQuick Order is submitted';

        dispatch(new SendCustomerEmail($customer , $message , $subject , $orderstatuses ));
        // dispatch(new SendEmail($customer , $message , $subject , 'emails.general' ));
        
        $admins = User::all();
        
        $subject = 'A new Order is pending';
        $message = 'Dear Admin <br> A new Order is pending. <br> ';
        // <a href="'.url('/').'/orderstatuses/'.$orderstatuses->id.'">View</a> 

        dispatch(new SendCustomerEmail($admins , $message , $subject , $orderstatuses ));
        // dispatch(new SendEmail($admins , $message , $subject , $orderstatuses ));
        
    
      
    
        // if(!isset($_SESSION)) 
        // { 
        //     session_start(); 
        // } 
        // $_SESSION['message'] = "Your Your Order has been submitted. Thank you";
        
        if(isset($_SESSION['id'] )){
            return redirect('order-history')->with('success' , 'Your Order has been submitted. Thank you');
        }else{
            return redirect('/')->with('success' , 'Your Order has been submitted. Thank you');
        }


        }

        return redirect('/')->with('error' , 'No Payment Method selected');



        // return view('checkout',compact('carts','shippings'));
    }








        public function sadadPaymentSuccess(Request $request){
            
            
            // array:10 [▼
            //     "website_ref_no" => "7803971"
            //     "transaction_status" => "3"
            //     "transaction_number" => "SD2414657280764"
            //     "MID" => "7803971"
            //     "RESPCODE" => "1"
            //     "RESPMSG" => "Txn Success"
            //     "ORDERID" => "10001"
            //     "STATUS" => "TXN_SUCCESS"
            //     "TXNAMOUNT" => "1.00"
            //     "checksumhash" => "q3zGVZEUomCu6WV9/RDrLKZHSFCYOv71iaee+8rpMRg7aF56Ye7IM46er7oWkavWPohc+4kWYF6aa/i7x4use6HRhGMMrB5BrrzPbC6iacU="
            //     ]
            $orderstatuses = orderstatuses::find($request->ORDERID);
            // dd($orderstatuses->total_price , array_sum($orderstatuses->carts->pluck('price')->toArray()) , array_sum($orderstatuses->carts->pluck('original_price')->toArray()) , $orderstatuses->total_price != array_sum($orderstatuses->carts->pluck('price')->toArray() ) ) ;
            // dd($orderstatuses);
            
            if($request->RESPCODE == 1){
                
            if($orderstatuses && $orderstatuses->payment_method == 'sadad' && $orderstatuses->payment_status != 'paid'){
                
            // dd($orderstatuses);

            $customer = $orderstatuses->customer;
            
            // $customer = regusers::where('id' ,$orderstatuses->cid )->get();
            
            foreach($orderstatuses->carts as $cart){
                if($cart->product){
                    $cart->product->stock_available = $cart->product->stock_available - $cart->quantity;
                    $cart->product->save();
                    $cart->available = $cart->product->stock_available;
                    $cart->save();
                }
            }

            unset($_SESSION['preffered_payment_method']);

            // $orderstatuses->payment_method = 'sadad';
            // $orderstatuses->total_price = array_sum($orderstatuses->carts->pluck('price')->toArray());
            $orderstatuses->payment_status = 'paid';
            $orderstatuses->transaction_number = $request->transaction_number;
            $orderstatuses->response = json_encode($request->all());
            $orderstatuses->txn_amount = $request->TXNAMOUNT;

            $customer = regusers::find($orderstatuses->cid );

            // dd($customer);

            if(isset($_SESSION["id"]) && $customer->has_discount == 1 && $customer->is_guest == 0){
                $orderstatuses->counted_for_discount = 1;
                $orderstatuses->discount_availed = 1;

                $discount = Discount::first();

                Discount_Record::create([
                    'order_id' => $orderstatuses->id,
                    'discount' => $discount->percentage,
                ]);

            }

            $orderstatuses->save();

    
            $message = 'Dear '. $orderstatuses->customer->full_name.', we have received order # ' . $orderstatuses->id .' and are working on it now.<br></p><p style="color: #242424;">We will email you an update when we have delivered it.<br>';
    
            $subject = 'Your vQuick Order is submitted';
            
            $customers = regusers::where( 'id' , $orderstatuses->cid )->get();
            dispatch(new SendCustomerEmail($customers , $message , $subject , $orderstatuses ));
            // dispatch(new SendEmail($customer , $message , $subject , 'emails.general' ));
            
            $admins = User::all();
            
            $subject = 'A new Order is pending';
            $message = 'Dear Admin <br> A new Order is pending. <br> ';
            // <a href="'.url('/').'/orderstatuses/'.$orderstatuses->id.'">View</a> 
    
            dispatch(new SendCustomerEmail($admins , $message , $subject , $orderstatuses ));
            // dispatch(new SendEmail($admins , $message , $subject , $orderstatuses ));
            
            $_SESSION['payment_message'] = 'Your Order has been submitted. Thank you' ;

            
              if(isset($_SESSION['id'] )){
                    return redirect('order-history')->with('success' , 'Your Order has been submitted. Thank you');
                }else{
                    return redirect('/')->with('success' , 'Your Order has been submitted. Thank you');
                }

                }else{
                    

                    $_SESSION['payment_message'] = 'Your Order has been submitted. Thank you' ;
                    
                    
                    if(isset($_SESSION['id'] )){
                        
                    return redirect('/order-history')->with('success' , 'Your Order has been submitted. Thank you');
                    }else{
                        return redirect('/')->with('success' , 'Your Order has been submitted. Thank you');
                    }
                }

            }
            // elseif($request->RESPCODE == 400 || $request->RESPCODE == 402 ){

                
            //     $orderstatuses->transaction_number = $request->transaction_number;
            //     $orderstatuses->txn_amount = $request->TXNAMOUNT;
            //     $orderstatuses->payment_status = 'incomplete';

            //     $orderstatuses->save();
                
            //     foreach($orderstatuses->carts as $cart){
            //         $cart->is_approved = 0;
            //         $cart->save();
            //     }
            // }
                
                else{
        
                // @dd('payment failed' , 'roleback');

                // foreach($orderstatuses->carts as $cart){
                //     if($cart->product){
                //         $cart->is_approved = 0;
                //         $cart->product->stock_available = $cart->product->stock_available + $cart->quantity;
                //         $cart->product->save();
                //         $cart->available = $cart->product->stock_available;
                //         $cart->save();
                        
                //     }
                // }
                
                
                  foreach($orderstatuses->carts as $cart){
                        $cart->is_approved = 0;
                        $cart->save();
                }
                
                
                $_SESSION['payment_message'] = 'an error occurred while processing the payment. please try again later' ;

                
                $orderstatuses->delete();

                return redirect('/checkout')->with('error' , 'Payment Failed');

            }
            
    
    
    
        }



    public function payment_success(Request $request)
    {

        // dd($request->all());
        // Once the transaction has been approved, we need to complete it.
        if ($request->input('paymentId') && $request->input('PayerID'))
        {
            $transaction = $this->gateway->completePurchase(array(
                'payer_id'             => $request->input('PayerID'),
                'transactionReference' => $request->input('paymentId'),
            ));
            $response = $transaction->send();
         
            if ($response->isSuccessful())
            {
                // The customer has successfully paid.
                $arr_body = $response->getData();
                // dd($response , $arr_body['id']);
                // Insert transaction data into the database


                $orderstatuses= new orderstatuses();
                // $orderstatuses->total_price = request('total_price');
        
                $last_order = orderstatuses::all() -> last();
            
        
                // $orderstatuses->total_price = $total_price;
                $orderstatuses->total_price = $arr_body['transactions'][0]['amount']['total'];
                $orderstatuses->cid = request('id');
                $orderstatuses->status = 'pending';
                $orderstatuses->payment_method = 'Paid VIA PayPal';
                $orderstatuses->save();
                $last_id=$orderstatuses->id;
                
                $isPaymentExist = Payment::where('payment_id', $arr_body['id'])->first();
         
                if(!$isPaymentExist)
                {
                    $payment = new Payment;
                    $payment->payment_id = $arr_body['id'];
                    $payment->payer_id = $arr_body['payer']['payer_info']['payer_id'];
                    $payment->payer_email = $arr_body['payer']['payer_info']['email'];
                    $payment->amount = $arr_body['transactions'][0]['amount']['total'];
                    $payment->currency = 'QAR';
                    $payment->customer_id = $request->id;
                    $payment->order_id = $orderstatuses->id;
                    $payment->payment_status = $arr_body['state'];
                    $payment->response = json_encode($arr_body);
                    $payment->save();
                }
                
                $carts = carts::where('cid', $orderstatuses->cid)->where('is_approved', '0')->get();
                
                foreach($carts as $cart){
                    if($cart->product){
                        $cart->product->stock_available = $cart->product->stock_available - $cart->quantity;
                        $cart->product->save();
                        $cart->available = $cart->product->stock_available;
                        $cart->save();
                    }
                }
        
                
                $carts= new carts();
                $carts = carts::all();
                $carts->order_id = $last_id;
                $carts->is_approved = '1';
        
                
                
                $carts = carts::where('cid', $orderstatuses->cid)->where('is_approved', '0')->update(array('order_id' => $carts->order_id,'is_approved' => '1'));
        
                $carts = carts::all()->where('is_approved', '0')->where('cid', $orderstatuses->cid);
                $shippings = shippings::all();
        
                $customer = regusers::where('id' ,$orderstatuses->cid )->get();
        
                $message = 'Dear '. $orderstatuses->customer->full_name.', we have received order # ' . $orderstatuses->id .' and are working on it now.<br></p><p style="color: #242424;">We will email you an update when we have delivered it.<br>';
        
                $subject = 'Your vQuick Order is submitted';
        
                dispatch(new SendCustomerEmail($customer , $message , $subject , $orderstatuses ));
                
                $admins = User::all();
                
                $subject = 'A new Order is pending';
                $message = 'Dear Admin <br> A new Order is pending. <br> ';
        
                dispatch(new SendCustomerEmail($admins , $message , $subject , $orderstatuses ));
                
                if(isset($_SESSION['id'] )){
                    return redirect('order-history')->with('success' , "Payment is successful. Your transaction id is: ". $arr_body['id']);
                }else{
                    return redirect('/')->with('success' , "Payment is successful. Your transaction id is: ". $arr_body['id']);
                }
         
                // return "Payment is successful. Your transaction id is: ". $arr_body['id'];
            } else {
                return $response->getMessage();
            }
        } else {
            return 'Transaction is declined';
        }
    }
 
    public function payment_error()
    {
        return 'User is canceled the payment.';
    }



    public function edit(orderstatuses $orderstatus)
    {
        return view('orderstatuses.edit',compact('orderstatus'));
    }
    
    public function update(Request $request, orderstatuses $orderstatus)
    {

        // dd($request->all() , $orderstatus);
        // $orderstatus->update($request->all());
  
        if($request->status == 'delivered' && $orderstatus->payment_method != 'sadad' ){
            $orderstatus->payment_status = $request->payment_method;
        }
        $orderstatus->status = $request->status;
        $orderstatus->save();

        // dd($request->all() , $orderstatus);


        if( $orderstatus) {

            // dd($orderstatus->carts);
            $customer = $orderstatus->customer;

            // $data['msg'] = 'Dear '.$customer->full_name .'<br>Your Order Status is now '.$request->status;

            if(!is_null(shippings::first())){
                $data['shipping'] =  shippings::first() ;
            }
            

            $customer = regusers::find($orderstatus->cid );

            if($orderstatus->payment_method == 'cash on delivery' && $orderstatus->payment_status == 'paid'  && $orderstatus->payment_status == 'delivered' ){

                if($customer->has_discount == 1 && $customer->is_guest == 0){

                    $orderstatus->counted_for_discount = 1;
                    $orderstatus->discount_availed = 1;
                    $orderstatus->save();
                    
                    $discount = Discount::first();
                    
                    Discount_Record::create([
                        'order_id' => $orderstatus->id,
                        'discount' => $discount->percentage,
                        ]);
                        
                    }
                }

            if($request->status == 'cancelled'){
                
                foreach($orderstatus->carts as $cart){
                    // dump($cart->product);
        
                    $cart->product->stock_available = $cart->quantity + $cart->product->stock_available;
                    $cart->available = $cart->quantity + $cart->product->stock_available;
                    $cart->save();
                    $cart->product->save();
                }

                $message = 'Dear '. $orderstatus->customer->full_name.', We have cancelled your order # ' . $orderstatus->id .'.<br></p><p style="color: #242424;">Please Contact for further details.<br>';
            }else{
                $message = 'Dear '. $orderstatus->customer->full_name.', we have received order # ' . $orderstatus->id .' and are working on it now.<br></p><p style="color: #242424;">We will email you an update when we have delivered it.<br>';
            }

            $subject = 'You Order Status is '. ucwords($orderstatus->status)  ;
            
            $customer = regusers::where('id' ,$orderstatus->cid )->get();
 
            dispatch(new SendCustomerEmail($customer , $message , $subject , $orderstatus ));

        }

        return redirect()->route('orderstatuses.index')
                        ->with('success','Order updated successfully');
    }

    public function destroy(orderstatuses $orderstatus)
    {

        // dd($orderstatus);

        
        if($orderstatus->status != 'cancelled'){
        foreach($orderstatus->carts as $cart){
            // dump($cart->product);

            $cart->product->stock_available = $cart->quantity + $cart->product->stock_available;
            $cart->product->save();
        }
        }
        $orderstatus->delete();
  
        return redirect()->back()
        ->with('success','Order deleted successfully');

        // return redirect()->route('orderstatuses.index')
        //                 ->with('success','Order deleted successfully');
    }


    public function exportOrders($ids)
    {
        $ids = json_decode($ids);

        $orders = orderstatuses::whereIn('id' , $ids )->get();

        return Excel::download(new OrdersExport($orders), 'orders.xlsx');
    }



    public function exportOrdersProducts($ids)
    {
        $ids = json_decode($ids);

        // $orders = orderstatuses::whereIn('id' , $ids )->where('payment_status', 'paid')->get();
        $orders = orderstatuses::whereIn('id' , $ids )->get();

        $sold_products = collect();

        foreach($orders as $order){
            // dd($order->carts);
            // dump($order->carts->pluck('name')->toArray() , $order->carts->pluck('quantity')->toArray());
            foreach($order->carts as $cart){
                // $sold_products->push($cart);

                if($sold_products->contains( 'name' , $cart->name)){
                    
                    $sold_products->where( 'name' , $cart->name )->first()->total_quantity += $cart->quantity;
                }else{
                    $sold_products->push( $cart );
                    // dump($sold_products->last() , $cart->quantity);
                    $sold_products->last()->total_quantity = $cart->quantity;
                }
                }
        }
        // dd($sold_products);

        
        // foreach($orders as $k => $order){
        //     // dump($order->carts->pluck('name')->toArray() , $order->carts->pluck('quantity')->toArray());
        //     foreach($order->carts as $cart){
        //         if($sold_products->contains( 'name' , $cart->name)){
                    
        //             $sold_products->where( 'name' , $cart->name )->first()->total_quantity += $cart->quantity;
        //         }else{
        //             $sold_products->push( $cart->product );
        //             dd($sold_products->last());
        //             $sold_products->last()->total_quantity = $cart->quantity;
        //         }
        //         }
        // }

        foreach($sold_products as $product){
            $product->total_price = $product->total_quantity * $product->price;
        }
        // dd($sold_products);
        
        return Excel::download(new OrdersProductsExport($sold_products), 'products_report.xlsx');
    }

    public function exportOrder($id)
    {
        // $ids = json_decode($ids);

        $order = orderstatuses::find( $id );

        return Excel::download(new CartsExport($order), 'order_details.xlsx');
    }

    public function exportInvoice($id)
    {
        // $ids = json_decode($ids);

        $order = orderstatuses::find( $id );

        // tcpdf liberary which does not correctly interprets css design
        // PDF::SetTitle('Hello World');
        // PDF::AddPage();
        // // PDF::Write(0, 'Hello World');
        // PDF::writeHTML(view('pdf.pdf_invoice2', [ 'order' => $order])->render());
        // PDF::Output('hello_world.pdf');

        // $pdf = PDF::loadView('pdf.document', $data);
		// return $pdf->stream('document.pdf');


        $pdf = MPDF::loadView('pdf.pdf_invoice2', [ 'order' => $order , 'msg' => 'Message' ]);
        return $pdf->stream();

        
        // $pdf = PDF::loadView('pdf.pdf_invoice', [ 'order' => $order , 'msg' => 'Message' ]);
        // return $pdf->stream('invoice.pdf');
        // return $pdf->download('invoice.pdf');

    }


}


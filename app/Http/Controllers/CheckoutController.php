<?php

namespace App\Http\Controllers;

use App\Area;
use App\carts;
use App\Discount;
use App\regusers;
use App\shippings;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function viewCart(){
        if( isset($_SESSION["logged_in"])) {
            $id = $_SESSION["id"];
        }
        //$carts = carts::where(['cid', $id],['is_approved', '0']);
        // $carts = carts::all()->where('is_approved', '0')->where('cid', $id);
    
       
    
        // if( isset($_SESSION["logged_in"])) {
        // }
            
            $session_carts = carts::where('session' , session()->getId() )->where('is_approved' , 0) ->get();
    
            // dd($session_carts);
    
            if( isset($_SESSION['id']) && $session_carts->count() > 0){
    
                $customer = regusers::find($_SESSION['id']);
    
                foreach($session_carts->where('cid' , null ) as $cart){
                    $cart->cid =  $_SESSION['id'];
                    $cart->save();
                }
    
                $carts = carts::where('cid' , $id )->where('is_approved' , 0) ->get();
                
            }elseif(isset($_SESSION['id']) && $session_carts->count() == 0){
                
                $customer = regusers::find($_SESSION['id']);
    
                $carts = carts::where('cid' , $id )->where('is_approved' , 0) ->get();
            }
            else{
                $carts = $session_carts;
            }
    
            // dd($carts);
    
            $shippings = shippings::all();
            $discount = Discount::first();
            $areas = Area::all();
            
            return view('checkout',compact('carts','shippings', 'discount' , 'areas' ));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\aboutuses;
class AboutusesController extends Controller
{
    public function index()
    {
        $aboutuses = aboutuses::latest()->simplePaginate(5);
        return view('aboutuses.index',compact('aboutuses'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('aboutuses.create');
    }
  
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'title_ar' => 'required',
        ]);

        if($request->has('image')){

            $file = $request->file('image');
            $name=$file->getClientOriginalName();
            $file->move(public_path().'/files/', $name);
        }
        $aboutuses= new aboutuses();
        $aboutuses->image=$name;
        $aboutuses->description = request('description');
        $aboutuses->description_ar = request('description_ar');
        $aboutuses->title = request('title');
        $aboutuses->title_ar = request('title_ar');
        $aboutuses->save();
        
        return redirect()->route('aboutuses.index')
                        ->with('success','About Us created successfully.');
    }

    public function show(aboutuses $aboutus)
    {
        return view('aboutuses.show',compact('aboutus'));
    }
   
    public function edit(aboutuses $aboutus)
    {
        return view('aboutuses.edit',compact('aboutus'));
    }
    
    public function update(Request $request, aboutuses $aboutus)
    {

        // dd($request->all());
        $request->validate([
            'title' => 'required',
            'title_ar' => 'required',
        ]);
        // $aboutus->update($request->all());
        if($request->has('image')){
            $file = $request->file('image');
            $name=$file->getClientOriginalName();
            $file->move(public_path().'/files/', $name);
            $aboutus->image=$name;
        }

        $aboutus->description = request('description');
        $aboutus->description_ar = request('description_ar');
        $aboutus->title = request('title');
        $aboutus->title_ar = request('title_ar');
        $aboutus->save();  

        return redirect()->route('aboutuses.index')
                        ->with('success','About Us updated successfully');
    }

    public function destroy(aboutuses $aboutus)
    {
        $aboutus->delete();
  
        return redirect()->route('aboutuses.index')
                        ->with('success','About Us deleted successfully');
    }
}

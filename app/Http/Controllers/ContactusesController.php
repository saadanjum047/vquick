<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\contactuses;
class ContactusesController extends Controller
{
    public function index()
    {
        $contactuses = contactuses::latest()->simplePaginate(5);
        return view('contactuses.index',compact('contactuses'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('contactuses.create');
    }
  
    public function store(Request $request)
    {
        $contactuses= new contactuses();
        $contactuses->phone = request('phone');
        $contactuses->email = request('email');
        $contactuses->save();
        return redirect()->route('contactuses.index')
                        ->with('success','Contact created successfully.');
    }

    public function show(contactuses $contactus)
    {
        return view('contactuses.show',compact('contactus'));
    }
   
    public function edit(contactuses $contactus)
    {
        return view('contactuses.edit',compact('contactus'));
    }
    
    public function update(Request $request, contactuses $contactus)
    {
  
        $contactus->update($request->all());
  
        return redirect()->route('contactuses.index')
                        ->with('success','Contact updated successfully');
    }

    public function destroy(contactuses $contactus)
    {
        $contactus->delete();
  
        return redirect()->route('contactuses.index')
                        ->with('success','Contact deleted successfully');
    }
}

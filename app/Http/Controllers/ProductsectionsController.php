<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\productsections;
class ProductsectionsController extends Controller
{
    public function index()
    {
        $productsections = productsections::latest()->simplePaginate(5);
        return view('productsections.index',compact('productsections'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
   
    public function create()
    {
        return view('productsections.create');
    }
  
    public function store(Request $request)
    {
        if($request->hasfile('image'))
        {
           foreach($request->file('image') as $file)
           {
               $name=$file->getClientOriginalName();
               $file->move(public_path().'/files/', $name);  
               $data[] = $name;  
           }
        }
        $productsections= new productsections();
        $productsections->content = request('content');
        $productsections->title = request('title');
        $productsections->title_ar = request('title_ar');
        $productsections->save();
        return redirect()->route('productsections.index')
                        ->with('success','Product Section created successfully.');
    }

    public function show(productsections $productsection)
    {
        return view('productsections.show',compact('productsection'));
    }
   
    public function edit(productsections $productsection)
    {
        return view('productsections.edit',compact('productsection'));
    }
    
    public function update(Request $request, productsections $productsection)
    {
  
        // dd($request->all());
        $productsection->update($request->all());
  
        return redirect()->route('productsections.index')
                        ->with('success','Product Section updated successfully');
    }

    public function destroy(productsections $productsection)
    {
        $productsection->delete();
  
        return redirect()->route('productsections.index')
                        ->with('success','Product Section deleted successfully');
    }
}

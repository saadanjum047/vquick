<?php

namespace App\Http\Controllers;

use App\Area;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    public function index(){
        $areas = Area::all();

        return view('areas.index' , compact('areas'));
    }

    public function create(){
        return view('areas.create');
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'charges' => 'required'
        ]);
        
        $area = Area::create([
            'area' => $request->name,
            'ar_area' => $request->ar_area,
            'charges' => $request->charges,
        ]);

        if($area){
            return redirect('/areas')->with('success' , 'Area Added Successfully');
        }else{
            return redirect('/areas')->with('success' , 'Unexpected error occurred');
        }
    }

    public function edit($id){
        $area = Area::find($id);
        return view('areas.edit' , compact('area'));
    }

    public function update($id , Request $request){

        $request->validate([
            'name' => 'required',
            'ar_area' => 'required',
            'charges' => 'required'
        ]);
        


        $area = Area::find($id);
        $area->area = $request->name;
        $area->ar_area = $request->ar_area;
        $area->charges = $request->charges;
        $area->save();

        if($area){
            return redirect('/areas')->with('success' , 'Area Updated Successfully');
        }else{
            return redirect('/areas')->with('success' , 'Unexpected error occurred');
        }

    }

    public function destroy($id){
        $area = Area::find($id);
        if($area->delete()){
            return redirect('/areas')->with('success' , 'Area deleted successfully');
        }else{
            return redirect('/areas')->with('success' , 'Unexpected error occurred');
        }

    }

}

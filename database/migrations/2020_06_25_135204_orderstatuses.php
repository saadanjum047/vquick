<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Orderstatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderstatuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('area_id');

            $table->String('order_number')->nullable() ;
            $table->String('total_price');
            $table->String('cid');
            $table->String('transaction_number')->nullable();
            $table->String('txn_amount')->nullable();

            $table->String('payment_method');
            $table->String('status');

            $table->String('shipping_charges')->nullable();

            $table->boolean('counted_for_discount')->default(0);
            $table->boolean('discount_availed')->default(0);
            
            $table->text('reponse')->nullable();
            
            $table->timestamps();
        });

        DB::update('ALTER TABLE orderstatuses AUTO_INCREMENT = 10000');

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderstatuses');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('product_order')->nullable();
            $table->String('image')->nullable() ;
            $table->text('images')->nullable();
            $table->String('name');
            $table->String('arabic_name')->nullable();
            $table->text('description');
            $table->text('description_ar');
            $table->String('price');
            $table->String('stock_available');
            $table->String('category');
            $table->String('is_approved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

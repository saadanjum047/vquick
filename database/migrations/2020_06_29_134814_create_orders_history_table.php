<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_history', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('order_id')->nullable() ;
            $table->unsignedInteger('cart_id')->nullable() ;
            $table->String('order_number')->nullable();
            $table->String('payment_method')->nullable();
            $table->String('quantity')->nullable();
            $table->String('available')->nullable();
            $table->String('status')->nullable();
            $table->String('full_name')->nullable();
            $table->String('address')->nullable();
            $table->String('date')->nullable();
            $table->String('product_name')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_history');
    }
}

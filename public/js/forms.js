$ = jQuery;
// submit forms by ajax
$('.ajax-submit input[type="submit"], .ajax-submit button[type="submit"]').click(function(){
	'use strict';
	$(this).closest('.ajax-submit').attr('clicked-name', $(this).attr('name'));
});
function ajax_form_submission_completed(form, response)
{
	//check if form submission needs to update a select option
	var update_select = $(form).attr('update');
	if(update_select)
	{
		$('select[name="' + update_select + '"]').val();
		$('select[name="' + update_select + '"]').append('<option value="' + response.data + '" selected>' + response.data + '</option>');
		$(form).trigger("reset");
	}
	//check if needs to close modal
	if($(form).hasClass('modal-form'))
	{
		$(form).closest('.modal').modal('hide');
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
	}
}
$('.ajax-submit').submit(function(){
	'use strict';
	var form_data = $(this).serialize();
	var clicked_input_name = $(this).attr('clicked-name');
	if(clicked_input_name)
	{
		var clicked_input_val = $(this).find('[name="'+clicked_input_name+'"]').val();
		form_data = form_data + '&' + clicked_input_name + '=' + clicked_input_val;
	}
	var method = $(this).attr('method');
	method = method ? method : 'GET';
	var url = $(this).attr('action');
	url = url ? url : window.location.href;
	var form = this;
	$.ajax({
		type: method,
		url: url,
		data: form_data,
		dataType: 'json',
		success: function(data) {
			ajax_form_submission_completed(form, data);
		},
		error: function(jqXHR, textStatus, errorThrown){
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
		}
	});
	return false;
});
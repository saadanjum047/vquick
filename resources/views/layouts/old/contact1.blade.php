<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- StyleSheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- GoogleFonts -->
    <link href="https://fonts.googleapis.com/css2?family=GFS+Didot&display=swap" rel="stylesheet">
    <!-- Slick slider -->
    <link rel="stylesheet" href="assets/slick/slick-master/slick/slick-theme.css">
    <link rel="stylesheet" href="assets/slick/slick-master/slick/slick.css">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="assets/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/_text.scss">
    <!--  <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css"> -->
    <title>Contact</title> <link rel="stylesheet" href="assets/css/own-style.css">
  </head>
  <body>
      <!-- Header -->
      <div class="container ">
         <nav id="navbar_style" class="navbar navnav navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarTogglerDemo03" style="margin-left:-122px;margin-top:9px;">
                	<a href="{{ url('/') }}" > 
					<img src="assets/images/logo.png" id="LogoR" style="margin-left:100%" class="img-fluid "alt="logo "/>
				</a>  
			  <ul class="navbar-nav ulbi mr-auto ml-auto mt-2 mt-lg-0">
                  <li class="nav-item libi" style="margin-left: 5rem;margin-right: 1rem;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/product') }}">Products</a>
                  </li>
                  <li class="nav-item libi" style="margin-left: 0px;margin-right: 32px;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/about') }}">About</a>
                  </li>
                  <li class="nav-item libi">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/contact') }}" style="border-bottom: 2px solid #627976;color: #627976;">Contact</a>
                  </li>
               </ul>
            </div>
            <?php
            if( isset($_SESSION["logged_in"])) { ?>
              <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
                <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="{{ url('/user-logout') }}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i></span></a>
                </li>
                <li class="li2" style="display: inline-block;"><a href="{{ url('/checkout') }}"> 
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i></span></a>
                </li>
                <li class="li2 font-weight-bold ml-1 mr-3" style="display:inline"><a class="text-dark" href="{{ url('/order-history') }}">
                <i class="fa fa-history" aria-hidden="true"></i></span></a>
                </li>
               
              
            </ul>
            <?php }
            else {?>
            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
               <li class="li2  mr-3" style="display: inline-block;"><a href="{{ url('/userlogin') }}"><i class="fa fa-user" style="-webkit-text-fill-color: white;
                  -webkit-text-stroke-width: 1px;
                  -webkit-text-stroke-color: black;  font-size: 14px;"></i></a></li>
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="">AR</a></li>
               </ul>
            <?php } ?>
         </nav>
      </div>
    <!-- Navbar -->

    <!-- navbar mbl view -->
 
<div class="container py-2 sideeen">
{{--  <div id="mySidenav" class="sidenav">
<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
<a class="sidea" href="{{ url('/product') }}">Products</a>
<a class="sidea" href="{{ url('/about') }}">About</a>
<a class="sidea" href="gallery.html">Gallery</a>
<a class="sidea" href="{{ url('/contact') }}">Contact Us</a>
</div>  --}}
@include('layouts.partials.mobile_nav')

<div class="row">
<div id="sidebar">
<span style="font-size: 1;cursor: pointer;" onclick="openNav()" >
	<div class="bar"></div>
	<div class="bar"></div>
	<div class="bar"></div>
</span>
</div>
<div  id="Logoleft">
<a href="index.html">   <img src="assets/images/New Project.svg" id="Logo" class="img-fluid mblnavimg"  alt=""></a>
</div>
<div id="colgal">
<ul class="ul2" id="icon"  style="position: absolute;">
<li class="li2  mt-1" style="display:inline;MARGIN-LEFT:-10PX">
 <a href="{{('userlogin')}}">
 <i class="fa fa-user" style="-webkit-text-fill-color: white;
	-webkit-text-stroke-width: 2px;
	-webkit-text-stroke-color: black;  font-size: 16px;margin-left: -43px;margin-top: -2px;">
 </i>
 </a>
</li>
<li class="li2 mr-5" style="display:inline;margin-right: -31px !important;">
 <a href="{{route('checkout')}}">
	<i class="fa fa-shopping-bag fa-stack-1x " style="-webkit-text-fill-color: white;-webkit-text-stroke-width: 2px;-webkit-text-stroke-color: black;  font-size: 16px;margin-top:-3px;"></i>
 </a>
</li>
</ul>
</div>
</div>
</div>

    <!-- navbar mbl view -->

<!-- Header -->

<form action="/contact-form" method="POST">
          @csrf
   <div class="container mt-5">
     <div class="row no-gutters">
       <div class="col-md-12 col-lg-8 col-sm-12 col-12 condi px-5 contact-us-div" >
         <h2 class="contact-us-h2">Send us a Message </h2>
         <h2 class="contact-us-h2-mobile">Get In Touch </h2>
         <div class="row mt-3 mt2">
          <div class="col-md-6 col-sm-12 col-12 mg_l">
            <div class="form-group">
              <input name="name" type="text" class="form-control  inpf contact-us-input" id="exampleInputname1" placeholder="Your Name" >
            </div>
          </div>
          <div class="col-md-6 col-sm-12 col-12 mg_r">
            <div class="form-group">
              <input name="email" type="email"  class="form-control  inpf contact-us-input" id="emailcontact" name="emailcontact" placeholder="Email">
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-12 mt-3 mg">
            <div class="form-group">
              <label for="" style="color: #627976;">Message</label>
              <textarea name="message" id=""  style="width: 100%;height: 200px;background-color: transparent;border:1px solid #999999;"></textarea>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-12 text-right btn-send">
            <button type="submit" name="submit" class="conbnt px-4 py-2 contact-us-send" style="border-radius:0px">Send</button>
          </div>
          
         </div>
       </div>
       <div class="col-md-4 margin-top-btn">
         <img src="assets/images/contact.PNG"  class=" conim" alt="">
       </div>
     </div>
   </div>
   </form>

       <!-- footer -->
 <div class="container mb-3 mt-5 chotip border_top">
	<div class="row">
		<div class="col-md-12 text-center">
			<img src="assets/images/vv.jpg" class="img-fluid pb-3" style="width: 80px;" alt="">

			<div class="text-center mt-2">
				<a class="text-dark" href="">
					<i class="fa fa-facebook fafoo"></i>
				</a>          
				<a class="text-dark" href="">
					<i class="fa fa-instagram fafoo"></i>
				</a> 
				<a class="text-dark" href="">
					<i class="fa fa-twitter fafoo"></i>
				</a>     
			</div>
			<h4 class="text-center mt-3 fh4" style="color:#00000094;">&copy; 2020  vequick.com, Inc.<br>All Rights Reserved.  Privacy Policy</h4>
		</div>
	</div>
</div>	  
<!-- footer -->
<div class="container badip mb-3 mt-5">
  <div class="row">
    <div class="col-md-12">
      <h4 class="text-center fh4">&copy; 2020  vequick.com, Inc. All Rights Reserved.  Privacy Policy</h4>
      <div class="text-center mt-3">
        <a class="text-dark" href=""><i class="fa fa-facebook fafoo"></i></a>
<a class="text-dark" href="">        <i class="fa fa-instagram fafoo"></i>
</a>        
<a class="text-dark" href=""><i class="fa fa-twitter fafoo"></i>
</a>      
    </div>
  </div>
</div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
    <script>
    function openNav(){
      document.getElementById("mySidenav").style.width="300px";
    }
    function closeNav(){
      document.getElementById("mySidenav").style.width="0px";
    }
  </script>
    </script>
  </body>
</html>


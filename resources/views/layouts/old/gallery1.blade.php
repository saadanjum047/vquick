<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<head>
<!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- StyleSheet -->
      <link rel="stylesheet" href="assets/css/style.css">
      <!-- GoogleFonts -->
      <link href="https://fonts.googleapis.com/css2?family=GFS+Didot&display=swap" rel="stylesheet">
      <!-- Slick slider -->
      <link rel="stylesheet" href="assets/slick/slick-master/slick/slick-theme.css">
      <link rel="stylesheet" href="assets/slick/slick-master/slick/slick.css">
      <!-- Link Swiper's CSS -->
      <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
	  <title>Gallery</title>
	  <link rel="stylesheet" href="assets/css/own-style.css">

</head>
<style>
.mySlides {display:none}
.w3-left, .w3-right, .w3-badge {cursor:pointer}
.w3-badge {height:13px;width:13px;padding:0}
</style>
<body>

<div class="container py-2 sideeen">
<div id="mySidenav" class="sidenav">
<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
<a class="sidea" href="Product.html">Products</a>
<a class="sidea" href="about.html">About</a>
<a class="sidea" href="gallery.html">Gallery</a>
<a class="sidea" href="contact.html">Contact Us</a>
</div>
<div class="row">
<div id="sidebar">
<span style="font-size: 1;cursor: pointer;" onclick="openNav()" >
	<div class="bar"></div>
	<div class="bar"></div>
	<div class="bar"></div>
</span>
</div>
<div  id="Logoleft">
<a href="index.html">   <img src="assets/images/New Project.svg" id="Logo" class="img-fluid mblnavimg"  alt=""></a>
</div>
<div id="colgal">
<ul class="ul2" id="icon"  style="    position: absolute;">
<li class="li2  mt-1" style="display:inline;MARGIN-LEFT:-10PX">
 <a href="login.html">
 <i class="fa fa-user" style="-webkit-text-fill-color: white;
	-webkit-text-stroke-width: 2px;
	-webkit-text-stroke-color: black;  font-size: 16px;margin-left: -43px;margin-top: -2px;">
 </i>
 </a>
</li>
<li class="li2 mr-5" style="display:inline;margin-right: -31px !important;">
 <a href="checkout.html">
	<i class="fa fa-shopping-bag fa-stack-1x " style="-webkit-text-fill-color: white;-webkit-text-stroke-width: 2px;-webkit-text-stroke-color: black;  font-size: 16px;margin-top:-3px;"></i>
 </a>
</li>
</ul>
</div>
</div>
</div>
<div class="container my-5 index-mobile-view">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="font-weight-bold text-uppercase our-product-mobile" >Our Gallery</h1>
		</div>
	</div>
</div>
	
<div class="w3-content w3-display-container" style="max-width:800px;margin:2rem 1rem;" >
  <img class="mySlides" src="assets/images/banner1.png" title="image11" style="width:100%">
  <img class="mySlides" src="assets/images/image11.png" title="image12" style="width:100%">
  <img class="mySlides" src="assets/images/banner1.png" title="image13" style="width:100%">
  <img class="mySlides" src="assets/images/image11.png" title="image21" style="width:100%">
  <img class="mySlides" src="assets/images/banner1.png" title="image22" style="width:100%">
  <img class="mySlides" src="assets/images/image11.png" title="image23" style="width:100%">
  <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle" style="width:100%">
    <div class="w3-left w3-hover-text-khaki" onclick="plusDivs(-1)">&#10094;</div>
    <div class="w3-right w3-hover-text-khaki" onclick="plusDivs(1)">&#10095;</div>
    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(1)"></span>
    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(2)"></span>
    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(3)"></span>
    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(4)"></span>
    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(5)"></span>
    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(6)"></span>
  </div>
</div>
<div class="container mb-3 mt-5 chotip border_top">
	<div class="row">
		<div class="col-md-12 text-center">
			<img src="assets/images/vv.jpg" class="img-fluid pb-3" style="width: 80px;" alt="">

			<div class="text-center mt-2">
				<a class="text-dark" href="">
					<i class="fa fa-facebook fafoo"></i>
				</a>          
				<a class="text-dark" href="">
					<i class="fa fa-instagram fafoo"></i>
				</a> 
				<a class="text-dark" href="">
					<i class="fa fa-twitter fafoo"></i>
				</a>     
			</div>
			<h4 class="text-center mt-3 fh4" style="color:#00000094;">&copy; 2020  vequick.com, Inc.<br>All Rights Reserved.  Privacy Policy</h4>
		</div>
	</div>
</div>	
<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" w3-white", "");
  }
  x[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " w3-white";
}
</script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <script src="assets/slick/slick-master/slick/slick.js"></script>
      <!-- Swiper JS -->
      <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
      <!-- Initialize Swiper -->

  <script>
         function increaseCount(e, el) {
         var input = el.previousElementSibling;
         var value = parseInt(input.value, 10);
         value = isNaN(value) ? 0 : value;
         value++;
         input.value = value;
         }
         function decreaseCount(e, el) {
         var input = el.nextElementSibling;
         var value = parseInt(input.value, 10);
         if (value > 1) {
         value = isNaN(value) ? 0 : value;
         value--;
         input.value = value;
         }
         }
      </script>
      <script>
         $(".variable").slick({
          dots: true,
          infinite: true,
          autoplay:true,
          time:200,
          pagination:false,
          arrows:true,
          variableWidth: true
         });
      </script>
      <script>
         function openNav(){
           document.getElementById("mySidenav").style.width="300px";
         }
         function closeNav(){
           document.getElementById("mySidenav").style.width="0px";
         }
      </script>

</body>
</html> 

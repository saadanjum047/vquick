
<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
$id = $_SESSION["id"];
$price=0;
$z=0;
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- StyleSheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- GoogleFonts -->
    <link href="https://fonts.googleapis.com/css2?family=GFS+Didot&display=swap" rel="stylesheet">

    <!-- Slick slider -->
    <link rel="stylesheet" href="assets/slick/slick-master/slick/slick-theme.css">
    <link rel="stylesheet" href="assets/slick/slick-master/slick/slick.css">
    <!-- Link Swiper s CSS -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
    <title>Order History</title>
    <link rel="stylesheet" href="assets/css/own-style.css">
  </head>
  <body>
      {{--  @dd($order)  --}}
  {{--  @foreach ($orders as $cart)
    <?php $z++; ?>
  @endforeach  --}}
        <div class="container ">
         <nav id="navbar_style" class="navbar navnav navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarTogglerDemo03" style="margin-left:-122px;margin-top:9px;">
                	<a href="{{ url('/') }}" > 
					<img src="assets/images/logo.png" id="LogoR" style="margin-left:100%" class="img-fluid "alt="logo "/>
				</a>  
			   <ul class="navbar-nav ulbi mr-auto ml-auto mt-2 mt-lg-0">
                  <li class="nav-item libi" style="margin-left: 5rem;margin-right: 1rem;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/product') }}">Products</a>
                  </li>
                  <li class="nav-item libi" style="margin-left: 0px;margin-right: 32px;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/about') }}">About</a>
                  </li>
                  <li class="nav-item libi">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/contact') }}">Contact</a>
                  </li>
               </ul>
            </div>
            <?php
            if( isset($_SESSION["logged_in"])) { ?>
            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
                <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="{{ url('/user-logout') }}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i></span></a>
                </li>
                <li class="li2" style="display: inline-block;"><a href="{{ url('/checkout') }}"> 
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i></span></a>
                </li>
                <li class="li2 font-weight-bold ml-1 mr-3" style="display:inline"><a class="text-dark" href="{{ url('/order-history') }}">
                <i class="fa fa-history" aria-hidden="true"></i></span></a>
                </li>
               
              
            </ul>
            <?php }
            else {?>
            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
               <li class="li2  mr-3" style="display: inline-block;"><a href="{{ url('/userlogin') }}"><i class="fa fa-user" style="-webkit-text-fill-color: white;
                  -webkit-text-stroke-width: 1px;
                  -webkit-text-stroke-color: black;  font-size: 14px;"></i></a></li>
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="">AR</a></li>
               </ul>
            <?php } ?>
         </nav>
      </div>
    <!-- Navbar -->

    <!-- navbar mbl view -->
   <div class="container py-2 sideeen">
  {{--  <div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a class="sidea" href="Product.html">Products</a>
    <a class="sidea" href="about.html">About</a>
    <a class="sidea" href="gallery.html">Gallery</a>
    <a class="sidea" href="contact.html">Contact Us</a>
  </div>  --}}
  @include('layouts.partials.mobile_nav')
<div class="row">
<div id="sidebar">
<span style="font-size: 1;cursor: pointer;" onclick="openNav()" >
	<div class="bar"></div>
	<div class="bar"></div>
	<div class="bar"></div>
</span>
</div>
<div  id="Logoleft">
<a href="/">   <img src="assets/images/New Project.svg" id="Logo" class="img-fluid mblnavimg"  alt=""></a>
</div>
<div id="colgal">
<ul class="ul2" id="icon"  style="    position: absolute;">
<li class="li2  mt-1" style="display:inline;MARGIN-LEFT:-10PX">
 <a href="login.html">
 <i class="fa fa-user" style="-webkit-text-fill-color: white;
	-webkit-text-stroke-width: 2px;
	-webkit-text-stroke-color: black;  font-size: 16px;margin-left: -43px;margin-top: -2px;">
 </i>
 </a>
</li>
<li class="li2 mr-5" style="display:inline;margin-right: -31px !important;">
 <a href="checkout.html">
	<i class="fa fa-shopping-bag fa-stack-1x " style="-webkit-text-fill-color: white;-webkit-text-stroke-width: 2px;-webkit-text-stroke-color: black;  font-size: 16px;margin-top:-3px;"></i>
 </a>
</li>
</ul>
</div>
</div>
</div>

    <!-- navbar mbl view -->

<!-- Header -->

       <div class="container mt-5 badip">
      <div class="row no1">
        <div class="col-md-6">
          <h1 class="chch1 checkout-h1">History</h1>
        </div>
        <div class="col-md-6">
          <h2 class="text-right"><a class="chch2 checkout-h2" href="/product">Continue Shopping</a></h2>
        </div>
      </div>
    </div>
    <div class="container mb-4 mt-3 badip">
      <div class="row no2">
        <div class="col-md-6">
          <p class="chcp1 checkout-main-row">Items</p>
        </div>
        <div class="col-md-1">
          <p class="chcp1 checkout-main-row">Status</p>
        </div>
        <div class="col-md-1">
          <p class="chcp1 checkout-main-row">Quantity</p>
        </div>
        <div class="col-md-2">
          <p class="chcp1 checkout-main-row">Price</p>
        </div>
        
        <div class="col-md-2">
          <p class="chcp1 checkout-main-row">Date</p>
        </div>

      </div>

      <hr class="no3" style="background-color: #d9d9d9;margin-top: -10px;" >
      @foreach ($carts as $cart)

      <div class="row">
        <div class="col-md-2">
          <img src="files/{{ $cart->image}}" style="height: 160px;width: 550px;" class="img-checkout py-2" alt="">
        </div>
        <div class="col-md-4 pt-5">
          <h2 class="chch2 checkout-list-h2 ">{{ $cart->name}}</h2>
          <p class="chcp1 checkout-list-p " ><?= substr($cart->description,0,30) ?></p>
        </div>
        <div class="col-md-1 pt-5 mt-2" >
            {{ $cart->order_status ? $cart->order_status->status : 'Removed'}}
        </div>

        <div class="col-md-1 pt-5" >
          <div class='counter-checkout'>


            <!--<div class='decrease-counter' onclick='decreaseCount(event, this)'>-</div>-->
              <input type='text' value='{{ $cart->quantity }}'  >
            <!--<div class='decrease-counter' Onclick='increaseCount(event, this)'>+</div>-->
            </div>
          </div>
          <div class="col-md-2 " style="padding-top: 62px;">
            <h2 class="chch2 c-price" >QR {{ $cart->price}}</h2>
          </div>
          <div class="col-md-2 d-flex align-items-center">  
              
            <p class="chcp1 checkout-list-p " >{{$cart->created_at->toDateString()}}</p>

           
          </div>
        </div>
        <hr class ="no3" style="background-color: #d9d9d9;">
        <?php
        $cart_price=$cart->price;
        $price+= $cart_price;
        ?>
        @endforeach
      </div>
    </div>

<!-- MOBILE DESIGN -->

<div class="container chotip  mt-4">
  @foreach ($carts as $cart)
	<div class="row no-gutters  margin-bottom-2 ">
		<div class="col-sm-5 col-5 checkout-mobile">
			<img src="/files/{{$cart->image}}" class="img-fluid"  alt="">
		</div>
		<div class="col-sm-7 col-7 px-2 py-4 mobile-checkout-container" >
			<h2 class="chch2 font-weight-bold checkout-list-h2">{{ $cart->name}}</h2>
			{{--  <i class="fa fa-ellipsis-v" aria-hidden="true" style="float: right;margin-top:-25px;color: #0000005c;"></i>  --}}
			<p class="chcp1" style="font-size: 12px;">{{substr($cart->description,0,30)}} </p>
      <h2 class="chch2 m-price"><strong>QR {{ $cart->price}} </strong></h2>

      {{ $cart->order_status ? $cart->order_status->status : 'Removed'}}
      <br>
      {{ $cart->created_at->toDateString()}}
	
		</div>
  </div>
	<hr style="background-color: #d9d9d9;">
  @endforeach
	{{--  <div class="row no-gutters  margin-bottom-2 ">
		<div class="col-sm-5 col-5 checkout-mobile">
			<img src="assets/images/banner1.PNG" class="img-fluid"  alt="">
		</div>
		<div class="col-sm-7 col-7 px-2 py-4 mobile-checkout-container" >
			<h2 class="chch2 font-weight-bold checkout-list-h2">Colombia M1</h2>
			<i class="fa fa-ellipsis-v" aria-hidden="true" style="float: right;margin-top:-25px;color: #0000005c;"></i>
			<p class="chcp1" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur </p>
			<h2 class="chch2 m-price"><strong>QR 123</strong></h2>

			<div class='counter mt-1'>
				<div class='decrease-counter'  onclick='decreaseCount(event, this)'>-</div>
				<input type='text' value='1' class="chotoo" >
				<div class='up' style="background-color: #dcdcdc;font-size: 20px;width: 30px;height: 30px;padding-top: 2px;padding-left: 9px !important;" onclick='increaseCount(event, this)'>+</div>
			</div>
		</div>
	</div>
	<div class="row no-gutters  margin-bottom-2 ">
		<div class="col-sm-5 col-5 checkout-mobile">
			<img src="assets/images/banner1.PNG" class="img-fluid"  alt="">
		</div>
		<div class="col-sm-7 col-7 px-2 py-4 mobile-checkout-container" >
			<h2 class="chch2 font-weight-bold checkout-list-h2">Colombia M1</h2>
			
			<p class="chcp1" style="font-size: 12px;">Lorem ipsum dolor sit amet, consectetur </p>
			<h2 class="chch2 m-price"><strong>QR 123</strong></h2>

			<div class='counter mt-1'>
				<div class='decrease-counter'  onclick='decreaseCount(event, this)'>-</div>
				<input type='text' value='1' class="chotoo" >
				<div class='up' style="background-color: #dcdcdc;font-size: 20px;width: 30px;height: 30px;padding-top: 2px;padding-left: 9px !important;" onclick='increaseCount(event, this)'>+</div>
			</div>
		</div>
	</div>  --}}
</div>

<!-- MOBILE DESIGN ENDS -->
<div class="col-md-12 mt-4">
          {{--  <button class="m-check-out-button">Check Out</button>  --}}
        </div>
    
        <!-- footer -->
      <div class="container mb-3 mt-5 chotip border_top">
        <div class="row">
          <div class="col-md-12 text-center ">
            <img src="assets/images/vv.jpg" class="img-fluid pb-3" style=" width: 80px;margin-top: 1rem;" alt="">
      
            <div class="text-center mt-2">
              <a class="text-dark" href=""><i class="fa fa-facebook fafoo"></i></a>          
              <a class="text-dark" href=""><i class="fa fa-instagram fafoo"></i></a> 
              <a class="text-dark" href=""><i class="fa fa-twitter fafoo"></i></a>      </div>
              <h4 class="text-center mt-3" style="color: #000000b0;">&copy; 2020  vequick.com, Inc.<br>
              All Rights Reserved.  Privacy Policy</h4>
            </div>
          </div>
        </div>

<!-- footer -->
<div class="container badip mb-3 mt-5">
  <div class="row">
    <div class="col-md-12">
      <h4 class="text-center fh4">&copy; vequick.com,Inc.All rights reserved.Privacy Policy</h4>
      <div class="text-center mt-3">
        <a class="text-dark" href=""><i class="fa fa-facebook fafoo"></i></a>
<a class="text-dark" href="">        <i class="fa fa-instagram fafoo"></i>
</a>        
<a class="text-dark" href=""><i class="fa fa-twitter fafoo"></i>
</a>      
    </div>
  </div>
</div>
</div>

    @include('layouts.partials.errors')


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    {{--  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>  --}}
    <script>
    function increaseCount(e, el) {
    var input = el.previousElementSibling;
    var value = parseInt(input.value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    input.value = value;
    }
    function decreaseCount(e, el) {
    var input = el.nextElementSibling;
    var value = parseInt(input.value, 10);
    if (value > 1) {
    value = isNaN(value) ? 0 : value;
    value--;
    input.value = value;
    }
    }
    </script>
    <script>
    function openNav(){
      document.getElementById("mySidenav").style.width="300px";
    }
    function closeNav(){
      document.getElementById("mySidenav").style.width="0px";
    }
  </script>
  <?php
    if(isset($_SESSION['message'])) 
    { 
      $login_message= $_SESSION['message'];?>
      <script>
          swal("{{ $login_message }}");
          //setTimeout(() => {  window.location.href = "{{ url('/checkout') }}"; }, 1000);
      </script><?php
      unset($_SESSION['message']);
    }
  ?>
    </script>
  </body>
</html>








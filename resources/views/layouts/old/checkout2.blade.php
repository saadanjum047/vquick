@extends('layouts.main')



@section('title')
<title>Checkout</title>
@endsection


@section('styles')
    
<style>

  .counter1 {
    width:90px !important;
  }

  .btn-danger:focus {
    border: none;
    box-shadow: none;

  }
  .remove-cart-button{
    background-color: white;
    border: none;
    box-shadow: none;
  }
  .remove-cart-button:focus {
    border: none;
    box-shadow: none;
  }
  .checkout-list-p{
    line-height: 18px !important;
  }

  .swal-footer{
    display: block;
  }
  .swal-footer{
    text-align: center;
  }
  .swal-button--guest{
    background-color: #666666 !important;
    border: 1px solid #666666 !important;
  }
  
  .swal-button--login{
    color: #999999;
  }
  
 
  .swal-button--guest:focus {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;
    box-shadow : none !important;
  }
  .swal-button--guest:hover {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;
  }  

  .swal-button--guest:not([disabled]):hover {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;

  }
  
  .swal-button--guest:not([disabled]):hover {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;

  }
</style>
  @endsection

@section('content')
    

<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if(isset($_SESSION["id"])){
  $id = $_SESSION["id"];
}
$price=0;
$z=0;
?>
<!-- Header -->

       <div class="container mt-5 badip">
      <div class="row no1">
        <div class="col-md-6">
          <h1 class="chch1 checkout-h1">{{__('Checkout')}}</h1>
        </div>
        <div class="col-md-6">
          <h2 class="text-right"><a class="chch2 checkout-h2" href="/product">{{__('Continue Shopping')}}</a></h2>
        </div>
      </div>
    </div>
    <div class="container mb-4 mt-3 badip">
      <div class="row no2">
        <div class="col-md-6">
          <p class="chcp1 checkout-main-row">{{__('Items')}}</p>
        </div>
        <div class="col-md-3">
          <p class="chcp1 checkout-main-row">{{__('Quantity')}}</p>
        </div>
        <div class="col-md-3">
          <p class="chcp1 checkout-main-row">{{__('Price')}}</p>
        </div>

      </div>
      {{--  @dd($carts)  --}}
      <hr class="no3" style="background-color: #d9d9d9;margin-top: -10px;" >
      @foreach ($carts as $cart)
      <div class="row">
        <div class="col-md-2">
          <img src="files/{{ $cart->image}}" style="height: 160px;width: 550px;" class="img-checkout py-2" alt="">
        </div>
        <div class="col-md-4 pt-5">
          <h2 class="chch2 checkout-list-h2 ">{{ $cart->product->proper_name}}</h2>
          <p class="chcp1 checkout-list-p " style="line-height: 18px !important" >{{  Str::limit($cart->product->proper_description,100) }}</p>
        </div>
        <div class="col-md-3 pt-5" >
          <div class='counter-checkout'>
            <!--<div class='decrease-counter' onclick='decreaseCount(event, this)'>-</div>-->
              {{--  <input type='text' value='{{ $cart->quantity }}'  >  --}}
              <div id="quantity-buttons" style="border:1px solid #d3aea6 ; border-radius: 5px; height: 36px; "
              <div class='counter1'  style="margin-top:1px;">
                <div id="down" class='down' onclick='decreaseCount(event, this ,  {{$cart->id }})'>-</div>
                <input type='text' value='{{ $cart->quantity }}' id="quantity" name="quantity" class="counter_1" value='1'  >
                <div class='up' id="up" onclick='increaseCount(event, this , {{$cart->id}} )'>+</div>
              </div>
              
            <!--<div class='decrease-counter' Onclick='increaseCount(event, this)'>+</div>-->
            </div>
          </div>
          <div class="col-md-2 " style="padding-top: 62px;">
            <h2 class="chch2 c-price cart-price-{{$cart->id}}" >QR {{ $cart->price}}</h2>
          </div>
          <div class="col-md-1">  
            <form id="cart-{{$cart->id}}" action="{{ route('carts.destroy',$cart->id) }}" method="POST">
              @csrf
              @method('DELETE')
              <a href="javascript:;" onclick=" $('#cart-{{$cart->id}}').submit(); " class="remove-cart-button"><i class="fa fa-close chchfa"></i></a>
            </form>
          </div>
        </div>
        <hr class ="no3" style="background-color: #d9d9d9;">
        <?php
        $cart_price=$cart->price;
        $price+= $cart_price;
        ?>
        @endforeach
      </div>
    </div>

<!-- MOBILE DESIGN -->

<div class="container chotip  mt-4">
  @foreach($carts as $cart)
	<div class="row no-gutters  margin-bottom-2 ">
		<div class="col-sm-5 col-5 checkout-mobile">
			<img src="/files/{{$cart->image}}" class="img-fluid"  alt="">
		</div>
		<div class="col-sm-7 col-7 px-2 py-4 mobile-checkout-container" >
			<h2 class="chch2 font-weight-bold checkout-list-h2">{{ $cart->product->proper_name}}</h2>
      <a href="#" onclick="delete_cart({{$cart->id}})" > <i class="fa fa-times-circle" aria-hidden="true" style="float: right;margin-top:-25px;color: #0000005c;"></i></a>
      
      <form id="delete-cart-{{$cart->id}}"   action="{{ route('carts.destroy',$cart->id) }}" method="POST">
        @csrf
        @method('DELETE')
      </form>
      
        <button type="submit"  class="btn-block btnchch py-3 checkout-btn uncfocused-item" id="placebutn">{{__('PLACE ORDER')}}</button>

			{{--  <i class="fa fa-ellipsis-v" aria-hidden="true" style="float: right;margin-top:-25px;color: #0000005c;"></i>  --}}
			<p class="chcp1" style="font-size: 12px;">{{ Str::limit($cart->product->proper_description , 200)}} </p>
			<h2 class="chch2 m-price "><strong class="cart-price-{{$cart->id}}" >QR {{$cart->price}}</strong></h2>

			<div class='counter mt-1'>
				<div class='decrease-counter'  onclick='decreaseCount(event, this , {{$cart->id}} )'>-</div>
				<input type='text' value='{{$cart->quantity}}' class="chotoo" >
				<div class='up' style="background-color: #dcdcdc;font-size: 20px;width: 30px;height: 30px;padding-top: 2px;padding-left: 9px !important;" onclick='increaseCount(event, this , {{$cart->id}})'>+</div>
			</div>
		</div>
  </div>
  @php
  $cart_price=$cart->price;
  $price+= $cart_price;

  @endphp
  <hr style="background-color: #d9d9d9;">
  @endforeach
	
  {{--  carts end here  --}}

</div>

  @if($carts->count() > 0)
  <div class="container px-4 mt-2">
    <div class="row mt-5">
      <div class="col-md-12">
        <h2 class="shiping-fee" >{{__('Shipping fee')}}</h2>
        <h2 id="ddddd" class="shiping-fee text-right">QR {{ $shippings->first()->fee}}</h2>
        
      </div>
      <div class="col-xs-12 col-12 col-sm-12 col-md-3 col-lg-3">
        <select class="form-control" name="payment_method" >
          <option disabled>{{__("Select Payment Option")}}</option>
          <option value="cash on delivery" >{{__('Cash On Delivery')}}</option>
          <option value="paypal" >{{__("PayPal")}}</option>
        </select>
      </div>
      
      @foreach ($shippings as $shipping)
      <div class="col-md-6 ">
        {{--  <h2 id="ddddd" class="shiping-fee text-right">QR {{ $shipping->fee}}</h2>  --}}
      </div>
      <?php $total_price = $shipping->fee + array_sum($carts->pluck('price')->toArray());?>
      @endforeach
    </div>
	  <hr class="no3" style="background-color: #d9d9d9;margin: 1rem 0;">
	<div class="row mv">
		<div class="col-md-6">
		  <h2 class="shiping-fee-s">{{__('Sub total')}}</h2>
		</div>
		<div class="col-md-6">
		  <h2 id="ddddd" class="shiping-fee-s text-right sub-total ">QR {{ array_sum($carts->pluck('price')->toArray()) }}</h2>
		</div>
	</div>
	<div class="row mt-4 ">
		<div class="col-md-6">
		  <h2 class="shiping-fee-m">{{__('Total')}}<span class="mobile-p-v total-amount">QR  <span class="total-amount"> {{$total_price}}</span></span></h2>
		</div>
		<div class="col-md-6">
		  <h2 id="dddddd" class=" text-right m-n-v " >QR <span class="total-amount">{{$total_price}} </span> </h2>
		</div>
    <div class="col-md-12 mt-4" >  
      <form method="get" action="/order-cart" class="place-order-form">
      <input type="hidden" name="total_price" class="total-amount" value="{{ $total_price }}" >
      <input type="hidden" name="id" value="{{ $id ?? session()->getId() }}" >
      <button @if(isset($_SESSION['id'])) type="submit" @else  type="button" onclick="ask_how_to_checkout()" @endif  class="btn-block btnchch py-3 checkout-btn uncfocused-item" id="placebutn">{{__('PLACE ORDER')}}</button>
      </form>
    </div>
	</div>
</div>
@else
<div class="container px-4 mt-2">
	<div class="row mt-5">
		<div class="col-md-6">
		  <h2 class="shiping-fee" >{{__('Shipping fee')}}</h2>
		</div>
		<div class="col-md-6 ">
		  <h2 id="ddddd" class="shiping-fee text-right">QR 0</h2>
		</div>
	</div>
	<hr class="no3" style="background-color: #d9d9d9;margin: 1rem 0;">
	<div class="row mv">
		<div class="col-md-6">
		  <h2 class="shiping-fee-s">{{__('Sub total')}}</h2>
		</div>
		<div class="col-md-6">
		  <h2 id="ddddd" class="shiping-fee-s text-right">QR 0</h2>
		</div>
	</div>
	<div class="row mt-4 ">
		<div class="col-md-6">
		  <h2 class="shiping-fee-m">{{__('Total')}}<span class="mobile-p-v">QR 0</span></h2>
		</div>
		<div class="col-md-6">
		  <h2 id="dddddd" class=" text-right m-n-v">QR 0</h2>
		</div>
    <div class="col-md-12 mt-4" >
      @if($carts->count() > 0)
      <button class="btn-block btnchch py-3 checkout-btn uncfocused-item" id="placebutn">{{__('PLACE ORDER')}}</button>
      @endif
      </form>
    </div>
	</div>
</div>
@endif

<!-- MOBILE DESIGN ENDS -->
<div class="col-md-12 mt-4">
        @if(isset($carts) && $carts->count() > 0)
        <form method="get" action="/order-cart" class="place-order-form">
          <input type="hidden" name="total_price" class="total-amount" value="{{ $total_price }}" required>
          <input type="hidden" name="id" value="{{ $id ?? session()->getId()}}" >
          <button type="submit" class="m-check-out-button uncfocused-item">{{__("PLACE ORDER")}}</button>
        </form>
        @endif
        </div>



@if(isset($carts) && $carts->count() > 0)

<form action="{{route('guest-signup')}}" id="guest-signup-form" method="GET" >
  <input type="hidden" class="total-amount" name="total_price" value="{{$total_price}}" />
</form>

@endif

@endsection


@section('scripts')
<script>

  function ask_how_to_checkout(){
  

    swal("{{__('How would you link to continue')}}", {
      buttons: {
      login: {
        text: "{{__('Login')}}",
        value: "login",
      },
      
      guest : {
        text: "{{__('Guest')}}",
        value: "guest",
      },
    },
  })
  .then((value) => {
    switch (value) {
   
      case "login":
        $('.place-order-form').submit();
        break;
        
      case "guest":
      $('#guest-signup-form').submit();
      break;
   
        default:
      }
    });
  }
  

  function delete_cart(id){
    console.log(id)
    document.getElementById("delete-cart-"+id).submit();    
  }


  function increaseCount(e, el , cart_id) {
    
    var input = el.previousElementSibling;
    var value = parseInt(input.value, 10);
    value = isNaN(value) ? 0 : value;
    
    //el.onclick = false;


    $.ajax({
      url: '{{url("/")}}/update-cart-quantity-ajax/'+ cart_id + '/' + (value + 1) ,
      success: function(res){

        $('.cart-price-'+cart_id ).html( 'QR ' + res.cart.price);
        $('.total-amount').html( res.total_amount);
        $('.total-amount').val( res.total_amount);
        $('.sub-total').html('QR ' +  res.sub_total);
        value++;
        input.value = value;
  

    }
    }).fail(function(errors){
      //swal(errors.responseJSON.message);

      if(errors.responseJSON.error_code == 1){

        swal({text: '{{__("Category restriction exceeded")}}' , timer:5000});
  
      }
      if(errors.responseJSON.error_code == 2){
  
        swal({ text: '{{__("This item stock is not enough")}}', timer:5000});
  
      }
  
    });
    
    //el.onclick = true;
    //$(el).on('click',increaseCount(event , this , cart_id)); 

  }
  function decreaseCount(e, el , cart_id) {
  var input = el.nextElementSibling;
  var value = parseInt(input.value, 10);
  if (value > 1) {
  value = isNaN(value) ? 0 : value;

  //el.onclick = false;


  $.ajax({
    url: '{{url("/")}}/update-cart-quantity-ajax/'+ cart_id + '/' + (value - 1) ,
    success: function(res){

      $('.cart-price-'+cart_id ).html( 'QR ' + res.cart.price);
      $('.total-amount').html( res.total_amount);
      $('.total-amount').val( res.total_amount);

      $('.sub-total').html('QR ' +  res.sub_total);

      value--;
      input.value = value;


  }
  }).fail(function(errors){

    if(errors.responseJSON.error_code == 1){

      swal({ text: '{{__("Category restriction exceeded")}}' , timer:5000 });

    }
    if(errors.responseJSON.error_code == 2){

      swal( { text: '{{__("This item stock is not enough")}}' , timer:5000 });

    }

  });

  }
  }
  </script>
  <script>
  function openNav(){
    document.getElementById("mySidenav").style.width="300px";
  }
  function closeNav(){
    document.getElementById("mySidenav").style.width="0px";
  }
</script>
<?php
  if(isset($_SESSION['message'])) 
  { 
    $login_message= $_SESSION['message'];?>
    <script>
        swal("{{ $login_message }}");
        setTimeout(() => {  window.location.href = "{{ url('/checkout') }}"; }, 1000);
    </script><?php
    unset($_SESSION['message']);
  }
?>
  </script>
@endsection
@extends('layouts.main')


@section('content')

@php
    
if(!isset($_SESSION)) 
{ 
  session_start(); 
}
$id = $_SESSION["id"];
$price=0;
$z=0;

@endphp

<div class="container mt-5 badip">
    <div class="row no1">
      <div class="col-md-6">
        <h1 class="chch1 checkout-h1">History</h1>
      </div>
      <div class="col-md-6">
        <h2 class="text-right"><a class="chch2 checkout-h2" href="/product">Continue Shopping</a></h2>
      </div>
    </div>
  </div>
  <div class="container mb-4 mt-3 badip">
    <div class="row no2">
      <div class="col-md-6">
        <p class="chcp1 checkout-main-row">Items</p>
      </div>
      <div class="col-md-1">
        <p class="chcp1 checkout-main-row">Status</p>
      </div>
      <div class="col-md-1">
        <p class="chcp1 checkout-main-row">Quantity</p>
      </div>
      <div class="col-md-2">
        <p class="chcp1 checkout-main-row">Price</p>
      </div>
      
      <div class="col-md-2">
        <p class="chcp1 checkout-main-row">Date</p>
      </div>

    </div>

    <hr class="no3" style="background-color: #d9d9d9;margin-top: -10px;" >
    @foreach ($carts as $cart)

    <div class="row">
      <div class="col-md-2">
        <img src="files/{{ $cart->image}}" style="height: 160px;width: 550px;" class="img-checkout py-2" alt="">
      </div>
      <div class="col-md-4 pt-5">
        <h2 class="chch2 checkout-list-h2 ">{{ $cart->name}}</h2>
        <p class="chcp1 checkout-list-p " ><?= substr($cart->description,0,30) ?></p>
      </div>
      <div class="col-md-1 pt-5 mt-2" >
          {{ $cart->order_status ? $cart->order_status->status : 'Removed'}}
      </div>

      <div class="col-md-1 pt-5" >
        <div class='counter-checkout'>


          <!--<div class='decrease-counter' onclick='decreaseCount(event, this)'>-</div>-->
            <input type='text' value='{{ $cart->quantity }}'  >
          <!--<div class='decrease-counter' Onclick='increaseCount(event, this)'>+</div>-->
          </div>
        </div>
        <div class="col-md-2 " style="padding-top: 62px;">
          <h2 class="chch2 c-price" >QR {{ $cart->price}}</h2>
        </div>
        <div class="col-md-2 d-flex align-items-center">  
            
          <p class="chcp1 checkout-list-p " >{{$cart->created_at->toDateString()}}</p>

         
        </div>
      </div>
      <hr class ="no3" style="background-color: #d9d9d9;">
      <?php
      $cart_price=$cart->price;
      $price+= $cart_price;
      ?>
      @endforeach
    </div>
  </div>

<!-- MOBILE DESIGN -->

<div class="container chotip  mt-4">
@foreach ($carts as $cart)
  <div class="row no-gutters  margin-bottom-2 ">
      <div class="col-sm-5 col-5 checkout-mobile">
          <img src="/files/{{$cart->image}}" class="img-fluid"  alt="">
      </div>
      <div class="col-sm-7 col-7 px-2 py-4 mobile-checkout-container" >
          <h2 class="chch2 font-weight-bold checkout-list-h2">{{ $cart->name}}</h2>
          {{--  <i class="fa fa-ellipsis-v" aria-hidden="true" style="float: right;margin-top:-25px;color: #0000005c;"></i>  --}}
          <p class="chcp1" style="font-size: 12px;">{{substr($cart->description,0,30)}} </p>
    <h2 class="chch2 m-price"><strong>QR {{ $cart->price}} </strong></h2>
    
    <h2 style="margin-top: -5px !important" class="chch2 m-price"><strong>Quantity {{ $cart->quantity}} </strong></h2>

    {{ $cart->order_status ? $cart->order_status->status : 'Removed'}}
    <br>
    {{ $cart->created_at->toDateString()}}
  
      </div>
</div>
  <hr style="background-color: #d9d9d9;">
@endforeach


@endsection
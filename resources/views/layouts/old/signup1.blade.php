<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <!-- StyleSheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- GoogleFonts -->
    <link href="https://fonts.googleapis.com/css2?family=GFS+Didot&display=swap" rel="stylesheet">

    <!-- Slick slider -->
    <link rel="stylesheet" href="assets/slick/slick-master/slick/slick-theme.css">
    <link rel="stylesheet" href="assets/slick/slick-master/slick/slick.css">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
    <title>Sign up</title>
  <link rel="stylesheet" href="assets/css/own-style.css">
   </head>
   <body>
      <!-- Header -->    
    <div class="container ">
         <nav id="navbar_style" class="navbar navnav navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
           
            <div class="collapse navbar-collapse" id="navbarTogglerDemo03" style="margin-left:-122px;margin-top:9px;">
               	<a href="{{ url('/') }}" > 
					<img src="assets/images/logo.png" id="LogoR" style="margin-left:100%" class="img-fluid "alt="logo "/>
				</a>   
			  <ul class="navbar-nav ulbi mr-auto ml-auto mt-2 mt-lg-0">
                  <li class="nav-item libi" style="margin-left: 5rem;margin-right: 1rem;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/product') }}">Products</a>
                  </li>
                  <li class="nav-item libi" style="margin-left: 0px;margin-right: 32px;">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/about') }}">About</a>
                  </li>
                  <li class="nav-item libi">
                     <a class="nav-link navbi font-weight-bold  text-uppercase"  href="{{ url('/contact') }}">Contact</a>
                  </li>
               </ul>
            </div>
            <?php
            if( isset($_SESSION["logged_in"])) { ?>
            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="{{ url('/user-logout') }}">
               <i class="fa fa-sign-out" aria-hidden="true"></i></span></a>
               </li>
               <li class="li2 mr-3" style="display: inline-block;"><a href="{{ url('/checkout') }}"> 
               <i class="fa fa-shopping-cart" aria-hidden="true"></i></span></a>
               </li>
            </ul>
            <?php }
            else {?>
            <ul class="ul2 ml-auto" id="Icons" style="    margin-right: 0.5rem;">
               <li class="li2  mr-3" style="display: inline-block;"><a href="{{ url('/userlogin') }}"><i class="fa fa-user" style="-webkit-text-fill-color: white;
                  -webkit-text-stroke-width: 1px;
                  -webkit-text-stroke-color: black;  font-size: 14px;"></i></a></li>
               <li class="li2 font-weight-bold" style="display:inline"><a class="text-dark" href="">AR</a></li>
               </ul>
            <?php } ?>
         </nav>
      </div>
    <!-- Navbar -->

    <!-- navbar mbl view -->
   
    <!-- navbar mbl view -->

<!-- Header -->

    <!-- signup web view -->
<div class="container websi mt-5 mb-5" style="width:78%">
  <div class="row no-gutters " >
    <div class="col-lg-6 sigdi1" >
      <img src="assets/images/contact.PNG" class="sigdi1 w-100" alt="">
    </div>
    
      <div class="col-lg-6 px-5  sigdi2" style="padding: 5%;background-color: #F0E1DD;margin-left:-2px;">
      <form action="{{ route('regusers.store') }}" method="POST" enctype="multipart/form-data">
      @csrf
        <div style="margin-left: 46px; margin-right: 83px;">
          <div class="form-group">
          <input type="text" class="form-control text-left inpf input_reg"  name="full_name" id="full_name" placeholder="Full Name" required="required">
          </div>
        
          <div class="form-group mt-5">
          <input type="text" class="form-control text-left inpf input_reg" name="phone" id="mobile_no" placeholder="Mobile No" required="required">
          </div>
          <div class="form-group">
          <input type="email" class="form-control text-left inpf input_reg" name="email" id="email_login" placeholder="Email" required="required">
          </div>
          <div>
          <input type="text" class="form-control text-left inpf input_reg"  name="address" id="address" placeholder="Address" required="required">
          </div>
          <div class="form-group mt-5">
          <input type="text" class="form-control text-left inpf input_reg" name="zone" id="zone" placeholder="Zone No" required="required">
          </div>
          <div class="form-group">
          <input type="text" class="form-control text-left inpf input_reg" name="street" id="street" placeholder="Street No" required="required">
          </div>
          <div class="form-group">
          <input type="text" class="form-control text-left inpf input_reg" name="building" id="building" placeholder="Building No" required="required">
          </div>

          <div class="form-group mt-5">
          <input type="password" class="form-control text-left inpf input_reg" name="password" id="password_login" placeholder="Password" required="required">
          </div>
          <div class="form-group mt-5">
          <input type="password" class="form-control text-left inpf input_reg" id="re_password_login" placeholder="Re Enter Password" >
          </div>
          <input type="hidden" value="1" name="page">
        </div>
        <div class="row mt-5 button-signup-login">
          <div class="col-lg-6 mgr">
          <button type="submit" class="logbt btn-block py-3 button-signup-login-buttons" >Signup</button>
          </div>
          
          <div class="col-lg-6 mgl">
          <button class="logbt2 btn-block py-3 button-signup-login-buttons-1" ><a href="{{ url('/userlogin') }}">Log In</a></button>
          </div>
        </div>
        </form>
      </div>
    </div>
</div>
  <!-- signup web view -->

  <!-- signup mobile view -->
  <section class="signview">
 <div class="container py-2 sideeen">
{{--  <div id="mySidenav" class="sidenav">
<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
<a class="sidea" href="Product.html">Products</a>
<a class="sidea" href="about.html">About</a>
<a class="sidea" href="gallery.html">Gallery</a>
<a class="sidea" href="contact.html">Contact Us</a>
</div>  --}}
@include('layouts.partials.mobile_nav')

<div class="row">
<div id="sidebar">
<span style="font-size: 1;cursor: pointer;" onclick="openNav()" >
	<div class="bar"></div>
	<div class="bar"></div>
	<div class="bar"></div>
</span>
</div>
<div  id="Logoleft">
<a href="{{ url('/') }}">   <img src="assets/images/New Project.svg" id="Logo" class="img-fluid mblnavimg"  alt=""></a>
</div>
<div id="colgal">
<ul class="ul2" id="icon"  style="    position: absolute;">
<li class="li2  mt-1" style="display:inline;MARGIN-LEFT:-10PX">
 <a href="{{route('userlogin')}}">
 <i class="fa fa-user" style="-webkit-text-fill-color: white;
	-webkit-text-stroke-width: 2px;
	-webkit-text-stroke-color: black;  font-size: 16px;margin-left: -43px;margin-top: -2px;">
 </i>
 </a>
</li>
<li class="li2 mr-5" style="display:inline;margin-right: -31px !important;">
 <a href="{{route('checkout')}}">
	<i class="fa fa-shopping-bag fa-stack-1x " style="-webkit-text-fill-color: white;-webkit-text-stroke-width: 2px;-webkit-text-stroke-color: black;  font-size: 16px;margin-top:-3px;"></i>
 </a>
</li>
</ul>
</div>
</div>
</div>

  <div class="container mblsig" id="mblsig">
    <div class="row">
      <div class="col-lg-12 login-mobile">
        <h3 class="text-center  font-weight-bold pb-3" >Sign Up</h3>
		
          <div class="form-group">
            <input  type="text" class="form-control inpf2"  placeholder="Full Name">
          </div>
          <div class="form-group">
            <input  type="text" class="form-control  inpf2"  placeholder="Mobile number">
          </div>
          <div class="form-group">
            <input  type="email" class="form-control  inpf2"  placeholder="Email">
          </div>
          <div class="form-group">
            <input  type="email" class="form-control  inpf2"  placeholder="Re-enter Email address">
          </div>
          <div class="form-group">
            <input  type="password" class="form-control  inpf2"  placeholder="Password">
          </div>
          <div class="form-group">
            <input  type="password" class="form-control  inpf2"  placeholder="Re-enter password">
          </div>
		
		
		<input class="text-right mt-3"  style="margin-left: 0px;"type="checkbox"><span style="margin-left: -8px;">I accept the terms and conditions</span>
		
		<button type="button" class="btn-block" style="margin-top:1rem;" >Sign Up</button>
		<p class="text-center mt-5 login-text">Already have an account?<a href="{{route('userlogin')}}" class="sigla">Log In</a></p>
		
		</div>
    </div>
  </div>
 <!-- <hr style="    border-top: 1px solid rgba(0,0,0,0.3) !important;    margin-top: 0;"> -->
<div class="container mb-3 mt-5 border_top chotip">
	<div class="row">
		<div class="col-md-12 text-center">
			<img src="assets/images/vv.jpg" class="img-fluid pb-3" style="width: 80px;" alt="">

			<div class="text-center mt-2">
				<a class="text-dark" href="">
					<i class="fa fa-facebook fafoo"></i>
				</a>          
				<a class="text-dark" href="">
					<i class="fa fa-instagram fafoo"></i>
				</a> 
				<a class="text-dark" href="">
					<i class="fa fa-twitter fafoo"></i>
				</a>     
			</div>
			<h4 class="text-center mt-3 fh4" style="color:#00000094;">&copy; 2020  vequick.com, Inc.<br>All Rights Reserved.  Privacy Policy</h4>
		</div>
	</div>
</div>
</section>
<!-- footer -->
<div class="container badip mb-3 mt-5">
  <div class="row">
    <div class="col-md-12">
      <h4 class="text-center fh4">&copy; vequick.com,Inc.All rights reserved.Privacy Policy</h4>
      <div class="text-center mt-3">
        <a class="text-dark" href=""><i class="fa fa-facebook fafoo"></i></a>
<a class="text-dark" href="">        <i class="fa fa-instagram fafoo"></i>
</a>        
<a class="text-dark" href=""><i class="fa fa-twitter fafoo"></i>
</a>      
    </div>
  </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
    <script>
    function openNav(){
      document.getElementById("mySidenav").style.width="300px";
    }
    function closeNav(){
      document.getElementById("mySidenav").style.width="0px";
    }
  </script>
    </script>
  </body>
</html>
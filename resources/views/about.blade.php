
@extends('layouts.main')



@section('title')
<title>About US</title>
@endsection


@section('styles')
    
<style>
  .textarea-line{
    white-space: pre-wrap;
    {{--  white-space: pre-line;  --}}
}

@media only screen and (max-width: 768px){
  .about-us-image{
    display:none;
  }
}



</style>
@endsection

@section('content')

@php
    
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if( isset($_SESSION["logged_in"])) {
   $id = $_SESSION["id"];
} else {
   if( !isset($_SESSION["language"])) {
      $_SESSION['language'] = 'english';
   }
}
if(isset($_GET['arabic'])) {
   $_SESSION['language'] = 'arabic';
} else if(isset($_GET['english'])) {
   $_SESSION['language'] = 'english';
}



use App\aboutuses;
$aboutuses = aboutuses::all();
@endphp


<!-- section -->
@foreach ($aboutuses as $aboutus)
<div class="container text-center my-5">
  <div class="row">
    <div class="col-lg-6  col-md-6 col-sm-12 col-12 ">
        <h1 class="text-left our-product about-heading" >{{ session('locale') == 'ar' ? $aboutus->title_ar : $aboutus->title }}</h1>
        
        <h4 class="d-sm-block d-xs-block d-md-none d-lg-none d-xl-none " >{{ session('locale') == 'ar' ? $aboutus->title_ar : $aboutus->title }}</h4>

        <p class="about-para textarea-line">{{ session('locale') == 'ar' ? $aboutus->description_ar : $aboutus->description }}</p>
    </div>
    <div class=" col-lg-6 col-md-6 px-3 about-us-image ">
      <img style="width:80%" src="files/{{ $aboutus->image}}" alt="about-us-image">
    </div>
  </div>
</div>
@endforeach
<div class="container-fluid abdiv mt-5" >
  <div class="container text-center about-container" class="divclr">
  <div class="row">
    <div class="col-md-4 about-div" >
        <p class="ban2p2 mt-3 about-bottom-p" >
       1- {{__('Open the filter')}}.
      </p>
      <img src="assets/images/03.jpg" class="  h-50 aaaa" alt="">
      <img src="assets/images/1png1.jpg" class="  bbbb"  style="width: 100px;height: auto;" alt="">
      
    </div>
    
    <div class="col-md-4 about-div" >
        <p class="ban2p2 mt-3 about-bottom-p">
        2- {{__('Put it on your cup')}}.
      </p>
      <img src="assets/images/01.jpg" class="  h-50 aaaa" alt="">
      <img src="assets/images/2png2.jpg" class=" bbbb"  style="width: 100px;height: auto;" alt="">
      
    </div>
    
    <div class="col-md-4 about-div" >
        <p class="ban2p2 mt-3 about-bottom-p" >
       3- {{__('Pour 200 ml of hot water')}}.
      </p>
      <img src="assets/images/02.jpg" class="  h-50 aaaa" alt="">
      <img src="assets/images/3png3.jpg" class="  bbbb" style="width: 140px;height: auto;" alt="">
      
    </div>
    
    
    
    
  </div>
  <p>Enjoy it</p>
</div>
</div>


@endsection

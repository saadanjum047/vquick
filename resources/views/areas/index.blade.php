@extends('areas.layout') 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('areas.create') }}"> Add New Area</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
      <tr>
        <td>#</td>
        <td width="50%">Name</td>
        <td>Charges</td>
        <td>Actions</td>
      </tr>

      @foreach($areas as $i => $area)
      <tr>
        <td>{{$i + 1}}</td>
        <td>{{$area->area}}</td>
        <td>{{$area->charges}} QAR</td>
        <td> 
          <a href="{{route('areas.edit' , $area->id)}}" class="btn btn-primary btn-sm">Edit</a>  
          <a href="{{route('areas.delete' , $area->id)}}" class="btn btn-danger btn-sm">Delete</a>  
        </td>
      </tr>
      @endforeach
    </table>
  
    {{--  @if($areas->links())
    
    {{$areas->links()}}
    
    @endif  --}}
      
@endsection
@extends('areas.layout')
@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Add Area</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('areas.index') }}"> Back</a>
      </div>
    </div>
  </div>
      
  @if ($errors->any())
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  

  

  <form action="{{ route('areas.update' , $area->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="row" style="width:80%;margin:auto;">   

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Area Name:</strong>
          <input type="text" class="form-control" value="{{$area->area}}" name="name" placeholder="Area Name" required="required"/>
        </div>
      </div>
      
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Area Name:</strong>
          <input type="text" class="form-control" value="{{$area->ar_area}}" name="ar_area" placeholder="Arabic Name" required="required"/>
        </div>
      </div>
       <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Charges:</strong>
          <input type="text" class="form-control" value="{{$area->charges}}" name="charges" placeholder="Area Charges" required="required"/>
        </div>
      </div>
      
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </div>    
  </form>
  @endsection
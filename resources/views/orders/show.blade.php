@extends('orders.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Products</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('orders.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Order Number:</strong>
                {{ $order->order_number }}<br>
                <strong>Payment Status:</strong>
                {{ $order->payment_method }}<br>
                <strong>Quantity:</strong>
                {{ $order->quantity }}<br>
                <strong>Available:</strong>
                {{ $order->available }}<br>
                <strong>Status:</strong>
                {{ $order->status }}<br>
                <strong>Full name:</strong>
                {{ $order->full_name }}<br>
                <strong>Address:</strong>
                {{ $order->address }}<br>
                <strong>Date:</strong>
                {{ $order->date }}<br>
                <strong>Product name:</strong>
                {{ $order->product_name }}
            </div>
        </div>
    </div>
@endsection
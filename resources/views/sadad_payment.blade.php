

@extends('layouts.main')


@section('content')


@php



if($carts->count() == 0 ){
//return redirect('/product')->with('error' , 'You dont have anything in the cart');

}

@endphp

<div class="row text-center">
    <div class="offset-2 col-8">
        <div class="alert alert-info" style="background-color:#627976; color:white; border-color:#627976;" >You Will be redirected to the Sadad Payment Page</div>

    </div>
</div>



 <form id="sadad-payment" action="https://sadadqa.com/webpurchase" method="post">
          @csrf
          <input type="hidden" class="form-control"  name="merchant_id" id="merchant_id" value="{{$merchant_id}} ">
              
          <input type="hidden" class="form-control"  name="secret_key" id="secret_key" value="{{$secret_key}}">
      
          <input type="hidden" class="form-control total-amount"  name="TXN_AMOUNT" id="amount" value="300">
              

          <input type="hidden"  class="form-control" name="ORDER_ID" id="order_id" value="{{$orderstatuses->id}}">
      
      
          <input type="hidden"  class="form-control" name="MOBILE_NO" id="MOBILE_NO" value="03147637613">
      
          <input type="hidden"  class="form-control" name="CALLBACK_URL" id="callback_url" value="{{url('api/sadad-payment-success')}}">
          

          <input type="hidden"  class="form-control" name="USER_ID" id="USER_ID" value="{{ $id ?? session()->getId()}}">
          

          @foreach($orderstatuses->carts as $k => $cart)
                <input type="hidden" id="itemname" class="form-control" name="productdetail[{{$k}}][itemname]" value="{{$cart->name}}">
                <input type="hidden" id="quantity" value="{{$cart->quantity}}"  class="form-control sadad-cart-quantity-{{$cart->id}}" name="productdetail[{{$k}}][quantity]" >
                
                <input type="hidden" id="price" value="{{$cart->price/$cart->quantity}}"  class="form-control sadad-cart-price-{{$cart->id}}" name="productdetail[{{$k}}][amount]">
          @endforeach

          <input type="hidden" id="itemname" class="form-control" name="productdetail[{{$orderstatuses->carts->count()}}][itemname]" value="Shipping Charges">

          <input type="hidden" id="quantity" value="1"  class="form-control sadad-cart-quantity-{{$cart->id}}" name="productdetail[{{$orderstatuses->carts->count()}}][quantity]" >
          
          <input type="hidden" id="price" value="{{$orderstatuses->shipping_charges}}"  class="form-control sadad-cart-price-{{$cart->id}}" name="productdetail[{{$orderstatuses->carts->count()}}][amount]">


          {{-- @if(isset($shipping))
           <!--<input type="hidden" id="itemname" class="form-control" name="productdetail[{{$carts->count()}}][itemname]" value="Shipping Fee">-->
           <!--       <input type="hidden" id="quantity" value="1"  class="form-control" name="productdetail[{{$carts->count()}}][quantity]" >-->
                  
           <!--       <input type="hidden" id="price" value="{{$shipping->fee}}"  class="form-control" name="productdetail[{{$carts->count()}}][amount]">-->
                  
          @endif
          <!--<input type="submit" value="Submit">--> --}}

      </form>    
      
@endsection
@section('scripts')
      
      <script>
        $(document).ready(function(){
            $('#sadad-payment').submit();
            // console.log('submit form');
        })
      </script>

@endsection
<header class="main-header">
    <a href="{{url('home')}}" class="logo">
      <span class="logo-mini"><b></b>VQuick</span>
      <span class="logo-lg"><b></b>VQuick</span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
      <a href="#" style="position:absolute;" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu" style="position: absolute;right: 10%;width:20%;">
        <div>
          <div style="list-style: none;float: left;width: 30%;padding: 5%;">
            <!--<li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o" style="color:white;"></i>
                <span class="label label-warning">10</span>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <span class="dropdown-item dropdown-header">15 Notifications</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> 4 new messages
                <span class="float-right text-muted text-sm">3 mins</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-users mr-2"></i> 8 friend requests
                <span class="float-right text-muted text-sm">12 hours</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-file mr-2"></i> 3 new reports
                <span class="float-right text-muted text-sm">2 days</span>
              </a>
              <div class="dropdown-divider"></div>
                <a href="{{ url('/notifications') }}" class="dropdown-item dropdown-footer">See All Notifications</a>
              </div>
            </li>-->
          </div>
          <div style="float: left;width: 50%;padding: 5%;">
            <form method="POST" action="{{ route('logout') }}">
                @csrf
                <button type="submit" style="background-color: transparent;border: none;color:white;"><i style="font-size:24px" class="fa">&#xf08b;</i>Logout</button>
              </form>
          </div>
        </div>
          
      </div>
    </nav>
  </header>
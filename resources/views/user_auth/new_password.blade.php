



@extends('layouts.main')



@section('title')
<title>Reset Email From</title>
@endsection

@section('content')
    
<div class="container websi mt-5"  style="width: 78%;">
  <div class="row no-gutters " >
    <div class="col-lg-6 sigdi1" >
      <img src="{{asset('assets/images/contact.PNG')}}" class="sigdi1 w-100" alt="">
    </div>
    <div class="col-lg-6 px-5  sigdi2" style="padding-top: 18%;background-color: #F0E1DD;margin-left:-2px;">
    
      <form action="{{route('save-new-password')}}" method="POST" enctype="multipart/form-data">
        @csrf
        
      <div style="margin-left: 46px; margin-right: 83px;">
		  <div class="form-group">
			<input type="password" class="form-control text-left inpf input_reg desktop-fields" name="password" placeholder="{{__('Password')}}" required >
          </div>
          
          <div class="form-group">
			<input type="password" class="form-control text-left inpf input_reg desktop-fields" name="conf_password" placeholder="{{__('Confirm Password')}}" required >
		  </div>
        </div>
        <input type="hidden" name="email" value="{{$email}}" />
 	<div class="row mt-5 button-signup-login">
	  <div class="offset-lg-2 col-lg-8 mgr">
		<button type="submit" class="logbt btn-block py-3 button-signup-login-buttons uncfocused-item" >{{__('Save Password')}}</button>
	  </div>
      <p class="text-center mt-5 login-text">{{__('Do not have an account')}}?<a href="{{ url('/signup') }}" class="sigla">{{__("Register Now")}}</a></p>

  </div>
      </form>
{{--  <p class="text-center mt-5 forget-password"><a  href="{{route('userlogin')}}" >{{__('Sign In')}}?</a></p>  --}}
    </div>
  </div>
</div>




<section class="formview">
  <div class="container mblsig mt-5">
   <div class="row">
     <div class="col-lg-12 login-mobile">
       <h3 class="text-center  font-weight-bold pb-3" >{{__('Reset Password')}}</h3>
   <form action="{{route('save-new-password')}}" method="POST" >
     @csrf
   <div class="form-group">
     <input  type="password" class="form-control inpf2 mobile-login " name="password" placeholder="{{__('Password')}}" required>
   </div>
   
   <div class="form-group">
     <input  type="password" class="form-control inpf2 mobile-login " name="conf_password" placeholder="{{__('Confirm Password')}}" required>
   </div>
   <input type="hidden" name="email" value="{{$email}}" />
   <button type="submit" class="btn-block" >{{__('Save Password')}}</button>
   <p class="text-center mt-5 login-text">{{__('Do not have an account')}}?<a href="{{ url('/signup') }}" class="sigla">{{__("Register Now")}}</a></p>
 </form>
    </div>
   </div>
 </div>
<!-- <hr style="border-top: 1px solid rgba(0,0,0,0.3) !important;    margin-top: 0;"> -->

</section>


@endsection


@extends('layouts.admin')

@section('content')
        
        <div style="width: 80%;" class="text-center">
            
            @if(session('success'))
            <p class="alert alert-success"> {{session('success')}} </p>
            @endif
            @if($errors->any())
            @foreach ($errors->all() as $error)
                <p class="alert alert-danger"> {{$error}} </p>
            @endforeach
            @endif
            <form id="form" action="{{route('update-discounts')}}" method="POST">
            <table class="table" >
                <tr>
                <th>Orders</th>
                <th>Percentage</th>
            </tr>
            <tr>
                
                    @csrf
                    <td> <input type="number" class="form-control" required name="orders" placeholder="Number of Orders" value="{{ $discount->orders ?? ''}}" >  </td>
                    <td> <input class="form-control" type="number" required name="discount" placeholder="Percentage Of Discount" value="{{ $discount->percentage ?? '' }}" >  </td>
                    
                </tr>
            </table>
            <button type="submit" class="btn btn-success float-right" > Update</button>
        </form>
    </div>
      
      
      
@endsection
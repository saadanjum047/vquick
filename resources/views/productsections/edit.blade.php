@extends('productsections.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Product Section</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('productsections.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('productsections.update',$productsection->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row" style="width:80%;margin:auto;">
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Title:</strong>
                <input type="text" class="form-control" name="title" value="{{ $productsection->title }}"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <strong>Arabic Title:</strong>
                  <input type="text" class="form-control" name="title_ar" value="{{ $productsection->title_ar }}" placeholder="Enter Arabic Title" required="required"/>
                </div>
              </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Content:</strong><?php
                $a=0;
                if($productsection->content == 'product') {
                  $a="categories";
                } else {
                  $a="product";
                }?>
                <select class="form-control" name="content">
                    <option selected value="<?= $productsection->content ?>"><?= $productsection->content ?></option>
                    <option value="<?= $a ?>"><?= $a ?></option>
                </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection
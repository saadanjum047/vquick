@extends('productsections.layout') 
@section('content')
<?php $inc=0; ?>
@foreach ($productsections as $productsection)
<?php $inc++; ?>
@endforeach

              
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
              <?php if($inc==0) { ?>
                <a class="btn btn-success" href="{{ route('productsections.create') }}"> Create New Product Section</a>
              <?php } ?>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Title</th>
          <th>Content</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($productsections as $productsection)
        <tr>
          <td>{{ ++$i }}</td>
          <td>{{ $productsection->title }}</td>
          <td>{{ $productsection->content }}</td>
          <td>
            <form id="delete-form-{{$productsection->id}}" action="{{ route('productsections.destroy',$productsection->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('productsections.show',$productsection->id) }}">Show</a>
              <a class="btn btn-primary" href="{{ route('productsections.edit',$productsection->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}
              
              <button type="button" onclick="ask_delete({{$productsection->id}})" class="btn btn-danger">Delete</button>

            </form>
          </td>
        </tr>
        @endforeach
    </table>
@endsection
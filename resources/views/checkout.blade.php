



@extends('layouts.main')



@section('title')
<title>Checkout</title>
@endsection


@section('styles')
    
<style>

  @media screen and (max-width:768px){
    #delivery_area{
      margin-top: 10px;
    }
  }

  .counter1 {
    width:90px !important;
  }

  .btn-danger:focus {
    border: none;
    box-shadow: none;

  }
  .remove-cart-button{
    background-color: white;
    border: none;
    box-shadow: none;
  }
  .remove-cart-button:focus {
    border: none;
    box-shadow: none;
  }
  .checkout-list-p{
    line-height: 18px !important;
  }

  .swal-footer{
    display: block;
  }
  .swal-footer{
    text-align: center;
  }
  .swal-button--guest{
    background-color: #666666 !important;
    border: 1px solid #666666 !important;
  }
  
  .swal-button--login{
    color: #999999;
  }
  
 
  .swal-button--guest:focus {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;
    box-shadow : none !important;
  }
  .swal-button--guest:hover {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;
  }  

  .swal-button--guest:not([disabled]):hover {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;

  }
  
  .swal-button--guest:not([disabled]):hover {
    background-color: #666666 !important;
    border: 1px solid #666666 !important;

  }
  .place-order-button:disabled{
    text-align: -webkit-center;
    }

  .loader {
    border: 7px solid #f3f3f3;
    border-radius: 50%;
    border-top: 7px solid #627976;
    width: 40px;
    height: 40px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
    text-align: center;
  }
  
  /* Safari */
  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }
  
  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }

</style>
  @endsection

@section('content')
    

<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
if(isset($_SESSION["id"])){
  $id = $_SESSION["id"];
}
$price=0;
$z=0;
?>


@php
if(isset($_SESSION["id"])){
  $customer = App\regusers::find($_SESSION["id"]);   
}
@endphp
<!-- Header -->

       <div class="container mt-5 badip">
      <div class="row no1">
        <div class="col-md-6">
          <h1 class="chch1 checkout-h1">{{__('Checkout')}}</h1>
        </div>
        <div class="col-md-6">
          <h2 class="text-right"><a class="chch2 checkout-h2" href="/product">{{__('Continue Shopping')}}</a></h2>
        </div>
      </div>
    </div>
    <div class="container mb-4 mt-3 badip">
      <div class="row no2">
        <div class="col-md-6">
          <p class="chcp1 checkout-main-row">{{__('Items')}}</p>
        </div>
        <div class="col-md-3">
          <p class="chcp1 checkout-main-row">{{__('Quantity')}}</p>
        </div>
        <div class="col-md-3">
          <p class="chcp1 checkout-main-row">{{__('Price')}}</p>
        </div>

      </div>
      {{--  @dd($carts)  --}}
      <hr class="no3" style="background-color: #d9d9d9;margin-top: -10px;" >
      @foreach ($carts as $cart)
      <div class="row">
        <div class="col-md-2">
          <img src="files/{{ $cart->image}}" style="height: 160px;width: 550px;" class="img-checkout py-2" alt="">
        </div>
        <div class="col-md-4 pt-5">
          <h2 class="chch2 checkout-list-h2 ">{{ $cart->product->proper_name}}</h2>
          <p class="chcp1 checkout-list-p " style="line-height: 18px !important" >{{  Str::limit($cart->product->proper_description,100) }}</p>
        </div>
        <div class="col-md-3 pt-5" >
          <div class='counter-checkout'>
            <!--<div class='decrease-counter' onclick='decreaseCount(event, this)'>-</div>-->
              {{--  <input type='text' value='{{ $cart->quantity }}'  >  --}}
              <div id="quantity-buttons" style="border:1px solid #d3aea6 ; border-radius: 5px; height: 36px; "
              <div class='counter1'  style="margin-top:1px;">
                <div id="down" class='down' onclick='decreaseCount(event, this ,  {{$cart->id }})'>-</div>
                <input type='text' value='{{ $cart->quantity }}' id="quantity" name="quantity" class="counter_1" value='1'  >
                <div class='up' id="up" onclick='increaseCount(event, this , {{$cart->id}} )'>+</div>
              </div>
              
            <!--<div class='decrease-counter' Onclick='increaseCount(event, this)'>+</div>-->
            </div>
          </div>
          <div class="col-md-2 " style="padding-top: 62px;">
            <h2 class="chch2 c-price cart-price-{{$cart->id}}" >QR {{ $cart->original_price}}</h2>
          </div>
          <div class="col-md-1">  
            <form id="cart-{{$cart->id}}" action="{{ route('carts.destroy',$cart->id) }}" method="POST">
              @csrf
              @method('DELETE')
              <a href="javascript:;" onclick=" $('#cart-{{$cart->id}}').submit(); " class="remove-cart-button"><i class="fa fa-close chchfa"></i></a>
            </form>
          </div>
        </div>
        <hr class ="no3" style="background-color: #d9d9d9;">
        <?php
        $cart_price=$cart->price;
        $price+= $cart_price;
        ?>
        @endforeach
      </div>
    </div>

<!-- MOBILE DESIGN -->

<div class="container chotip  mt-4">
  @foreach($carts as $cart)
	<div class="row no-gutters  margin-bottom-2 ">
		<div class="col-sm-5 col-5 checkout-mobile">
			<img src="/files/{{$cart->image}}" class="img-fluid"  alt="">
		</div>
		<div class="col-sm-7 col-7 px-2 py-4 mobile-checkout-container" >
			<h2 class="chch2 font-weight-bold checkout-list-h2">{{ $cart->product->proper_name}}</h2>
      <a href="#" onclick="delete_cart({{$cart->id}})" > <i class="fa fa-times-circle" aria-hidden="true" style="float: right;margin-top:-25px;color: #0000005c;"></i></a>
      
      <form id="delete-cart-{{$cart->id}}"   action="{{ route('carts.destroy',$cart->id) }}" method="POST">
        @csrf
        @method('DELETE')
      </form>
      
        <button type="submit"  class="btn-block btnchch py-3 checkout-btn uncfocused-item" id="placebutn">{{__('PLACE ORDER')}}</button>

			{{--  <i class="fa fa-ellipsis-v" aria-hidden="true" style="float: right;margin-top:-25px;color: #0000005c;"></i>  --}}
			<p class="chcp1" style="font-size: 12px;">{{ Str::limit($cart->product->proper_description , 200)}} </p>
			<h2 class="chch2 m-price "><strong class="cart-price-{{$cart->id}}" >QR {{$cart->original_price}}</strong></h2>

			<div class='counter mt-1'>
				<div class='decrease-counter'  onclick='decreaseCount(event, this , {{$cart->id}} )'>-</div>
				<input type='text' value='{{$cart->quantity}}' class="chotoo" >
				<div class='up' style="background-color: #dcdcdc;font-size: 20px;width: 30px;height: 30px;padding-top: 2px;padding-left: 9px !important;" onclick='increaseCount(event, this , {{$cart->id}})'>+</div>
			</div>
		</div>
  </div>
  @php
  $cart_price=$cart->price;
  $price+= $cart_price;

  @endphp
  <hr style="background-color: #d9d9d9;">
  @endforeach
	
  {{--  carts end here  --}}

</div>

@php
//$total_price = $shippings->first()->fee + array_sum($carts->pluck('price')->toArray());
$total_price = array_sum($carts->pluck('price')->toArray());
@endphp

  @if($carts->count() > 0)
  <div class="container px-4 mt-2">
    <div class="row mt-5">
      <div class="col-md-12">
        <h2 class="shiping-fee" >{{__('Shipping fees based on delivery area (Within 1 or 2 working days)')}}</h2>
        <!--<h2 id="ddddd" class="shiping-fee text-right">QR {{ $shippings->first()->fee}}</h2>-->
        
      </div>
      <form method="post" action="/order-cart" class="place-order-form w-100">
        <input type="hidden" name="total_price" class="total-amount" value="{{ $total_price }}" >
        <input type="hidden" name="shipping_charge" class="shipping-charges" value="{{ isset($customer) && isset($customer->area) ? $customer->area_id : '' }}" >
        <input type="hidden" name="id" value="{{ $id ?? session()->getId() }}" >
        @csrf
        
        <div class="row">

      <div class="col-xs-12 col-12 col-sm-12 col-md-3 col-lg-3">
        
        <select class="form-control" id="payment_method" name="payment_method" required >
          <option disabled>{{__("Select Payment Option")}}</option>
          <option value="cash on delivery" @if(isset($_SESSION['preffered_payment_method']) && $_SESSION['preffered_payment_method'] == 'cash on delivery' ) selected @endif >{{__('Cash On Delivery')}}</option>
          <option value="sadad" @if(isset($_SESSION['preffered_payment_method']) && $_SESSION['preffered_payment_method'] == 'sadad' ) selected @endif >{{__("Online payment (Sadad)")}}</option>
        </select>
      </div>



        
      
      <div class="col-xs-12 col-12 offset-md-6 offset-lg-6 col-sm-12 col-md-3 col-lg-3">
        <select class="form-control float-right" name="delivery_area" required id="delivery_area" >
          <option value="">{{__("Select Area")}}</option>
          @foreach($areas as $i => $area)
          <option @if( isset($customer) && isset($customer->area) && $area->id == $customer->area_id) selected @endif value="{{$area->id}}">{{$area->proper_area}} | <span>{{$area->charges}}</span> </option>
          @endforeach
        </select>
      </div>
    </div>

    </form>
      
      @foreach ($shippings as $shipping)
      <div class="col-md-6 ">
        {{--  <h2 id="ddddd" class="shiping-fee text-right">QR {{ $shipping->fee}}</h2>  --}}
      </div>
      @endforeach
    </div>
	  <hr class="no3" style="background-color: #d9d9d9;margin: 1rem 0;">
	<div class="row mv">
		<div class="col-sm-6 col-6 col-md-6">
      @if(isset($customer) && $customer->has_discount == 1 )
		   <h2 class="shiping-fee-s">{{__('Sub total')}}</h2> 
       <h2 class="shiping-fee-s">{{__('Discount')}}</h2> 
       @endif
       
		</div>
		<div class="col-sm-6 col-6  col-md-6">
      @if(isset($customer) && $customer->has_discount == 1)
		   <h2 id="" class="shiping-fee-s text-right sub-total ">QR {{ array_sum($carts->pluck('original_price')->toArray()) }} </h2> 
       <h2 id="" class="shiping-fee-s text-right ">{{ $discount->percentage }}%</h2> 
       @endif
		</div>
	</div>
	<div class="row mt-4 ">
    
		<div class="col-md-6">
		  <h2 class="shiping-fee">{{__('Sub Total')}}<span class="mobile-p-v">QR  <span class="total-amount"> {{$total_price}}</span></span></h2>
		  
      <h2 class="shiping-fee">{{__('Shipping Fee')}}<span class="mobile-p-v">QR  <span class="shipping-charges"> {{ isset($customer) && isset($customer->area) ? $customer->area->charges : '0'}}</span></span></h2>

		  <h2 class="shiping-fee-m">{{__('Total')}}<span class="mobile-p-v">QR  <span class="full-total"> {{$total_price +  ( isset($customer) && isset($customer->area)  ? $customer->area->charges : 0) }}</span></span></h2>
		</div>

		<div class="col-md-6">
		  <h2 id="dddddd" class=" shiping-fee m-n-v text-right" >QR <span class="total-amount">{{$total_price}} </span></h2>
		  <h2 id="dddddd" class=" shiping-fee m-n-v text-right " >QR <span class="shipping-charges"> {{ isset($customer) && isset($customer->area)  ? $customer->area->charges : 0 }} </span> </h2>
		  <h2 id="dddddd" class=" text-right m-n-v " >QR <span class="full-total">{{$total_price +  (isset($customer) && isset($customer->area)  ? $customer->area->charges : 0) }} </span> </h2>
		</div>
    <div class="col-md-12 mt-4" >  
      <button type="button" @if(isset($_SESSION['id'])) onclick="submit_order_form()"  @else  onclick="ask_how_to_checkout()" @endif  class="place-order-button btn-block btnchch py-3 checkout-btn uncfocused-item" id="placebutn">{{__('PLACE ORDER')}}</button>
    </div>
	</div>
</div>
@else
<div class="container px-4 mt-2">
	<div class="row mt-5">
		<div class="col-md-6">
		  <h2 class="shiping-fee" >{{__('Shipping fee')}}</h2>
		</div>
		<div class="col-md-6 ">
		  <h2 id="ddddd" class="shiping-fee text-right">QR 0</h2>
		</div>
	</div>
	<hr class="no3" style="background-color: #d9d9d9;margin: 1rem 0;">
	<div class="row mv">
		<div class="col-md-6">
		  <h2 class="shiping-fee-s">{{__('Sub total')}}</h2>
		</div>
		<div class="col-md-6">
		  <h2 id="ddddd" class="shiping-fee-s text-right">QR 0</h2>
		</div>
	</div>
	<div class="row mt-4 ">
		<div class="col-md-6">
		  <h2 class="shiping-fee-m">{{__('Total')}}<span class="mobile-p-v">QR 0</span></h2>
		</div>
		<div class="col-md-6">
		  <h2 id="dddddd" class=" text-right m-n-v">QR 0</h2>
		</div>
    <div class="col-md-12 mt-4" >
      @if($carts->count() > 0)
      <button class="btn-block btnchch py-3 checkout-btn uncfocused-item" id="placebutn">{{__('PLACE ORDER')}}</button>
      @endif
      </form>
    </div>
	</div>
</div>
@endif

<!-- MOBILE DESIGN ENDS -->
<div class="col-md-12 mt-4">
        @if(isset($carts) && $carts->count() > 0)
          <button type="button" @if(isset($_SESSION['id'])) onclick="submit_order_form()"  @else  onclick="ask_how_to_checkout()" @endif  class="m-check-out-button uncfocused-item place-order-button">{{__("PLACE ORDER")}}</button>
        @endif
        </div>



@if(isset($carts) && $carts->count() > 0)

<form action="{{route('guest-signup')}}" id="guest-signup-form" method="POST" >
  @csrf
  <input type="hidden" class="total-amount" name="total_price" value="{{$total_price}}" />
  <input type="hidden" id="payment_method_guest" name="payment_method" value=" @if(isset($_SESSION['preffered_payment_method']) && $_SESSION['preffered_payment_method'] == 'sadad' ) sadad @else cash on delivery @endif" />
  <input type="hidden" name="shipping_charge" class="shipping-charges" value="{{ isset($customer) ? $customer->area_id : '' }}" >
</form>

@endif

<form id="sadad-payment" method="get" action="{{route('sadad-payment')}}" >
    
      <input type="hidden" class="total-amount" name="total_price" value="{{$total_price}}" />

          <input type="hidden" name="id" value="{{ $id ?? session()->getId() }}">
</form>

       

@endsection


@section('scripts')
<script>


  var areas = {!! $areas ?? 'NULL' !!};

  var current_area = {!! $customer->area ?? 'null' !!};

  $('#delivery_area').on('change' , function(e){

    //console.log(areas)
    
    areas.forEach(function(area){
      if(area.id == $('#delivery_area').val()){
        console.log($('.total-amount').html())
        var new_val = parseInt($('.total-amount').val()) + area.charges;
        //console.log(new_val)

        current_area = area;

        $('.shipping-charges').html(area.charges);
        $('.shipping-charges').val(area.id);
        $('.full-total').html('');
        $('.full-total').html(new_val);
      }
    })
    //console.log($('#delivery_area').val())
  })


  
  function submit_order_form(){

    if($('.shipping-charges').val() == ''){
      swal('Please Select Shipping Area')
    }else{


    $('.place-order-button').prop('disabled' , true);
    $('.place-order-button').html('<div class="loader"></div>');
    $('.place-order-form').submit();
    }
      
    //$('#sadad-payment').modal('show');
    // if($('#payment_method').val() == 'sadad'){
        
    //     $('#sadad-payment').submit();
        
    // }else{
    //     console.log('submit original form');
    //     $('.place-order-form').submit();
    
    // }
    
  }

  function ask_how_to_checkout(){
    
    console.log($('.shipping-charges').val())
    
    if($('.shipping-charges').val() == ''){
      swal('Please Select Shipping Area')
    }else{

      
      swal("{{__('How would you link to continue')}}", {
      buttons: {
        login: {
          text: "{{__('Login')}}",
          value: "login",
        },
        
        guest : {
          text: "{{__('Guest')}}",
          value: "guest",
        },
      },
    })
    .then((value) => {
      switch (value) {
        
        case "login":
        //console.log($('#payment_method').val());
        
        $('.place-order-button').prop('disabled' , true);
        $('.place-order-button').html('<div class="loader"></div>');
        $('.place-order-form').submit();
        break;
        
        case "guest":
        //console.log($('#payment_method').val());
        
        $('.place-order-button').prop('disabled' , true);
        $('.place-order-button').html('<div class="loader"></div>');
        $('#guest-signup-form').submit();
        break;
        
        default:
      }
    });
  }
  }
  
  $('#payment_method').on('change' , function(){
    $('#payment_method_guest').val( $('#payment_method').val() )
  })
  
  function delete_cart(id){
    //console.log(id)
    document.getElementById("delete-cart-"+id).submit();    
  }
  

  function increaseCount(e, el , cart_id) {
    
    var input = el.previousElementSibling;
    var value = parseInt(input.value, 10);
    value = isNaN(value) ? 0 : value;
    
    //el.onclick = false;


    $.ajax({
      url: '{{url("/")}}/update-cart-quantity-ajax/'+ cart_id + '/' + (value + 1) ,
      success: function(res){
        
        //console.log(res)
        
        //$('.cart-price-'+cart_id ).html( 'QR ' + res.cart.price);
        $('.cart-price-'+cart_id ).html( 'QR ' + res.cart.original_price);
        
        //$('.total-amount').html( res.sub_total);
        //$('.total-amount').val( res.sub_total);
        
        $('.total-amount').html( res.total_amount);
        $('.total-amount').val( res.total_amount);
        
        //$('.sadad-cart-price-'+cart_id).val( res.cart.price);
        $('.sadad-cart-quantity-'+cart_id).val( res.cart.quantity);
        
        $('.sub-total').html('QR ' +  res.sub_total);
        


        
        if(current_area){
          $('.full-total').html('');
          $('.full-total').html(res.total_amount + parseFloat(current_area.charges) );
        }else{
          $('.full-total').html('');
          $('.full-total').html(res.total_amount );  
        }
        
        
        value++;
        input.value = value;
        

    }
    }).fail(function(errors){
      //swal(errors.responseJSON.message);

      if(errors.responseJSON.error_code == 1){

        swal({text: '{{__("Category restriction exceeded")}}' , timer:5000});
  
      }
      if(errors.responseJSON.error_code == 2){
  
        swal({ text: '{{__("This item stock is not enough")}}', timer:5000});
  
      }
  
    });
    
    //el.onclick = true;
    //$(el).on('click',increaseCount(event , this , cart_id)); 

  }
  function decreaseCount(e, el , cart_id) {
  var input = el.nextElementSibling;
  var value = parseInt(input.value, 10);
  if (value > 1) {
  value = isNaN(value) ? 0 : value;

  //el.onclick = false;


  $.ajax({
    url: '{{url("/")}}/update-cart-quantity-ajax/'+ cart_id + '/' + (value - 1) ,
    success: function(res){

      //$('.cart-price-'+cart_id ).html( 'QR ' + res.cart.price);
      $('.cart-price-'+cart_id ).html( 'QR ' + res.cart.original_price);

      //$('.total-amount').html( res.sub_total);
      //$('.total-amount').val( res.sub_total);

      $('.total-amount').html( res.total_amount);
      $('.total-amount').val( res.total_amount);

        //$('.sadad-cart-price-'+cart_id).val( res.cart.price);
        $('.sadad-cart-quantity-'+cart_id).val( res.cart.quantity);


      $('.sub-total').html('QR ' +  res.sub_total);
      
      
      if(current_area){
        $('.full-total').html('');
        $('.full-total').html(res.total_amount + parseFloat(current_area.charges) );
      }else{
        $('.full-total').html('');
        $('.full-total').html(res.total_amount );
      }

      value--;
      input.value = value;


  }
  }).fail(function(errors){

    if(errors.responseJSON.error_code == 1){

      swal({ text: '{{__("Category restriction exceeded")}}' , timer:5000 });

    }
    if(errors.responseJSON.error_code == 2){

      swal( { text: '{{__("This item stock is not enough")}}' , timer:5000 });

    }

  });

  }
  }
  </script>
  <script>
  function openNav(){
    document.getElementById("mySidenav").style.width="300px";
  }
  function closeNav(){
    document.getElementById("mySidenav").style.width="0px";
  }
</script>
<?php
  if(isset($_SESSION['message'])) 
  { 
    $login_message= $_SESSION['message'];?>
    <script>
        swal("{{ $login_message }}");
        setTimeout(() => {  window.location.href = "{{ url('/checkout') }}"; }, 1000);
    </script><?php
    unset($_SESSION['message']);
  }
?>
  </script>
@endsection
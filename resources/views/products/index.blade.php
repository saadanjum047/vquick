@extends('products.layout') 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.exports' , json_encode($products->pluck('id')->toArray() ) ) }}"> Export</a>
                <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Products</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Order</th>
          <th>Name</th>
          <th>Description</th>
          <th>Image</th>
          <th>Price</th>
          <th>Status</th>
          <th>Stock Available</th>
          <th>Category</th>
          <th width="280px">Action</th>
        </tr>

        <tbody id="current-files" >
        @foreach ($products as $k => $product)
        <tr class="draggable-rows" id="{{$product->id}}" name="{{$k}}" >
          <td>{{ $k + 1 }}</td>
          <td class="index-td">{{ $product->product_order + 1 }}</td>
          <td>{{ $product->name }}</td>
          <td>{{ $product->description }}</td>
          <td><img style="width:100px;height: 100px;" src="files/{{$product->image}}" /></td>
          <td>{{ $product->price }} QTR</td>
          <td> 
              @if($product->is_approved == 1)
                <span class="label label-success">Active</span>
              @else
                <span class="label label-danger">Disabled</span>
              @endif
            </td>
          <td>{{ $product->stock_available }}</td>
          <td>{{ $product->category }}</td>
          <td>
            <form id="delete-form-{{$product->id}}" action="{{ route('products.destroy',$product->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('products.show',$product->id) }}">Show</a>
              <a class="btn btn-primary" href="{{ route('products.edit',$product->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}
              <button type="button" onclick="ask_delete({{$product->id}})" class="btn btn-danger">Delete</button>
              

            </form>
          </td>
        </tr>
        @endforeach
        </tbody>
    </table>

    {{--  {{$products->links()}}  --}}




    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <script>
      $("#current-files").sortable({
        connectWith: "#selected-files",
        update: function(event, ui) { 
            var newIndex = ui.item.index();
            var obj = [];
            $('#current-files tr').each(function() {
                obj.push(this.id)
              })
              
              $.ajax({
                  url: "/update-product-order",
                  method: 'POST',
                  data:{
                    ids : obj,
                    _token: document.getElementsByName("_token")[0].value
                  },
                  success: function(res){
                      index = 0;
                      items = document.getElementsByClassName('index-td');
                      for(i = 0; i <= items.length; i++){
                          console.log(i)
                            $(items[i]).html(i+1) 
                      }
                  }
              });
        },
        
        start: function(event, ui) { 
            console.log('start: ' + ui.item.index())
        }
    });
    $("#selected-files").sortable();

    
    </script>

    @endsection
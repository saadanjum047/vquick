<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>VQuick | Dashboard</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">

    <style>
        .draggable-rows{
			cursor: move;
		}

    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    @include('/header');
    @include('/aside');
    <div class="content-wrapper">
        <section class="content-header">
            <h1 style="float:left;">
                Products
            </h1>
        </section>
        <div style="padding:5%;">
            @yield('content')
        </div>
    </div>
    <footer class="main-footer">
        <strong>Copyright &copy; 2020 <a href="#">vQuick</a>.</strong> All rights reserved.
    </footer>
</div>
<script src="{{asset('js/app.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script>
    function ask_delete(id){
        
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.value) {
                $('#delete-form-'+id).submit();
            }
          });   
    }

</script>

<script>

    function open_images(){
        $('#gallery-photo-add').click()
    }
    function open_video(){
        $('#video').click()
    }




      


    $(function() {
        // Multiple images preview in browser
        var imagesPreview = function(input, placeToInsertImagePreview) {
    
            if (input.files) {
                var filesAmount = input.files.length;
    
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
    
                    reader.onload = function(event) {
                        $('#image-preview').append('<img src="'+event.target.result+'"  class="m-2 image-preview-tag"> ')
                        //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }
    
                    reader.readAsDataURL(input.files[i]);
                }
            }
    
        };
        
       
        $('#gallery-photo-add').on('change', function() {
            imagesPreview(this, 'div.gallery');
        });
        
       
    });


    document.querySelector("#video")
        .onchange = function(event) {
            var files = event.target.files;
            for (var i = 0; i < files.length; i++) {
            var f = files[i];
            // Only process video files.
            if (!f.type.match('video.*')) {
                continue;
            }

            var source = document.createElement('video'); //added now

            $(source).addClass('m-2');

            source.width = 250;

            source.height = 150;

            source.controls = true;

            source.src = URL.createObjectURL(files[i]);

            //document.body.appendChild(source); // append `<video>` element
            $('#video_area').append(source); // append `<video>` element

            }
        }

        function remove_image(index){
            //console.log(index);
            //$('#deleted_images').val(index);
            $('#image-'+index).remove();
            $('#deleted_images').append('<input type="hidden" name="deleted_images[]" value="'+index+'" />')
            
        }

</script>


</body>
</html>
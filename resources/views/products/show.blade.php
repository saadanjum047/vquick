@extends('products.layout')

@section('content')

<style>
    .image-container{
        display: inline;
        position: relative;
    }
    .image-container .btn{
        position: absolute ;
        top: -70px;
        right: 0;
        border-radius: 50%;
        background-color: white;
        color: black;
        padding: 4px 10px !important
    }
    .video-container {
        position: relative;
        display: inline;
  
    }
     .video-container .btn{
        position: absolute ;
        top: -150;
        right: 0;
        border-radius: 50%;
        background-color: #c8c8c8;
  
    }
    
    .image-container .btn:hover{
        background-color: #c8c8c8;
        color: #fff;
  
    }
    
    .video-container .btn:hover{
        background-color: #c8c8c8;
        color: #fff;
  
    }
  
    .image-preview-tag{
      width: 130px;
      height: 130px;
      border-radius: 4%;
  
    }
  </style>
  
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Products</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $product->name }}<br>
                <strong>Arabic Name:</strong>
                {{ $product->arabic_name }}<br>
                <strong>Description:</strong>
                {{ $product->description }}<br>
                <strong>Arabic Description:</strong>
                {{ $product->description_ar }}<br>
                <strong>Price:</strong>
                {{ $product->price }} QTR<br>
                <strong>Available Stock:</strong>
                {{ $product->stock_available }}<br>
                <strong>Category:</strong>
                {{ $product->category }}<br>
                <strong>Images:</strong>
                <?php $path= '/files/'. $product->image; ?>
                {{-- <img style="width:100px;height: 100px;" src="<?= $path ?>" /><br> --}}

                <div class="col-xs-12 col-sm-12 col-md-12" >
                    <input type="file" name="images[]" style="display: none" multiple id="gallery-photo-add">
                    <div class="gallery" id="image-preview" >
                      @if( $product->images && is_array(json_decode($product->images)))
                      @foreach(json_decode($product->images) as $k => $image )
                      <div class="image-container" id="image-{{$k}}" >
                      <img src="{{asset('files/'.$image)}} "  class="m-2 image-preview-tag" > 
                      </div>
                      @endforeach
                      @endif
                    </div>
              </div>

            </div>
        </div>
    </div>
@endsection
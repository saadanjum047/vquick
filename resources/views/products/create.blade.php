@extends('products.layout')
@section('content')


<style>
  input[type="file"] {
      display: block;
    }
    .imageThumb {
      max-height: 75px;
      border: 2px solid;
      padding: 1px;
      cursor: pointer;
    }
    .pip {
      display: inline-block;
      margin: 10px 10px 0 0;
    }
    .remove {
      display: block;
      background: #444;
      border: 1px solid black;
      color: white;
      text-align: center;
      cursor: pointer;
    }
    .remove:hover {
      background: white;
      color: black;
    } 
    
    .image-preview-tag{
      width: 130px;
      height: 130px;
      border-radius: 4%;
  
    }
</style>


  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Add New Products</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>
      </div>
    </div>
  </div>
      
  @if ($errors->any())
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  

  <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    
    <div class="row" style="width:80%;margin:auto;">   

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Name:</strong>
          <input type="text" class="form-control" placeholder="Enter Product Name" name="name" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>ArabicName:</strong>
          <input type="text" class="form-control" placeholder="Enter Product Name in Arabic" name="arabic_name" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>English Description:</strong>
          <textarea class="form-control" name="description" placeholder="Enter English Description" ></textarea>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Arabic Description:</strong>
          <textarea  class="form-control" name="description_ar" placeholder="Enter Arabic Description" ></textarea>
        </div>
      </div>
      {{--  <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Image:</strong>
          <input type="file" name="image" style="width:100%;" class="btn btn-success" required="required">
        </div>
      </div>  --}}
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Price:</strong>
          <input type="text" class="form-control" name="price" placeholder="Enter Price" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Stock Available:</strong>
          <input type="text" class="form-control" name="stock_available" placeholder="Enter Stock Available" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Category:</strong>
          <select class="form-control" name="category">
            <option selected disabled>Select Category</option>
            <?php use App\categories;
              $categories = categories::all(); ?>
              @foreach ($categories as $category)
                <option value="{{ $category->name}}">{{ $category->name}}</option>
              @endforeach
          </select>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Enable / Disable:</strong>
          <select class="form-control" name="is_approved">
            <option value="1">Enable</option>
            <option value="0">Disable</option>
          </select>
        </div>
      </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
          <label for="inputEmail1" class="row col-form-label ">Image Gallery:</label>
          <h2><button style="font-size: 20px" class="btn btn-success" type="button" onclick="open_images()" > <i class="fa fa-image"></i> </button></h2>
      </div>

          <div class="col-xs-12 col-sm-12 col-md-12" >
              <input type="file" name="images[]" style="display: none" multiple id="gallery-photo-add">
              <div class="gallery" id="image-preview" >
              </div>
        </div>


      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>    
  </form>






  
  @endsection
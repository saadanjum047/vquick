@extends('aboutuses.layout')
@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Add New About Us</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('aboutuses.index') }}"> Back</a>
      </div>
    </div>
  </div>
      
  @if ($errors->any())
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif


  <form action="{{ route('aboutuses.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    
    <div class="row" style="width:80%;margin:auto;">   

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Title:</strong>
          <input type="text" class="form-control" name="title" placeholder="Enter Title" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
        <strong>Description:</strong>
        <textarea class="form-control" name="description" placeholder="Enter English Description" ></textarea>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Arabic Title:</strong>
          <input type="text" class="form-control" name="title_ar" placeholder="Enter Arabic Title" value=""  />
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Arabic Description:</strong>
          <textarea class="form-control" placeholder="Enter Arabic Description" name="description_ar"></textarea>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12" >
        <strong>Image:</strong>
        <input type="file" name="image" style="width:100%;" class="btn btn-success">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>    
  </form>
  @endsection
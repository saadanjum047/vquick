@extends('aboutuses.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show About Us</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('aboutuses.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $aboutus->title }}<br>
                <strong>Description:</strong>
                {{ $aboutus->description }}<br>
                <strong>Arabic Name:</strong>
                {{ $aboutus->title_ar }}<br>
                <strong>Arabic Description:</strong>
                {{ $aboutus->description_ar }}<br>
                <hr>
                <strong>Image:</strong>
                <?php $path= '/files/'. $aboutus->image; ?>
                <img style="width:200px;height: 200px;" src="<?= $path ?>" />
            </div>
        </div>
    </div>
@endsection
@extends('aboutuses.layout')
   
@section('content')

<style>
    .textarea-line{
        white-space: pre-wrap;
        {{--  white-space: pre-line;  --}}
    }

</style>

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit About Information</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('aboutuses.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('aboutuses.update',$aboutus->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
   
         <div class="row" style="width:80%;margin:auto;">
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Name:</strong>
                <input type="text" class="form-control" name="title" value="{{ $aboutus->title }}"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Description:</strong>
                <textarea class="form-control textarea-line" name="description" required="required">{{ $aboutus->description }}</textarea>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <strong>Arabic Title:</strong>
                  <input type="text" class="form-control" name="title_ar" placeholder="Enter Arabic Title" value="{{ $aboutus->title_ar }}"  />
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <strong>Arabic Description:</strong>
                  <textarea class="form-control textarea-line" placeholder="Enter Arabic Description" name="description_ar">{{ $aboutus->description_ar }}</textarea>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12" >
                <strong>Image:</strong>
                <input type="file" name="image" style="width:100%;" class="btn btn-success">
              </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection
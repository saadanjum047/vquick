<!DOCTYPE>
<html >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>

    {{--  <meta http-equiv="Content-Type" content="charset=utf-8"/>  --}}
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    
    {{--  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>  --}}

    {{--  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;  --}}
    <style>

        

        {{--  html,body { font-family: arabic, serif; }  --}}
        
        .arabic-fort{ font-family: DejaVu Sans; }


        #customers {
          border-collapse: collapse;
          width: 100%;
          margin-top: 70px;
        }
        
        #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
        }
        
        #customers tr:nth-child(even){background-color: #f2f2f2;}
        
        #customers tr:hover {background-color: #ddd;}
        
        #customers th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #627976;
          color: white;
        }
        .header{
            background-color: black;
            color: #fff;
            width: 100%;
            border-radius: 3px;
            text-align: center;
            margin-bottom: 40px;
        }

        .invoice-info{
            position: absolute;
            float: right;
            right: 60px;
        }
        .text-center{
            
        }
        </style>
        
</head>
<body>

    <div class="header">
        <span style="font-size: 27px" >Invoice</span>
    </div>
    <div style="text-align: center;">
        <a target="_blank" href="{{url('/')}} "><img src="assets/images/logo1.jpeg"  alt width="105"></a>
    </div>


    <div class="invoice-info">
        <div> <strong>Order # </strong>: {{$order->id}} </div>
        <div>  <strong> Date </strong>: {{Carbon\Carbon::now()->toDateString()}} </div>
        <div>  <strong>Total Amount</strong>: QTR {{$order->carts->count() > 0 ? array_sum($order->carts->pluck('original_price')->toArray())  : '' }} </div>
    </div>
 
    <div class="customer-info">
        <div> <strong>Name</strong>: <span class="arabic-fort"> {{ $order->customer->full_name}} </span> </div>
        <div>  <strong>Email</strong>: {{$order->customer->email}} </div>
        <div>  <strong>Phone</strong>: {{$order->customer->phone}} </div>
        <div>  <strong>Address</strong>: {{$order->customer->address}} </div>
    </div>



    <table id="customers">
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Image</th>
            <th>Quantity</th>
            <th>Price</th>
        </tr>
        @if(isset($order))
        @foreach($order->carts as $cart)
        <tr>
            <td> {{ $cart->name }}</td>
            <td> {{ $cart->description }}</td>
            <td style="text-align: center" >  <img src="{{asset('files/'.$cart->image)}}" style="max-height: 180px; max-width:120px; width:auto;border-radius: 3px;" >  </td>
            <td> {{ $cart->quantity }}</td>
            <td> QTR {{ $cart->original_price }}</td>
  
        </tr>
        @endforeach

        <tr>
            <td colspan="4" style="text-align: right" > Subtotal </td>
            <td >QTR: {{$order->carts->count() > 0 ? array_sum($order->carts->pluck('original_price')->toArray())  : '' }} </td>
        </tr>
        @if($order->discount_availed == 1)
        <tr>
            <td colspan="4" style="text-align: right" > Discount </td>
            <td >QTR: {{$order->carts->first()->discount}}% </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: right" > Subtotal </td>
            <td >QTR: {{$order->total_price}}% </td>
        </tr>
        @endif
        @if($order->area)
        <tr>
            <td colspan="4" style="text-align: right" > Shipping Chages </td>
            <td >QTR: {{$order->shipping_charges}} </td>
        </tr>
        @endif
        <tr>
            <td colspan="4" style="text-align: right" > Total </td>
            <td >QTR: {{$order->total_price + $order->shipping_charges}} </td>
        </tr>
        @endif
      
      </table>
      
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>


    <style>

        body{
            width: 100%;
            {{--  align-items: center;  --}}
                
        }
          
          
        .header{
            background-color: #e7e4da;
            width: 100%;
            height: 125px;
            text-align: center;
            padding:5px;
        }

        .order-area{
            background-color: #f8f8f8;
            width: 100%;
            min-height: 100px;
            margin-top: -20px;
            padding: 5px;
        }

        .order-details{
            background-color: #eeeeee;
            width: 100%;
            min-height: 100px;
            margin-top: -20px;
            padding: 5px;
        }

        .shipping-area{
            background-color: #eeeeee;
            width: 100%;
            min-height: 50px;
        }
        .text-center{
            text-align: center;
        }

    
        .wrap {
            display: flex;    
        }
        
        .first {
            width: 50%;
            {{--  height: 200px;  --}}
        }
        
        .second {
            width: 50%;
            margin-left: 50%;
            {{--  height: 200px;  --}}
        }

          .totla-area{
              font-size: 22px;
          }
        .social-media{
            text-align: center;
            padding: 5px;
            margin: 5px;
        }

        .wrapper {
            display: grid;
            grid-template-columns: repeat(2, 1fr);
            grid-gap: 10px;
            grid-auto-rows: minmax(100px, auto);
          }
        
          .one {
            grid-column: 1 / 2;
            {{--  background-color: #16f3d5;  --}}
            padding: 5px;
        }
        .two { 
            grid-column: 2 / 2;
            padding: 5px;
          }

          
    </style>

</head>
<body>

    <div class="text-center">
        <a target="_blank" href="{{url('/')}} "><img class="adapt-img" src="assets/images/logo.png"  alt width="105"></a>
    </div>

        
    <div class="header" >
        @if(isset($order))
        <h1>
            <strong>Your order is {{ucwords($order->status) }}. </strong></span>
        </h1>
        @endif
        <p>  {!! $msg !!} </p>

    </div>


    <div class="order-area">
        
        <h2 style="color: #191919; text-align:center;">Items ordered<br></h2>
        

        @if(isset($order))

        @foreach($order->carts as $cart)
        
            <div class="wrap" style="padding:5px; " >
                {{--  wrapper  --}}
                <div class="first text-center" style="place-self: center" >
                   <img src="{{asset('files/'.$cart->image)}}" style="max-height: 180px; max-width:120px; width:auto;  display: flex; justify-content: center; align-items: center;" >
                </div>
                
                <div class="second" >
                    <div> <b> {{$cart->name}} </b> </div>
                    <div> {{$cart->description}} </div>
                    <div> Item Price: QTR {{$cart->price}} </div>
                    <div> Qty: {{$cart->quantity}} </div>
                </div>
                
            </div>
            <hr>
            
            @endforeach
                <div class="text-center totla-area" >
                    
                    <div style="margin: 10px;" > <strong>Subtotal:</strong>  QTR {{  array_sum ($order->carts->pluck('original_price')->toArray()) }}  </div>

                    
                    @if($order->discount_availed == 1)
                    <div style="margin: 5px;" > <strong>Disocunt:</strong>  QTR {{ $order->carts->first()->discount }}%  </div>
                    @endif

                    <div style="margin: 10px;" > <strong>Total:</strong>  QTR {{ $order->total_price  }}  </div>
                </div>
    @endif
    <hr>
    </div>

        <div class="order-details">
            <div class="wrap" style="padding:5px;" >
            <div class="first" style="padding-left: 5px;" >
                <div> <h2> Order details </h2> </div>
                <div> Order # {{$order->id}} </div>
                <div> Shipping Method: Mandoob </div>
                <div> Order date: {{$order->created_at->toDateString()}} </div>

            </div>
            
            <div class="second " style="padding-left: 5px;" >
                <div> <h2> Our guarantee </h2> </div>
                <div> <p> Your satisfaction is 100% guaranteed. </p> </div>
             
            </div>

            
        </div>
        </div>

        <div class="social-media">

            <a href class="social-media-icons" style="margin : 5px"><img title="Twitter" src="https://stripo.email/cabinet/assets/editor/assets/img/social-icons/circle-gray/twitter-circle-gray.png" alt="Tw" width="32" height="32"></a>

            <a href class="social-media-icons" style="margin : 5px"><img title="Twitter" src="https://stripo.email/cabinet/assets/editor/assets/img/social-icons/circle-gray/facebook-circle-gray.png" alt="Tw" width="32" height="32"></a>

            <a href class="social-media-icons" style="margin : 5px"><img title="Twitter" src="https://stripo.email/cabinet/assets/editor/assets/img/social-icons/circle-gray/youtube-circle-gray.png" alt="Tw" width="32" height="32"></a>

            <a href class="social-media-icons" style="margin : 5px"><img title="Twitter" src="https://stripo.email/cabinet/assets/editor/assets/img/social-icons/circle-gray/linkedin-circle-gray.png" alt="Tw" width="32" height="32"></a>
            
            <br>
            @php
            $contact = App\contactuses::first();
            @endphp
            @if(isset($contact))

            <a target="_blank" style="line-height: 150%;" href="{{route('product')}}">Browse all products</a>
            
            <br>
            <a target="_blank" style="line-height: 120%;" href="tel:{{$contact->phone}}">{{$contact->phone}}</a>
            
            <br>
            <a target="_blank" href="mailto:{{$contact->email}}" style="line-height: 120%;">{{$contact->email}}</a>
            @endif
        </div>

        <hr>


        <div class="text-center">
            <a target="_blank" href="{{url('/')}} "><img class="adapt-img" src="assets/images/logo.png"  alt width="105"></a>
        </div>
    
</body>
</html>
@extends('contactuses.layout') 
@section('content')
<?php $inc=0; ?>
@foreach ($contactuses as $contactus)
<?php $inc++; ?>
@endforeach
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
              <?php if($inc==0) { ?>
                <a class="btn btn-success" href="{{ route('contactuses.create') }}"> Create New Contact</a>
              <?php } ?>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Phone Number</th>
          <th>Email</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($contactuses as $contactus)
        <tr>
          <td>{{ ++$i }}</td>
          <td>{{ $contactus->phone }}</td>
          <td>{{ $contactus->email }}</td>
          <td>
            <form id="delete-form-{{$contactus->id}}" action="{{ route('contactuses.destroy',$contactus->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('contactuses.show',$contactus->id) }}">Show</a>
              <a class="btn btn-primary" href="{{ route('contactuses.edit',$contactus->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}
            
              <button type="button" onclick="ask_delete({{$contactus->id}})" class="btn btn-danger">Delete</button>

            </form>
          </td>
        </tr>
        @endforeach
    </table>

    @if($contactuses->links())
    
    {{$contactuses->links()}}
    
    @endif
  
      
@endsection
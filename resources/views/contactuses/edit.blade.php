@extends('contactuses.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Contact</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('contactuses.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('contactuses.update',$contactus->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row" style="width:80%;margin:auto;">
            
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Phone Number:</strong>
                <input type="text" class="form-control" name="phone" value="{{ $contactus->phone }}"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Email:</strong>
                <input type="text" class="form-control" value="{{ $contactus->email }}" name="email" required="required"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection
@extends('homepages.layout') 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('homepages.create') }}"> Create New Home Page Content</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Title</th>
          <th>Description</th>
          <th>Image</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($homepages as $homepage)
        <tr>
          <td>{{ ++$i }}</td>
          <td>{{ $homepage->title }}</td>
          <td>{{ $homepage->description }}</td>
          <td>{{ $homepage->image }}</td>
          <td>
            <form action="{{ route('homepages.destroy',$homepage->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('homepages.show',$homepage->id) }}">Show</a>
              <a class="btn btn-primary" href="{{ route('homepages.edit',$homepage->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
    </table>
  
      
@endsection
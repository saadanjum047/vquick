



@extends('layouts.main')




@section('title')
<title>Edit Profile</title>
@endsection

@section('styles')
    <style>
      .progress-bar{
        background-color: #627976 !important;
      }

      .form-margin{
        margin-left: 11%; 
        margin-right: 21%;
      }

      .area-select{
        padding-left: 8px !important;
      }


      @media screen and (max-width : 715px){

        .form-margin{
          margin-left: 0; 
          margin-right: 0;
        }
      }
    </style>
@endsection

@section('content')

<!-- signup web view -->
<div class="container websi mt-5 mb-5" style="width:78%">
  <div class="row no-gutters " >
    <div class="col-lg-6 sigdi1" >
      <img src="assets/images/contact.PNG" class="sigdi1 w-100" alt="">
    </div>
    
    <div class="col-lg-6 px-5  sigdi2" style="padding: 5%;background-color: #F0E1DD;margin-left:-2px;">
    
      

      
<div class="d-flex align-items-center form-margin">
  <span class="stars">
    <span><i class="fa fa-star" style="@if($user->has_discount == 1 ) color:red @else color:#666666 @endif" ></i></span>
  </span>
  <div class="col text-center pr-0">
    <div class="progress">

      <div class="progress-bar" role="progressbar" style="width:{{$progress}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
        @if($user->has_discount == 1)
        {{__("Discount Availed")}}
        @else
         {{$user->new_orders->count().' / '.$discount->orders}} 
        {{-- {{ceil($progress)}}% --}}
        @endif
      </div>
    </div>
  </div>
</div>

{{--     
      <div class="progress form-margin inline" >
          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
          aria-valuemin="0" aria-valuemax="100" style="width:{{$progress}}%">
            @if($user->has_discount == 1)
            {{__("Discount Availed")}}
            @else
             {{$user->new_orders->count().' / '.$discount->orders}} 
            @endif

          </div>
        </div>  --}}
        <hr class="form-margin" >



        <form action="{{ route('regusers.update' , $user->id) }}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <div style="margin-left: 46px; margin-right: 83px;">
          <div class="form-group">
          <input type="text" class="form-control text-left inpf input_reg"  name="full_name" id="full_name" placeholder="{{__('Full Name')}}" value="{{$user->full_name}}" required="required">
          </div>
        
          <div class="form-group mt-5">
          <input type="text" class="form-control text-left inpf input_reg" name="phone" id="mobile_no" placeholder="{{__('Mobile number')}}" value="{{$user->phone}}" required="required">
          </div>
          <div class="form-group">
          <input type="email" class="form-control text-left inpf input_reg" name="email" id="email_login" placeholder="{{__('Email')}}" value="{{$user->email}}" required="required">
          </div>
          <div class="form-group">
          <input type="text" class="form-control text-left inpf input_reg"  name="address" id="address" placeholder="{{__('Address')}}" value="{{$user->address}}" required="required">
          </div>

          <div class="form-group">
            <select type="text" class="form-control text-left inpf input_reg area-select"  name="area" id="area" required="required"  >
              <option value="">Select Area</option>
              @foreach($areas as $area)
              <option value="{{$area->id}}"  @if($area->id == $user->area_id) selected @endif  >{{$area->proper_area}}</option>
              @endforeach
            </select>
          </div>

          <!--<div class="form-group mt-5">-->
          <!--<input type="text" class="form-control text-left inpf input_reg" name="zone" id="zone" placeholder="{{__('Zone Number')}}" value="{{$user->zone}}" >-->
          <!--</div>-->
          <!--<div class="form-group">-->
          <!--<input type="text" class="form-control text-left inpf input_reg" name="street" id="street" placeholder="{{__('Street Number')}}" value="{{$user->street}}" >-->
          <!--</div>-->
          <!--<div class="form-group">-->
          <!--<input type="text" class="form-control text-left inpf input_reg" name="building" id="building" placeholder="{{__('Building Number')}}" value="{{$user->building}}" >-->
          <!--</div>-->

          <div class="form-group mt-5">
          <input type="password" class="form-control text-left inpf input_reg" name="password" id="password_login" placeholder="{{__('Password')}}"  >
          </div>
          <div class="form-group mt-5">
          <input type="password" class="form-control text-left inpf input_reg" id="re_password_login" name="conf_password" placeholder="{{__('Re-enter Password')}}" >
          </div>
          <input type="hidden" value="1" name="page">
        </div>
        <div class="row mt-5 button-signup-login">
          <div class="offset-lg-3 col-lg-6 mgr">
          <button type="submit" class="logbt btn-block py-3 button-signup-login-buttons uncfocused-item" >{{__('Update')}}</button>
          </div>
          
          <div class="col-lg-6 mgl">
          </div>
        </div>
        </form>
      </div>
    </div>
</div>
  <!-- signup web view -->

  <!-- signup mobile view -->
  <section class="signview">

 

  <div class="container mblsig" id="mblsig">
    <div class="row">
      <div class="col-lg-12 login-mobile">
        <h3 class="text-center  font-weight-bold pb-3" >{{__('Edit Profile')}}</h3>

        @if($errors->any())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
        @endforeach
        @endif

       
        {{--  @dd($user->new_orders , $user)  --}}
      
<div class="d-flex align-items-center form-margin">
  <span class="stars">
    <span><i class="fa fa-star" style="@if($user->has_discount == 1 ) color:red @else color:#666666 @endif" ></i></span>
  </span>
  <div class="col text-center pr-0">
    <div class="progress">

      <div class="progress-bar" role="progressbar" style="width:{{$progress}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
        @if($user->has_discount == 1)
        {{__("Discount Availed")}}
        @else
         {{$user->new_orders->count().' / '.$discount->orders}} 
        {{-- {{ceil($progress)}}% --}}
        @endif
      </div>
    </div>
  </div>
</div>
        <hr>




        <form action="{{ route('regusers.update' , $user->id) }}"  method="POST" >
          @csrf
          @method('PATCH')
          <div class="form-group">
            <input  type="text" class="form-control inpf2"  value="{{$user->full_name}}"  name="full_name" placeholder="{{__('Full Name')}}" required>
          </div>
          <div class="form-group">
            <input  type="text" class="form-control  inpf2" value="{{$user->phone}}"  name="phone"  placeholder="{{__('Mobile number')}}" required>
          </div>
          <div class="form-group">
            <input  type="text" class="form-control  inpf2" value="{{$user->address}}"  name="address"   placeholder="{{__('Address')}}" required>
          </div>

          <div class="form-group">
            <select type="text" class="form-control  inpf2 area-select"  name="area" id="area" placeholder="{{__('Area')}}" required="required"  >
              <option value="">Select Area</option>
              @foreach($areas as $area)
              <option value="{{$area->id}}"  @if($area->id == $user->area_id) selected @endif > {{$area->proper_area}}</option>
              @endforeach
            </select>
          </div>


          <!--<div class="form-group">-->
          <!--  <input  type="text" class="form-control  inpf2" value="{{$user->zone}}"  name="zone"  placeholder="{{__('Zone Number')}}" required>-->
          <!--</div>-->
          
          <!--<div class="form-group">-->
          <!--  <input  type="text" class="form-control  inpf2" value="{{$user->street}}"  name="street"   placeholder="{{__('Street Number')}}" >-->
          <!--</div>-->
          <!--<div class="form-group">-->
          <!--  <input  type="text" class="form-control  inpf2" value="{{$user->building}}" name="building"    placeholder="{{__('Building Number')}}" >-->
          <!--</div>-->
          <div class="form-group">
            <input  type="email" class="form-control  inpf2"  value="{{$user->email}}" name="email"  placeholder="{{__('Email address')}}" >
          </div>
          <div class="form-group">
            <input  type="password" class="form-control  inpf2" name="password"   placeholder="{{__('Password')}}" >
          </div>
          <div class="form-group">
            <input  type="password" class="form-control  inpf2" name="conf_password"  placeholder="{{__('Re-enter password')}}">
          </div>

          <input type="hidden" value="1" name="page">

		
          {{--  <input class="text-right mt-3" id="terms_and_conditions" style="margin-left: 0px;"type="checkbox"/><lable style="margin-left: 8px;" for="terms_and_conditions" >I accept the terms and conditions</lable>  --}}
		
          <button type="submit" class="btn-block uncfocused-item" style="margin-top:1rem;" >{{__('Update')}}</button>

          </form>
		
		</div>
    </div>
  </div>
 <!-- <hr style="    border-top: 1px solid rgba(0,0,0,0.3) !important;    margin-top: 0;"> -->

</section>



@endsection

@extends('shippings.layout') 
@section('content')
<?php $inc=0; ?>
@foreach ($shippings as $shipping)
<?php $inc++; ?>
@endforeach
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
              <?php if($inc==0) { ?>
                <a class="btn btn-success" href="{{ route('shippings.create') }}"> Create New shipping</a>
              <?php } ?>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Shipping Fee</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($shippings as $shipping)
        <tr>
          <td>{{ ++$i }}</td>
          <td>{{ $shipping->fee }}</td>
          <td>
            <form id="delete-form-{{$shipping->id}}" action="{{ route('shippings.destroy',$shipping->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('shippings.show',$shipping->id) }}">Show</a>
              <a class="btn btn-primary" href="{{ route('shippings.edit',$shipping->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}
              
              <button type="button" onclick="ask_delete({{$shipping->id}})" class="btn btn-danger">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
    </table>

    @if($shippings->links())
    
    {{$shippings->links()}}
    
    @endif
  
      
@endsection
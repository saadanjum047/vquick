@extends('admins.layout') 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('admins.create') }}"> Create New Admin</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bregusered">
        <tr>
          <th>No</th>
          <th>Email</th>
          <th>Name</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($admins as $admin)
        <tr>
          <td>{{ ++$i }}</td>
          <td>{{ $admin->email }}</td>
          <td>{{ $admin->name }}</td>
          <td>
            <form id="delete-form-{{$admin->id}}" action="{{ route('admins.destroy',$admin->id) }}" method="POST">
              {{--  <a class="btn btn-info" href="{{ route('admins.show',$admin->id) }}">Show</a>  --}}
              <a class="btn btn-primary" href="{{ route('admins.edit',$admin->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              <button type="button" onclick="ask_delete({{$admin->id}})" class="btn btn-danger">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
    </table>
  
      

@endsection
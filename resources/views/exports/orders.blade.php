<table>
    <thead>
    <tr>
        <th style="width: 20px; text-align:center;" >Order Number</th>
        <th style="width: 20px; text-align:center;" >Customer Name</th>
        <th style="width: 20px; text-align:center;" >Status</th>
        <th style="width: 20px; text-align:center;" >Shipping Area</th>
        <th style="width: 20px; text-align:center;" >Shipping Charges</th>
        <th style="width: 20px; text-align:center;" >Price</th>
        <th style="width: 20px; text-align:center;" >Discount</th>
        <th style="width: 20px; text-align:center;" >Discounted Price</th>
        <th style="width: 20px; text-align:center;" >Payment Status</th>
        <th style="width: 20px; text-align:center;" >Payment Method</th>
        <th style="width: 20px; text-align:center;" >Transaction Number</th>
        <th style="width: 20px; text-align:center;" >Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
    @if(!($order->payment_method == 'sadad' && $order->payment_status == 'pending'))

        <tr>
            <td style="width: 20px; text-align:center;">{{ $order->id }}</td>
            <td style="width: 20px; text-align:center;">{{ $order->customer->full_name ?? '' }}</td>
            <td style="width: 20px; text-align:center;">{{ ucwords($order->status) }}</td>
            <td style="width: 20px; text-align:center;" >{{ $order->area ? $order->area->area : '' }}</td>
            <td style="width: 20px; text-align:center;" >{{ $order->shipping_charges }}</td>
            <td style="width: 20px; text-align:center;">{{ array_sum($order->carts->pluck('original_price')->toArray()) }}</td>
            <td style="width: 20px; text-align:center;"> @if($order->carts->first() && $order->carts->first()->discount) {{$order->carts->first()->discount}} % @endif  </td>
            <td style="width: 20px; text-align:center;" >{{ $order->total_price }}</td>
            <td style="width: 20px; text-align:center;" >{{ ucwords($order->payment_status) }}</td>
            <td style="width: 20px; text-align:center;" >{{ ucwords($order->payment_method) }}</td>
            <td style="width: 20px; text-align:center;" >{{ $order->transaction_number }}</td>
            <td style="width: 20px; text-align:center;" >{{ $order->created_at->toDateString() }}</td>
        </tr>
    @endif
    @endforeach
    </tbody>
</table>
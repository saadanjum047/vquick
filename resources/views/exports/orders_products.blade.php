
<table>
    <thead>
    <tr>
        <th style="width: 20px;" >#</th>
        <th style="width: 20px;" >Product Name</th>
        <th style="width: 20px;" >Sold Quantity</th>
        <th style="width: 20px;" >Price</th>
    </tr>
    </thead>
    <tbody>
        @foreach($products as $k => $product)
        <tr>
            <td>{{ $k + 1 }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->total_quantity }}</td>
            <td>{{ $product->total_price }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
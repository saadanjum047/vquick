<table>
    <thead>
        
    <tr>
        <th style="width: 20px; text-align:center;" >Order Number</th>
        <th style="width: 20px; text-align:center;" >Customer Name</th>
        <th style="width: 20px; text-align:center;" >Status</th>
        <th style="width: 20px; text-align:center;" >Price</th>
        <th style="width: 20px; text-align:center;" >Shipping Charges</th>
        <th style="width: 20px; text-align:center;" >Payment Status</th>
        <th style="width: 20px; text-align:center;" >Date</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td style="width: 20px; text-align:center;">{{ $order->id }}</td>
            <td style="width: 20px; text-align:center;">{{ $order->customer->full_name }}</td>
            <td style="width: 20px; text-align:center;">{{ ucwords($order->status) }}</td>
            <td style="width: 20px; text-align:center;">{{ $order->total_price }}</td>
            <td style="width: 20px; text-align:center;">{{ $order->shipping_charges }}</td>
            <td style="width: 20px; text-align:center;">{{ ucwords($order->payment_method) }}</td>
            <td style="width: 20px; text-align:center;">{{ $order->created_at->toDateString() }}</td>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        
    <tr>
        <th style="width: 20px; text-align:center;" >#</th>
        <th style="width: 20px; text-align:center;" >Product Name</th>
        <th style="width: 20px; text-align:center;" >Description</th>
        <th style="width: 20px; text-align:center;" >Quantity</th>
        <th style="width: 20px; text-align:center;" >Stock Available</th>
        <th style="width: 20px; text-align:center;" >Price</th>
    </tr>
    </thead>
    <tbody>
        @foreach($order->carts as $k => $cart)
        <tr>
            <td style="width: 20px; text-align:center;" >{{ $k + 1 }}</td>
            <td style="width: 20px; text-align:center;" >{{ $cart->name }}</td>
            <td style="width: 20px; text-align:center;" >{{ $cart->description }}</td>
            <td style="width: 20px; text-align:center;" >{{ $cart->quantity }}</td>
            <td style="width: 20px; text-align:center;" >{{ $cart->product->stock_available }}</td>
            <td style="width: 20px; text-align:center;" >{{ $cart->price }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
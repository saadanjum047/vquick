<table>
    <thead>
    <tr>
        <th style="width: 20px;" >#</th>
        <th style="width: 20px;" >Name</th>
        <th style="width: 20px;" >Email</th>
        <th style="width: 20px;" >Mobile Number</th>
        <th style="width: 20px;" >Address</th>
        <th style="width: 20px;" >Discount</th>
        <th style="width: 20px;" >Discount End Date</th>
  </tr>
    </thead>
    <tbody>
    @foreach($customers as $k => $customer)
        <tr>
            <td>{{ $k+1 }}</td>
            <td>{{ $customer->full_name }}</td>
            <td> {{ $customer->email }} </td>
            <td>{{ $customer->phone }}</td>
            <td>{{ $customer->address }}</td>
            <td> @if($customer->has_discount == 1) YES @else No @endif </td>
            <td> @if($customer->has_discount == 1) {{$customer->discount_end_date}}  @endif </td>
          </tr>
    @endforeach
    </tbody>
</table>
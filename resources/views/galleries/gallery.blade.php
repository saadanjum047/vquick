@extends('galleries.layout')

@section('styles')

<link rel="stylesheet" href="{{asset('assets/dropzone/dist/dropzone.css')}}" >
    
<link href="{{asset('assets/lightbox/dist/css/lightbox.css')}}" rel="stylesheet" />

<style>
    .gallery{
        display: inline !important;
    }
    .lightbox-image-list
    {
        max-width:170px;
        max-height:170px;
        width:auto;
        height:auto; 
        border-radius:8px; 
        margin:5px;
    }
    .lightbox-image-anchor
    {
        max-width:80px; 
        max-height:80px;
        width:auto;
        height:auto;
    }
    .image-container {
        position: relative;
        width: 100%;
    }
    
    .image-container img {
        width: 100%;
        height: auto;
    }
    
    .image-container .btn {
        position: absolute;
        top: 0;
        right: 1%;

        transform: translate(-1%, -230%);
        -ms-transform: translate(-5%, -5%);
        color: white;
        font-size: 16px;
        border: none;
        cursor: pointer;
        border-radius: 50px;
        text-align: center;
        color:black;
        background-color: Transparent;
        background-repeat:no-repeat;
    
        
    }
    
    .image-container .btn:hover {
        background-color: #555;
        color:white;

    }

</style>
@endsection

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Add New Gallery Image</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('galleries.index') }}"> Back</a>
      </div>
    </div>
  </div>
      
  @if ($errors->any())
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif


  {{--  @dd($galleries)  --}}


    <div class="row">

      <form action="{{route('galleries.store')}} " method="POST" enctype="multipart/form-data" >
        @csrf
    <div class="col-md-8">
        <div class="form-group">
          <button class="btn btn-sm btn-success w-100" type="button" onclick=" $('#image-picker').click() " > <i class="fa fa-image"></i> Upload</button>
          <input type="file"  name="images[]" multiple class="d-none" id="image-picker" />
        </div> 
    </div>
    <div class="col-md-4">
        
        <div class="form-group">
          <button type="submit" class="btn btn-sm btn-primary">Save</button>
        </div>
        </div>
  </form>
    
    <div class="col-md-12">
        
        <hr>
        <div class=""id="company-gallery">
        @if($galleries->count() > 0 )
        @foreach($galleries as $image)

        <div class="image-container gallery" id="{{$image->id}}">
            <a href="{{asset('files/'.$image->image)}}" data-lightbox="roadtrip"  class="lightbox-image-anchor">
                <img src="{{asset('files/'.$image->image)}}" alt=""/ class="lightbox-image-list" />
                </a>
                <button class="btn" onclick="remove_image('{{$image->id}}')" >X</button>
            </div>
            @endforeach
            @endif
            
            <p id="gallery-info" style="@if($galleries->count() > 0 )  display: none @endif" class="alert alert-info">No Photos found! Upload Now</p>
        </div>
        <hr>

        {{-- <form method="post" action="{{url('images-save')}}" enctype="multipart/form-data" 
        class="dropzone" id="dropzone" style="width:auto;height:auto; margin-left:10px; ">
        @csrf
        </form>    --}}

    </div>
</div>



    
  @endsection


  @section('scripts')
  <script src="{{asset('assets/dropzone/dist/dropzone.js')}}"></script>
  <script src="{{asset('assets/lightbox/dist/js/lightbox.js')}}"></script>
  
  <script>
  
      Dropzone.options.dropzone =
       {
          maxFilesize: 12,
          renameFile: function(file) {
              var dt = new Date();
              var time = dt.getTime();
             return time+file.name;
          },
          acceptedFiles: ".jpeg,.jpg,.png,",
          //addRemoveLinks: true,
          timeout: 5000,
          removedfile: function(file) 
          {
              var name = file.upload.filename;
              $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                          },
                  type: 'POST',
                  url: '{{ url("images-save") }}',
                  data: {filename: name},
                  success: function (data){
  
                      console.log("File has been successfully removed!!");
                  },
                  error: function(e) {
                      console.log(e);
                  }});
                  var fileRef;
                  return (fileRef = file.previewElement) != null ? 
                  fileRef.parentNode.removeChild(file.previewElement) : void 0;
          },
          success: function(file, response) 
          {
  
            $('#company-gallery').append(' <div class="image-container gallery" id="'+response.id+'" ><a href="{{url('/')}}/files/'+response.image+'" data-lightbox="roadtrip" class="lightbox-image-anchor"> <img src="{{url('/')}}/files/'+response.image+'" class="lightbox-image-list"></a> <button class="btn" onclick="remove_image('+response.id+')">X</button></div> ');
            $('#gallery-info').hide();
          },
          error: function(file, response)
          {
             return false;
          }
  };
  
  
  
    function remove_image( id){
  
      $.ajax({
          url: '{{url("/")}}/images-delete/'+id,
          success : function(res){
  
              document.getElementById(id).remove();

              console.log($('.image-container').length)
                if($('.image-container').length == 0 ){
                    $("#gallery-info").show();
                }

              Swal.fire({
                  position: 'top-end',
                  icon: 'success',
                  title: 'Image removed successfully',
                  showConfirmButton: false,
                  timer: 1500
                });
  
  
              //$('#'+name).remove();
          }
      }).fail( function(res){
          Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: 'Some problem occoured! Plz try',
              showConfirmButton: false,
              timer: 1500
            });
      }); 
  }
  
  
  </script>
  
  @endsection
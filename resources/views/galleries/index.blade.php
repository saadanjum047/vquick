@extends('galleries.layout') 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('galleries.create') }}"> Create New Gallery Image</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Title</th>
          <th>Image</th>
          <th width="280px">Action</th>
        </tr>
        @foreach ($galleries as $gallery)
        <?php 
        
        $path= public_path().'/files/'.$gallery->image; ?>
        <tr>
          <td>{{ ++$i }}</td>
          <td>{{ $gallery->title }}</td>
          <td><img style="width:100px;height: 100px;" src="files/<?= $gallery->image ?>" /></td>
          <td>
            <form action="{{ route('galleries.destroy',$gallery->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('galleries.show',$gallery->id) }}">Show</a>
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
    </table>
  
      
@endsection
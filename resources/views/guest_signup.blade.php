



@extends('layouts.main')


@section('styles')
    <style>
      .area-select{
        padding-left: 8px !important;
      }

    </style>
@endsection


@section('title')
<title>Guest Sign Up</title>
@endsection


@section('content')
    

    <!-- signup web view -->
<div class="container websi mt-5 mb-5" style="width:78%">
  <div class="row no-gutters " >
    <div class="col-lg-6 sigdi1" >
      <img src="assets/images/contact.PNG" class="sigdi1 w-100" alt="">
    </div>
    
      <div class="col-lg-6 px-5  sigdi2" style="padding: 5%;background-color: #F0E1DD;margin-left:-2px;">
      <form action="{{ route('register-guest') }}" onsubmit="from_submitted()" class="submit-form" method="POST" enctype="multipart/form-data">
      @csrf
        <div style="margin-left: 46px; margin-right: 83px;">
          <div class="form-group">
          <input type="text" class="form-control text-left inpf input_reg"  name="full_name" id="full_name" placeholder="{{__('Full Name')}}" required="required">
          </div>
        
          <div class="form-group mt-5">
          <input type="number" class="form-control text-left inpf input_reg" name="phone" id="mobile_no" placeholder="{{__('Mobile number')}}" required="required">
          </div>
          <div class="form-group">
          <input type="email" class="form-control text-left inpf input_reg" name="email" id="email_login" placeholder="{{__('Email')}}" required="required">
          </div>
          <div class="form-group">
          <input type="text" class="form-control text-left inpf input_reg"  name="address" id="address" placeholder="{{__('Address')}}" required="required">
          </div>
          
          <div class="form-group">
            <select type="text" class="form-control text-left inpf input_reg area-select"  name="area" id="area" placeholder="{{__('Area')}}" required="required"  >
              <option value="">{{__('Select Area')}}</option>
              @foreach($areas as $area)
              <option value="{{$area->id}}" @if(isset($shipping_charge) && $shipping_charge == $area->id ) selected @endif >{{$area->proper_area}}</option>
              @endforeach
            </select>
          </div>


          <!--<div class="form-group mt-5">-->
          <!--<input type="text" class="form-control text-left inpf input_reg" name="zone" id="zone" placeholder="{{__('Zone Number')}}" >-->
          <!--</div>-->
          <!--<div class="form-group">-->
          <!--<input type="text" class="form-control text-left inpf input_reg" name="street" id="street" placeholder="{{__('Street Number')}}" >-->
          <!--</div>-->
          <!--<div class="form-group">-->
          <!--<input type="text" class="form-control text-left inpf input_reg" name="building" id="building" placeholder="{{__('Building Number')}}" >-->
          <!--</div>-->

          <input type="hidden" value="1" name="guest_signup">


          <input type="hidden" value="{{$total_price}}" name="total_price" >
          <input type="hidden" value="{{$payment_method}}" name="payment_method" >
          {{--  <input type="hidden" value="{{$shipping_charge}}" name="shipping_charge" >  --}}


          <input type="hidden" value="1" name="page">
        </div>
        <div class="row mt-5 button-signup-login">
          <div class="offset-lg-3 col-lg-6 mgr">
          <button type="submit" class="logbt btn-block py-3 button-signup-login-buttons uncfocused-item confirm-order-button " >{{__('Confirm Order')}}</button>
          </div>
          
          
        </div>
        </form>
      </div>
    </div>
</div>
  <!-- signup web view -->

  <!-- signup mobile view -->
  <section class="signview">

 

  <div class="container mblsig" id="mblsig">
    <div class="row">
      <div class="col-lg-12 login-mobile">
        <h3 class="text-center  font-weight-bold pb-3" >{{__('Guest Signup')}}</h3>

        @if($errors->any())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
        @endforeach
        @endif

        <form action="{{ route('register-guest') }}" onsubmit="from_submitted()"   class="submit-form"  method="POST" >
          @csrf
          <div class="form-group">
            <input  type="text" class="form-control inpf2"  name="full_name" placeholder="{{__('Full Name')}}" required>
          </div>
          <div class="form-group">
            <input  type="number" class="form-control  inpf2" name="phone"  placeholder="{{__('Mobile number')}}" required>
          </div>
          <div class="form-group">
            <input  type="text" class="form-control  inpf2" name="address"   placeholder="{{__('Address')}}" required>
          </div>

          <div class="form-group">
            <select type="text" class="form-control  inpf2 area-select"  name="area" id="area" placeholder="{{__('Area')}}" required="required"  >
              <option value="">{{__('Select Area')}}</option>
              @foreach($areas as $area)
              <option value="{{$area->id}}" @if(isset($shipping_charge) && $shipping_charge == $area->id ) selected @endif >{{$area->proper_area}}</option>
              @endforeach
            </select>
          </div>

          <!--<div class="form-group">-->
          <!--  <input  type="text" class="form-control  inpf2" name="zone"  placeholder="{{__('Zone Number')}}" >-->
          <!--</div>-->
          
          <!--<div class="form-group">-->
          <!--  <input  type="text" class="form-control  inpf2" name="street"   placeholder="{{__('Street Number')}}" >-->
          <!--</div>-->
          <!--<div class="form-group">-->
          <!--  <input  type="text" class="form-control  inpf2"name="building"    placeholder="{{__('Building Number')}}" >-->
          <!--</div>-->
          <div class="form-group">
            <input  type="email" class="form-control  inpf2" name="email"  placeholder="{{__('Email address')}}" required>
          </div>
      
      
          <input type="hidden" value="1" name="guest_signup">
      

          <input type="hidden" value="1" name="page">

          <input type="hidden" value="{{$total_price}}" name="total_price" >
          <input type="hidden" value="{{$payment_method}}" name="payment_method" >
          {{--  <input type="hidden" value="{{$shipping_charge}}" name="shipping_charge" >  --}}

		
          <input class="text-right mt-3" id="terms_and_conditions" style="margin-left: 0px;"type="checkbox"/><lable style="margin-left: 8px;" for="terms_and_conditions" >I accept the terms and conditions</lable>
		
          <button type="submit" class="btn-block uncfocused-item confirm-order-button" style="margin-top:1rem;" >{{__('Confirm Order')}}</button>

          </form>
		
		</div>
    </div>
  </div>
 <!-- <hr style="border-top: 1px solid rgba(0,0,0,0.3) !important;    margin-top: 0;"> -->

</section>

@endsection






@section('scripts')

<script>

  function from_submitted(){
    $('.confirm-order-button').prop('disabled' , true);
  }


//  $('.confirm-order-button').on( 'click' , function(){
  //  $('.confirm-order-button').prop('disabled' , true);
    //$('.submit-form').submit();
//  });
</script>
@endsection

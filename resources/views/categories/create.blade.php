@extends('categories.layout')
@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Add New categories</h2>
      </div>
      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('categories.index') }}"> Back</a>
      </div>
    </div>
  </div>
      
  @if ($errors->any())
  <div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h3>Create New Expense Type</h3>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="{{ url('/create-popup-expensetype') }}" class="ajax-submit modal-form" method="GET" update="expensetype">
            @csrf
            <div class="row" style="width:80%;margin:auto;">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <strong>Name:</strong>
                  <input type="text" class="form-control" placeholder="Name" name="name" required="required"/>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="supplier" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h3>Create New Expense Type</h3>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="{{ url('/create-popup-supplier') }}" class="ajax-submit modal-form" method="GET" update="supplier">
            @csrf
            <div class="row" style="width:80%;margin:auto;">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <strong>Name:</strong>
                  <input type="text" class="form-control" placeholder="Name" name="name" required="required"/>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    
    <div class="row" style="width:80%;margin:auto;">   

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Name:</strong>
          <input type="text" class="form-control" name="name" placeholder="Enter Category Name" required="required"/>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Arabic Name:</strong>
          <input type="text" class="form-control" name="arabic_name" placeholder="Enter Category Name in Arabic" required="required"/>
        </div>
      </div>
      
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>English Description:</strong>
          <textarea class="form-control" name="description" placeholder="Enter Category English" ></textarea>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Arabic Description:</strong>
          <textarea  class="form-control" name="description_ar" placeholder="Enter Category Arabic" ></textarea>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Restriction Level:</strong>
          <input type="text" class="form-control" placeholder="Restriction Level" name="restriction_level" />
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12">
        <strong>Image:</strong>
        <input type="file" name="image" style="width:100%;" class="btn btn-success" required="required">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="form-group">
          <strong>Enable / Disable:</strong>
          <select class="form-control" name="is_approved">
            <option value="1">Enable</option>
            <option value="0">Disable</option>
          </select>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>    
  </form>
  @endsection
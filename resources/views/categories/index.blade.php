@extends('categories.layout') 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('categories.create') }}"> Create New Categories</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Order</th>
          <th>Name</th>
          <th>Arabic Name</th>
          <th>Restriction Level</th>
          <th>Status</th>
          <th>Image</th>
          <th width="280px">Action</th>
        </tr>
        <tbody id="current-files" >
        @foreach ($categories->SortBy('cat_order') as $k => $category)

        <tr class="draggable-rows" id="{{$category->id}}" name="{{$k}}" >
          <td>{{ $k + 1 }}</td>
          <td class="index-td"> {{$category->cat_order + 1}} </td>
          <td>{{ $category->name }}</td>
          <td>{{ $category->arabic_name }}</td>
          <td>{{ $category->restriction_level }}</td>
          <td> @if( $category->is_approved == 1) 
              <label  class="label label-success" >Active</label>   
              @else
              <label  class="label label-danger" >Disabled</label>   
               @endif
          </td>
          <td><img style="width:100px;height: 100px;" src="files/<?= $category->image ?>" /></td>
          <td>
            <form id="delete-form-{{$category->id}}" action="{{ route('categories.destroy',$category->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('categories.show',$category->id) }}">Show</a>
              <a class="btn btn-primary" href="{{ route('categories.edit',$category->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}
              <button type="button" onclick="ask_delete({{$category->id}})" class="btn btn-danger">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  
    {{--  @if($categories->links())
    
    {{$categories->links()}}
    
    @endif  --}}
      
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

    <script>
      $("#current-files").sortable({
        connectWith: "#selected-files",
        update: function(event, ui) { 
            var newIndex = ui.item.index();
            var obj = [];
            $('#current-files tr').each(function() {
                obj.push(this.id)
              })
              
              $.ajax({
                  url: "/update-category-order",
                  method: 'POST',
                  data:{
                    ids : obj,
                    _token: document.getElementsByName("_token")[0].value
                  },
                  success: function(res){
                      index = 0;
                      items = document.getElementsByClassName('index-td');
                      for(i = 0; i <= items.length; i++){
                          console.log(i)
                            $(items[i]).html(i+1) 
                      }
                  }
              });
        },
        
        start: function(event, ui) { 
            console.log('start: ' + ui.item.index())
        }
    });
    $("#selected-files").sortable();

    
    </script>

@endsection
@extends('orderstatuses.layout') 
@section('content')


<style>
  .badge-danger{
    background-color: red;
    color:white;
  }
  .badge-success{

  }
  .badge-primary{

  }
  .badge-warning{

  }
</style>


    <?php use App\regusers;?>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
  


 <table class="table table-bordered">
        <tr>
          <th>Order Number</th>
          <th>Customer Name</th>
          <th>Payment Status</th>
          <th>Payment Method</th>
          <th>Transaction Number</th>
          <th>Status</th>
          <th>Mobile Number</th>
          <th>Address</th>
          <th>Shipping Area</th>
          <th>Shipping Charges</th>
          <th>Total Price</th>
          <th>Discounted Price</th>
          <th>Date Time</th>
          <th width="280px">Action</th>
        </tr>
        
        @foreach ($customer->orders->sortByDesc('id')  as $orderstatus)
        
        @if(!($orderstatus->payment_method == 'sadad' && $orderstatus->payment_status == 'pending'))

       <tr>
          <td>{{ $orderstatus->id }}</td>

          <td>{{ $orderstatus->customer->full_name ?? '' }}</td>

          <td>{{ $orderstatus->payment_status }}</td>

          <td>{{ $orderstatus->payment_method }}</td>
          <td>{{ $orderstatus->transaction_number }}</td>

          <td>
            @if($orderstatus->status == 'pending')
            <span class="label label-danger">Pending</span>
            
            @elseif($orderstatus->status == 'confirmed')

            <span class="label label-primary">Confirmed</span>
            
            @elseif($orderstatus->status == 'out for delivery')
            
            <span class="label label-warning">Out For Delivery</span>
            
            @elseif($orderstatus->status == 'delivered')
            
            <span class="label label-success">Delivered</span>

            @elseif($orderstatus->status == 'cancelled')
            
            <span class="label label-danger">Cancelled</span>
            
            @endif
            
            <td>{{ $orderstatus->customer->phone ?? '' }}</td>
         
            <td>{{ $orderstatus->customer->address ?? '' }}</td>

            <td> {{ $orderstatus->area ? $orderstatus->area->area : "" }}</td>
            
            <td> @if($orderstatus->shipping_charges) {{ $orderstatus->shipping_charges }} QTR @endif</td>
            
            
            <td>{{ $orderstatus->carts->count() > 0 ? array_sum($orderstatus->carts->pluck('original_price')->toArray()) +  $orderstatus->charges : '' }} QTR </td> 
            
            
            <td>{{ $orderstatus->total_price }} QTR</td>
  
  
            <td>{{ $orderstatus->created_at }}</td>


          <td>
            <form id="delete-form-{{$orderstatus->id}}" action="{{ route('orderstatuses.destroy',$orderstatus->id) }}" method="POST">
              <a class="btn btn-info" href="{{ route('orderstatuses.show',$orderstatus->id) }}">View</a>
              <a class="btn btn-primary" href="{{ route('orderstatuses.edit',$orderstatus->id) }}">Edit</a>
              @csrf
              @method('DELETE')
              {{--  <button type="submit" class="btn btn-danger">Delete</button>  --}}
              <button type="button" onclick="ask_delete({{$orderstatus->id}})" class="btn btn-danger">Delete</button>

            </form>
          </td>
        </tr>
        @endif
        @endforeach
    </table>
  

@endsection
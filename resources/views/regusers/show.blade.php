@extends('regusers.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Customer Details</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('regusers.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $reguser->email }}<br>
                <strong>Full Name:</strong>
                {{ $reguser->full_name }}<br>
                <strong>Mobile Number:</strong>
                {{ $reguser->phone }}<br>
                <strong>Address:</strong>
                {{ $reguser->address }}<br>
                <strong>Zone No.:</strong>
                {{ $reguser->zone }}<br>
                <strong>Street No.:</strong>
                {{ $reguser->street }}<br>
                <strong>Building No.:</strong>
                {{ $reguser->building }}
            </div>
        </div>
    </div>
@endsection
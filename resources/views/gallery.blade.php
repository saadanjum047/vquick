



@extends('layouts.main')



@section('title')
<title>Gallery</title>
@endsection

@section('tags')
    
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- StyleSheet -->
      <link rel="stylesheet" href="assets/css/style.css">
      <!-- GoogleFonts -->
      <link href="https://fonts.googleapis.com/css2?family=GFS+Didot&display=swap" rel="stylesheet">
      <!-- Slick slider -->
      <link rel="stylesheet" href="assets/slick/slick-master/slick/slick-theme.css">
      <link rel="stylesheet" href="assets/slick/slick-master/slick/slick.css">
      <!-- Link Swiper s CSS -->
      <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
	  <title>Gallery</title>
	  <link rel="stylesheet" href="assets/css/own-style.css">


@endsection

@section('content')
<div class="container my-5 index-mobile-view">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="font-weight-bold text-uppercase our-product-mobile" >Our Gallery</h1>
		</div>
	</div>
</div>
	
<div class="w3-content w3-display-container" style="max-width:800px;margin:2rem 1rem;" >
  
  @foreach($images as $k => $image)
  <img class="mySlides" src="files/{{$image->image}}" title="{{$image->title}}" style="width:100%; height: 228px;">
  @endforeach
  
  <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle" style="width:100%; ">
  
    <div class="w3-left w3-hover-text-khaki" onclick="plusDivs(-1)">&#10094;</div>
    <div class="w3-right w3-hover-text-khaki" onclick="plusDivs(1)">&#10095;</div>
  
    @foreach($images as $image)
    <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv({{$k}})"></span>
    @endforeach

  </div>
</div>
	
@endsection

@section('scripts')
    
<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" w3-white", "");
  }
  x[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " w3-white";
}
</script>

  <script>
         function increaseCount(e, el) {
         var input = el.previousElementSibling;
         var value = parseInt(input.value, 10);
         value = isNaN(value) ? 0 : value;
         value++;
         input.value = value;
         }
         function decreaseCount(e, el) {
         var input = el.nextElementSibling;
         var value = parseInt(input.value, 10);
         if (value > 1) {
         value = isNaN(value) ? 0 : value;
         value--;
         input.value = value;
         }
         }
      </script>
      <script>
         $(".variable").slick({
          dots: true,
          infinite: true,
          autoplay:true,
          time:200,
          pagination:false,
          arrows:true,
          variableWidth: true
         });
      </script>
      <script>
         function openNav(){
           document.getElementById("mySidenav").style.width="300px";
         }
         function closeNav(){
           document.getElementById("mySidenav").style.width="0px";
         }
      </script>

@endsection


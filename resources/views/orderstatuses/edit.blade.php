@extends('orderstatuses.layout')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Orders</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('orderstatuses.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('orderstatuses.update', $orderstatus->id) }}" method="POST">
      @csrf
      @method('PUT')

      <div class="row" style="width:80%;margin:auto;">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="form-group">
            <strong>Total Price:</strong>
            <input type="text" class="form-control" value="{{ $orderstatus->total_price }}" name="total_price" readonly />
          </div>
        </div>
        <?php $a=0;
        if( $orderstatus->payment_method == 'pending') {
          $a="paid";
        } else {
          $a="pending";
        }?>
        <div class="col-xs-12 col-sm-12 col-md-12"  >
          <div class="form-group" id="payment_method" >
            <strong>Payment Status:</strong>
            <select class="form-control" name="payment_method">
              <option value="pending" @if($orderstatus->payment_method ==  'pending' )  selected @endif >pending</option>
              <option value="paid" @if($orderstatus->payment_method == 'paid'  ) selected @endif  >paid</option>
            </select>
          </div>
        </div>
    
        <div class="col-xs-12 col-sm-12 col-md-12" >
          <div class="form-group"  >
            <strong>Status:</strong>
            <select class="form-control" name="status" id="status">
              {{--  <option value="{{ $orderstatus->status }}">{{ $orderstatus->status }}</option>  --}}
              {{--  <option value="" > Change Status </option>  --}}
              <option value="pending" @if($orderstatus->status == 'pending' ) selected @endif >pending</option>
              <option value="confirmed" @if($orderstatus->status == 'confirmed' ) selected @endif >confirmed</option>
              <option value="out for delivery" @if($orderstatus->status == 'out for delivery' ) selected @endif >out for delivery</option>
              <option value="delivered" @if($orderstatus->status == 'delivered' ) selected @endif >delivered</option>
              <option value="cancelled" @if($orderstatus->status == 'cancelled' ) selected @endif >cancelled</option>
            </select>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </div>
   
    </form>


@endsection
@extends('orderstatuses.layout') 
@section('content')
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> View Order Details</h2>
            </div>
            <div class="pull-right" style="width: 225px;">
                @if($carts->first()->order_id)

                <a class="btn btn-warning" href="{{ route('export-invoice' , $orderstatus->id) }}"><i class="fa fa-print"></i> </a>

                <a class="btn btn-success mr-2" href="{{ route('export-order' , $carts->first()->order_id) }}" > Export</a>
                @endif
                <a class="btn btn-primary" href="{{ url()->previous() }}"> Back</a>
            </div>
        </div>
    </div>
    {{--  @foreach ($carts as $cart)
    <?php $oid=$cart->order_id; ?>
    @endforeach  --}}
    <form action="/product-filter" method="GET">
    @csrf
        <div class="col-12" style="display:grid;grid-template-columns:70% 20%;grid-column-gap:5%;">
            <div class="form-group">
            </div>
            <!--<div class="form-group">-->
            <!--    <input type="hidden" name="oid" value="{{ $orderstatus->id }}" >-->
            <!--    <button type="submit" class="btn btn-primary">Search</button>-->
            <!--    <a href="{{url('orderstatuses' , $orderstatus->id)}} " class="btn btn-danger">Clear</a>-->
            <!--</div>-->
        </div>
    </form>
    <table class="table table-bordered">
        <tr>
          <th>No</th>
          <th>Product Image</th>
          <th>Product Name</th>
          <!--<th>Address</th>-->
          <th>Quantity</th>
          <th>Price</th>
          <th>Discounted Price</th>
        </tr>

        @foreach ($carts as $cart)
        <tr>
          <td>{{ ++$i }}</td>
          <td><img style="width: 150px;height: 100px;" src="/files/{{ $cart->image }}"></td>
          <td>{{ $cart->name }}</td>

         {{--  <!--<td>Building: {{ $orderstatus->customer->building }}, Street: {{ $orderstatus->customer->street }}, Zone: {{ $orderstatus->customer->zone }}, {{ $orderstatus->customer->address }} </td>-->  --}}
          <td>{{ $cart->quantity }}</td>
          <td>{{ $cart->original_price }} QTR </td> 
          <td>{{ $cart->price }} QTR</td>
        </tr>
        @endforeach
        <tr>
            <td colspan="5" class="text-right" >Sub Total</td>
            <td > {{$orderstatus->total_price}} </td>
        </tr>
        @if($orderstatus->area)
        <tr>
            <td colspan="5" class="text-right" >Shipping Area </td>
            <td > {{$orderstatus->area->area}} </td>
        </tr>
        <tr>
            <td colspan="5" class="text-right" > Shipping Charges</td>
            <td > {{$orderstatus->shipping_charges}} </td>
        </tr>
        @endif
        <tr>
            <td colspan="5" class="text-right" >Total</td>
            <td > {{$orderstatus->total_price + $orderstatus->shipping_charges}} </td>
        </tr>
    </table>
@endsection
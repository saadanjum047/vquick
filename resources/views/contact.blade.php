@extends('layouts.main')



@section('title')
<title>Contact US</title>
@endsection

@section('content')
    


<form action="/contact-form" method="POST">
  @csrf
<div class="container mt-5">
<div class="row no-gutters">
<div class="col-md-12 col-lg-8 col-sm-12 col-12 condi px-5 contact-us-div" >
 <h2 class="contact-us-h2">{{__("Send us a Message")}} </h2>
 <h2 class="contact-us-h2-mobile">{{__("Get In Touch")}} </h2>
 <div class="row mt-3 mt2">
  <div class="col-md-6 col-sm-12 col-12 mg_l">
    <div class="form-group">
      <input name="name" type="text" class="form-control  inpf contact-us-input" id="exampleInputname1" placeholder="{{__('Your Name')}}"  required>
    </div>
  </div>
  <div class="col-md-6 col-sm-12 col-12 mg_r">
    <div class="form-group">
      <input name="email" type="email"  class="form-control  inpf contact-us-input" id="emailcontact" name="emailcontact" placeholder="{{__('Email')}}" required>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-12 mt-3 mg">
    <div class="form-group">
      <label for="" style="color: #627976;">{{__('Message')}}</label>
      <textarea name="message" id=""  style="width: 100%;height: 200px;background-color: transparent;border:1px solid #999999;" required></textarea>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-12 text-right btn-send">
    <button type="submit" name="submit" class="conbnt px-4 py-2 contact-us-send uncfocused-item" style="border-radius:0px">{{__("Send")}}</button>
  </div>
  
 </div>
</div>
<div class="col-md-4 margin-top-btn">
 <img src="assets/images/contact.PNG"  class=" conim" alt="">
</div>
</div>
</div>
</form>



@endsection